#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
using namespace std;

vector<vector<string>> groupAnagrams(vector<string> &strs)
{
    vector<vector<string>> s;
    if (strs.empty())
        return s;
    map<string, vector<string>> dict;
    for (int i = 0; i < strs.size(); i++)
    {
        string str = strs[i];
        sort(str.begin(), str.end());
        auto it = dict.find(str);
        if (it != dict.end())
            it->second.push_back(strs[i]);
        else
        {
            vector<string> temp;
            temp.push_back(strs[i]);
            dict.insert(make_pair(str, temp));
        }
    }
    for (auto it = dict.begin(); it != dict.end(); it++)
        s.push_back(it->second);
    return s;
}