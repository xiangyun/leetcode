#include <iostream>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void hasPathSum(TreeNode *root, int sum, vector<vector<int>> &arr, vector<int> cur)
{
    if (!root)
        return;
    sum -= root->val;
    cur.push_back(root->val);
    if (sum == 0 && !root->left && !root->right)
        arr.push_back(cur);
    if (root->left)
        hasPathSum(root->left, sum, arr, cur);
    if (root->right)
        hasPathSum(root->right, sum, arr, cur);
}

vector<vector<int>> pathSum(TreeNode *root, int sum)
{
    vector<vector<int>> res;
    hasPathSum(root, sum, res, vector<int>());
    return res;
}