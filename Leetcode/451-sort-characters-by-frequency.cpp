#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <algorithm>
using namespace std;
string frequencySort(string s)
{
    unordered_map<char, int> hash;
    for (auto c : s)
        hash[c]++;
    vector<pair<char, int>> table(hash.begin(), hash.end());
    auto cmp = [](const pair<char, int> &a, const pair<char, int> &b) {
        return a.second > b.second;
    };
    sort(table.begin(), table.end(), cmp);
    string t = "";
    for (auto p : table)
    {
        for (int k = 0; k < p.second; k++)
            t += p.first;
    }
    return t;
}