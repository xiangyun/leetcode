#include <iostream>
#include <vector>
// #include <set>
#include <map>
using namespace std;

// struct node
// {
// 	int index;
// 	int val;
// 	bool operator<(const node &a) const
// 	{
// 		return val < a.val;
// 	}
// 	node(int index, int val)
// 	{
// 		this->index = index;
// 		this->val = val;
// 	}
// };

// int oddEvenJumps(vector<int> &A)
// {
// 	set<node> S;
// 	for (int i = 0; i < A.size(); i++)
// 	{
// 		S.emplace(i, A[i]);
// 	}
// 	vector<vector<int>> arr(A.size(), vector<int>(2, 0));
// 	for (int i = 0; i < A.size(); i++)
// 	{
// 		node t = node(i, A[i]);
// 		S.erase(t);
// 		auto it = S.lower_bound(t);
// 		if (it != S.end())
// 		{
// 			arr[i][0] = it->index;
// 			if (it->val == A[i])
// 			{
// 				arr[i][1] = it->index;
// 			}
// 			else
// 			{
// 				if (it != S.begin())
// 				{
// 					it--;
// 					arr[i][1] = it->index;
// 				}
// 				else
// 					arr[i][1] = -1;
// 			}
// 		}
// 		else
// 		{
// 			arr[i][0] = -1;
// 			auto it = S.rbegin();
// 			if (it == S.rend())
// 				arr[i][1] = -1;
// 			else
// 				arr[i][1] = it->index;
// 		}
// 	}
// 	int cnt = 0;
// 	for (int i = 0; i < A.size(); i++)
// 	{
// 		int step = 1;
// 		int index = i;
// 		while (index != -1 && index != A.size() - 1)
// 		{
// 			if (step % 2 == 1)
// 				index = arr[index][0];
// 			else
// 				index = arr[index][1];
// 		}
// 		if (index == A.size() - 1)
// 			++cnt;
// 	}
// 	return cnt;
// }
int oddEvenJumps(vector<int> &A)
{
	int n = A.size();
	vector<int> odd(n, -1), even(n, -1);
	map<int, int> nums;
	for (int i = n - 1; i >= 0; --i)
	{
		auto it = nums.lower_bound(A[i]);
		if (it != nums.end())
			odd[i] = it->second;
		it = nums.upper_bound(A[i]);
		if (it != nums.begin())
		{
			it--;
			even[i] = it->second;
		}
		nums[A[i]] = i;
	}
	int cnt = 0;
	for (int i = 0; i < n; ++i)
	{
		int index = i, step = 1;
		while (index != -1 && index != n - 1)
		{
			if (step % 2 == 1)
				index = odd[index];
			else
				index = even[index];
			++step;
		}
		cnt += (index == n - 1);
	}
	return cnt;
}