#include <iostream>
#include <vector>
using namespace std;

vector<int> fairCandySwap(vector<int> &A, vector<int> &B)
{
    int sum_A = 0, sum_B = 0;
    for (auto i : A)
        sum_A += i;
    for (auto j : B)
        sum_B += j;
    vector<int> ans;
    for (int i = 0; i < A.size(); i++)
    {
        for (int j = 0; j < B.size(); j++)
            if (sum_B - sum_A + 2 * A[i] == 2 * B[j])
            {
                ans.push_back(A[i]);
                ans.push_back(B[j]);
                break;
            }
        if (ans.size() == 2)
            break;
    }
    return ans;
}