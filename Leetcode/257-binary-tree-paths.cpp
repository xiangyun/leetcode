#include <iostream>
#include <vector>
#include <string>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void preOrder(TreeNode *root, vector<string> &res, string arr)
{
    if (!root)
        return;
    if (!root->left && !root->right)
    {
        arr += to_string(root->val);
        res.push_back(arr);
        return;
    }
    arr = arr + to_string(root->val) + "->";
    preOrder(root->left, res, arr);
    preOrder(root->right, res, arr);
}

vector<string> binaryTreePaths(TreeNode *root)
{
    vector<string> res;
    preOrder(root, res, "");
    return res;
}