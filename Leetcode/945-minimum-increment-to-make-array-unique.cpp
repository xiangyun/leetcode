#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int minIncrementForUnique(vector<int> &A)
{
    int cnt = 0;
    sort(A.begin(), A.end());
    for (int i = 1; i < A.size(); i++)
    {
        if (A[i] > A[i - 1])
            continue;
        else
        {
            cnt = A[i - 1] - A[i] + 1;
            A[i] = A[i - 1] + 1;
        }
    }
    return cnt;
}