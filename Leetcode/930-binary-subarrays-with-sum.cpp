#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;
int numSubarraysWithSum(vector<int> &A, int S)
{
    // unordered_map<int, int> hash;
    // int sum = 0, cnt = 0;
    // for (auto &i : A)
    // {
    //     sum += i;
    //     hash[sum]++;
    // }
    // hash[0] = 1;
    // for (auto it = hash.begin(); it != hash.end(); it++)
    // {
    //     if (hash.count(it->first + S))
    //     {
    //         cnt += hash[it->first + S] * hash[it->first];
    //     }
    // }
    // return cnt;
    unordered_map<int, int> hash;
    int *partial_sum = new int[A.size() + 1];
    partial_sum[0] = 0;
    int sum = 0, cnt = 0;
    for (int i = 0; i < A.size(); i++)
    {
        partial_sum[i + 1] = partial_sum[i] + A[i];
    }
    for (int i = 0; i <= A.size(); i++)
    {
        cnt += hash[partial_sum[i]];
        hash[partial_sum[i] + S]++;
    }
    delete [] partial_sum;
    return cnt;
}