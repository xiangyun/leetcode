#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
  public:
    int removeBoxes(vector<int> &boxes)
    {
        arr = boxes;
        return DFS(0, arr.size() - 1, 0);
    }

  private:
    int DFS(int l, int r, int k) //k boxes equal to boxes[r] after r
    //points: remove boxes from l->r
    {
        if (l > r)
            return 0;
        if (dp[l][r][k])
            return dp[l][r][k];
        dp[l][r][k] = DFS(l, r - 1, 0) + (k + 1) * (k + 1); //remove boxes from l->r-1
        for (int i = l; i < r; i++)
        {
            if (arr[i] == arr[r])
                dp[l][r][k] = max(dp[l][r][k], DFS(l, i, k + 1) + DFS(i + 1, r - 1, 0));
        }
        return dp[l][r][k];
    }
    vector<int> arr;
    // int MAX_SIZE = 100;
    int dp[100][100][100] = {0};
};
