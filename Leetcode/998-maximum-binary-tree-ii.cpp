#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

TreeNode *insertIntoMaxTree(TreeNode *root, int val)
{
    if (!root)
        return new TreeNode(val);
    if (root->val < val)
    {
        TreeNode *node = new TreeNode(val);
        node->left = root;
        return node;
    }
    else
    {
        root->right = insertIntoMaxTree(root->right, val);
        return root;
    }
}