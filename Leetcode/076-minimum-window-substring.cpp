#include <iostream>
#include <string>
#include <unordered_map>
#include <algorithm>
using namespace std;
/*
given a string S and a string T, find the minimum window in S will contian all the characters in T in complexity O(n)
input: S="ADOBECODEBANC", T='ABC'
output: "BANC"
*/
/*
string minWindow(string s, string t)
{
    vector<int> map(128, 0);
    for (auto c : t)
        map[c]++;
    int counter = t.size(), begin = 0, end = 0, d = INT_MAX, head = 0;
    while (end < s.size())
    {
        if (map[s[end++]]-- > 0)
            counter--;
        while (counter == 0)
        {
            if (end - begin < d)
            {
                head = begin;
                d = end - begin;
            }
            if (map[s[begin++]]++ == 0)
                counter++;
        }
    }
    return d == INT_MAX ? "" : s.substr(head, d);
}*/

string minWindow(string s, string t)
{
    vector<int> map(128, 0);
    for (auto c : t)
        map[c]++;
    int count = t.size(), begin = 0, end = 0, d = INT_MAX, head = 0;
    while (end < s.size())
    {
        if (map[s[end]] > 0)
            count--;
        map[s[end]]--;
        end++;
        while (count == 0)
        {
            if (d > end - begin)
            {
                d = end - begin;
                head = begin;
            }
            if (map[s[begin]] == 0)
                count++;
            map[s[begin]]++;
            begin++;
        }
    }
    return d == INT_MAX ? "" : s.substr(head, d);
}