#include <iostream>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
void inOrder(TreeNode *root, vector<int> &arr)
{
    if (root == NULL)
        return;
    inOrder(root->left, arr);
    arr.push_back(root->val);
    inOrder(root->right, arr);
}

bool isValidBST(TreeNode *root)
{
    if (root == NULL || (root->left == NULL && root->right == NULL))
        return true;
    vector<int> arr;
    inOrder(root, arr);
    int pivot = arr[0];
    for (int i = 1; i < arr.size(); i++)
    {
        if (arr[i] > pivot)
            pivot = arr[i];
        else
            return false;
    }
    return true;
}