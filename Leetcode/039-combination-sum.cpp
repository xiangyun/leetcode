#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
/*
 backtracking method
*/

void backTracking(vector<vector<int>> &vec, vector<int> &arr, vector<int> &num, const int target, int cursum, int index)
{
    if (cursum == target)
    {
        vec.push_back(arr);
        return;
    }
    if (cursum > target || index >= num.size())
        return;
    for (int i = index; i < num.size(); i++)
    {
        arr.push_back(num[i]);
        backTracking(vec, arr, num, target, cursum + num[i], i);
        arr.pop_back();
    }
}

vector<vector<int>> combinationSum(vector<int> &candidates, int target)
{
    vector<vector<int>> vec;
    if (candidates.empty())
        return vec;
    vector<int> arr;
    sort(candidates.begin(), candidates.end());
    backTracking(vec, arr, candidates, target, 0, 0);
    return vec;
}