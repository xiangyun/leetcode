#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
using namespace std;
bool backtracking(vector<int> &arr, vector<int> &matches, int cur_index, int target)
{
    if (cur_index == arr.size())
        return matches[0] == matches[1] && matches[1] == matches[2] && matches[2] == matches[3];

    for (int i = 0; i < 4; i++)
    {
        int j = i - 1;
        while (j >= 0)
        {
            if (matches[i] == matches[j])
                break;
            j--;
        }
        if (j != -1)
            continue;
        matches[i] += arr[cur_index];
        if (matches[i] <= target && backtracking(arr, matches, cur_index + 1, target))
            return true;
        matches[i] -= arr[cur_index];
    }
    return false;
}

bool makesquare(vector<int> &nums)
{
    if (nums.empty())
        return false;
    int sum = 0;
    for (auto i : nums)
        sum += i;
    if (sum % 4 != 0)
        return false;
    int target = sum / 4;
    vector<int> matches(4, 0);
    auto cmp = [](const int &a, const int &b) {
        return a > b;
    };
    sort(nums.begin(), nums.end(), cmp);
    return backtracking(nums, matches, 0, target);
}
