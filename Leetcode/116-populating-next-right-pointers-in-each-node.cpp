#include <iostream>
#include <vector>
using namespace std;

struct TreeLinkNode
{
    int val;
    TreeLinkNode *left, *right, *next;
    TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
};

void connect(TreeLinkNode *root)
{
    if (root)
    {
        TreeLinkNode *L_node = root->left;
        TreeLinkNode *R_node = root->right;
        while (L_node)
        {
            L_node->next = R_node;
            L_node = L_node->right;
            R_node = R_node->left;
        }
        connect(root->left);
        connect(root->right);
    }
}