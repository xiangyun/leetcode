#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;
int numRabbits(vector<int> &answers)
{
    unordered_map<int, int> hash;
    for (auto a : answers)
        hash[a]++;
    int sum = 0;
    for (auto it = hash.begin(); it != hash.end(); it++)
    {
        if (it->second % (it->first + 1) != 0)
            sum += (it->second / (it->first + 1) + 1) * (it->first + 1);
        else
            sum += it->second;
    }
    return sum;
}