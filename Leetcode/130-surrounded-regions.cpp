#include <iostream>
#include <vector>
using namespace std;

class Solution
{
  public:
    bool isValid(vector<vector<char>> &board, int i, int j)
    {
        return i >= 0 && j >= 0 && i < board.size() && j < board[0].size();
    }

    void dfs(vector<vector<char>> &board, int i, int j)
    {
        if (isValid(board, i, j) && board[i][j] == 'O')
        {
            board[i][j] = '#';
            dfs(board, i + 1, j);
            dfs(board, i - 1, j);
            dfs(board, i, j + 1);
            dfs(board, i, j - 1);
        }
        else
            return;
    }

    void solve(vector<vector<char>> &board)
    {
        if (board.empty())
            return;
        for (int i = 0; i < board.size(); i++)
        {
            dfs(board, i, 0);
            dfs(board, i, board[0].size() - 1);
        }
        for (int j = 0; j < board[0].size(); j++)
        {
            dfs(board, 0, j);
            dfs(board, board.size() - 1, j);
        }
        for (int i = 0; i < board.size(); i++)
        {
            for (int j = 0; j < board[0].size(); j++)
            {
                if (board[i][j] == '#')
                    board[i][j] = 'O';
                else if (board[i][j] == 'O')
                    board[i][j] = 'X';
                else
                    continue;
            }
        }
    }
};