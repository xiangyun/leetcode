#include <iostream>
#include <string>
using namespace std;
string reverseString(string s)
{
    return string(s.rbegin(),s.rend());
}