#include <iostream>
#include <stack>
using namespace std;

class MyQueue
{
  public:
    MyQueue()
    {
    }
    ~MyQueue()
    {
    }
    void push(int x)
    {
        stk_in.push(x);
    }
    int pop()
    {
        while (!stk_in.empty())
        {
            int x = stk_in.top();
            stk_out.push(x);
            stk_in.pop();
        }
        int val = stk_out.top();
        stk_out.pop();
        while (!stk_out.empty())
        {
            int x = stk_out.top();
            stk_in.push(x);
            stk_out.pop();
        }
        return val;
    }
    int peek()
    {
        while (!stk_in.empty())
        {
            int x = stk_in.top();
            stk_out.push(x);
            stk_in.pop();
        }
        int val = stk_out.top();
        while (!stk_out.empty())
        {
            int x = stk_out.top();
            stk_in.push(x);
            stk_out.pop();
        }
        return val;
    }
    bool empty()
    {
        return stk_in.empty();
    }

  private:
    stack<int> stk_in, stk_out;
};