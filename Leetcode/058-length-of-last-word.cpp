#include <iostream>
#include <string>
using namespace std;
int lengthOfLastWord(string s)
{
    while (s.back() == ' ')
        s.pop_back();
    if (s.length() == 0)
        return 0;
    int count = 0;
    for (int i = s.length() - 1; i >= 0; i--)
    {
        if (s[i] == ' ')
            break;
        else
            count++;
    }
    return count;
}