#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <unordered_map>
using namespace std;

void dfs(string s, unordered_map<string, int> dicts, vector<string> arr, unordered_map<string, int>  &res)
{
	if (s.empty())
	{
		string temp_str = "";
		for (auto &i : arr)
			temp_str += i + " ";
		res[temp_str.substr(0, temp_str.size() - 1)]++;
	}
	for (int i = 0; i <= s.size(); i++)
	{
		string str = s.substr(0, i);
		if (dicts.count(str))
		{
			arr.push_back(str);
			dfs(s.substr(i, s.size() - i), dicts, arr, res);
			arr.pop_back();
		}
	}
}

vector<string> wordBreak(string s, vector<string> &wordDict)
{
	unordered_map<string, int> dicts;
	for (auto &i : wordDict)
		dicts[i]++;
	unordered_map<string, int> res;
	dfs(s, dicts, vector<string>(), res);
	vector<string> result;
	for (auto it = res.begin(); it != res.end(); it++)
	{
		result.push_back(it->first);
	}
	return result;
}