#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <unordered_set>
using namespace std;

void backtracking(string s, int left_r, int right_r, int pair, string path, int index, unordered_set<string> &res)
{
    if (index == s.size())
    {
        if (left_r == 0 && right_r == 0 && pair == 0)
            res.insert(path);
        return;
    }
    if (s[index] != '(' && s[index] != ')')
        backtracking(s, left_r, right_r, pair, path + s[index], index + 1, res);
    else
    {
        if (s[index] == '(')
        {
            if (left_r > 0)
                backtracking(s, left_r - 1, right_r, pair, path, index + 1, res);
            backtracking(s, left_r, right_r, pair + 1, path + s[index], index + 1, res);
        }
        if (s[index] == ')')
        {
            if (right_r > 0)
                backtracking(s, left_r, right_r - 1, pair, path, index + 1, res);
            if (pair > 0)
                backtracking(s, left_r, right_r, pair - 1, path + s[index], index, res);
        }
    }
}

vector<string> removeInvalidParentheses(string s)
{
    unordered_set<string> res;
    int left_r = 0, right_r = 0;
    for (auto c : s)
    {
        if (c == '(')
            left_r++;
        if (c == ')')
        {
            if (left_r != 0)
                left_r--;
            else
                right_r++;
        }
    }
    backtracking(s, left_r, right_r, 0, "", 0, res);
    return vector<string>(res.begin(), res.end());
}
