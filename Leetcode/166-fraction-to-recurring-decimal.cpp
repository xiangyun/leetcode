#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;
string fractionToDecimal(int numerator, int denominator)
{
    if (!numerator)
        return "0";
    string ans;
    if (numerator < 0 || denominator < 0)
        ans += '-';
    long n = labs(long(numerator)), d = labs(long(denominator)), r; //numerator denominator remainder
    ans += to_string(n / d);
    r = n % d;
    if (!r)
        return ans;
    unordered_map<long, int> m;
    long q; //quotient
    ans += '.';
    r *= 10;
    while (r)
    {
        q = r / d;
        if (m.count(r))//count:return true or false
        {
            ans.insert(m[r], 1, '(');
            ans += ')';
            return ans;
        }
        m[r] = ans.size();
        ans += to_string(q);
        r = (r % d) * 10;
    }
    return ans;
}