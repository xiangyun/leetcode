#include <iostream>
#include <vector>
using namespace std;

int consecutiveNumbersSum(int N)
{
    N *= 2;
    int sum = 0;
    for (int k = 1; k <= sqrt(N); k++)
    {
        if (N % k == 0 && ((N / k - k) % 2))
            sum++;
    }
    return sum;
}