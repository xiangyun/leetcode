#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
using namespace std;
class UnionFind
{
  private:
    int count;
    int *parent;
    int *rank;

  public:
    UnionFind(int n)
    {
        count = n;
        parent = new int[n];
        rank = new int[n];
        for (int i = 0; i < n; i++)
        {
            parent[i] = i;
            rank[i] = 1;
        }
    }
    ~UnionFind()
    {
        delete[] parent;
        delete[] rank;
    }
    int find(int x)
    {
        if (x != parent[x])
            parent[x] = find(parent[x]);
        return parent[x];
    }
    void Union(int x, int y)
    {
        int fx = find(x), fy = find(y);
        if (fx == fy)
            return;
        if (rank[fx] > rank[fy])
        {
            parent[fy] = fx;
            rank[fx] += rank[fy];
        }
        else
        {
            parent[fx] = fy;
            rank[fy] += rank[fx];
        }
        count--;
    }
    bool isConnect(int x, int y)
    {
        return find(x) == find(y);
    }
};
class Solution
{
  public:
    vector<vector<string>> accountsMerge(vector<vector<string>> &accounts)
    {
        UnionFind uf(accounts.size());
        for (int i = 0; i < accounts.size(); i++)
        {
            for (int j = i + 1; j < accounts.size(); j++)
            {
                if(accounts[i][0]==accounts[j][0])
                {
                    
                }
            }
        }
    }
};