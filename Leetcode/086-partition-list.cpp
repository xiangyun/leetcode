#include <iostream>
using namespace std;
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

ListNode *partition(ListNode *head, int x)
{
    if (head == NULL)
        return head;
    ListNode *p_head = new ListNode(-1), *q_head = new ListNode(-1), *cur = head;
    ListNode *p = p_head, *q = q_head;
    while (cur)
    {
        if (cur->val < x)
        {
            p->next = cur;
            cur = cur->next;
            p = p->next;
        }
        else
        {
            q->next = cur;
            cur = cur->next;
            q = q->next;
        }
    }
    p->next = q_head->next;
    q_head->next = NULL;
    return p_head->next;
}