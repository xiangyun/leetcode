#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;
int subarraySum(vector<int> &nums, int k)
{
    unordered_map<int, int> hash;
    int sum = 0, cnt = 0;
    for (auto i : nums)
    {
        sum += i;
        if (hash.count(sum - k))
            cnt += hash[sum - k];
        hash[sum]++;
    }
    return cnt;
}