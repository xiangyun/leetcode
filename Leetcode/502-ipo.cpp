#include <iostream>
#include <vector>
#include <queue>
using namespace std;
int findMaximizedCapital(int k, int W, vector<int> &Profits, vector<int> &Capital)
{
    auto cmp = [](const pair<int, int> &a, const pair<int, int> &b) {
        if (a.first > b.first)
            return true;
        else if (a.first == b.first)
            return a.second > b.second;
        else
            return false;
    };
    priority_queue<int> low;
    priority_queue<pair<int, int>, vector<pair<int, int>>, decltype(cmp)> high(cmp);
    for (int i = 0; i < Capital.size(); i++)
    {
        if (Capital[i] <= W)
            low.push(Profits[i]);
        else
            high.emplace(Capital[i], Profits[i]);
    }
    while (k > 0 && !low.empty())
    {
        W += low.top();
        low.pop();
        k--;
        while (!high.empty() && high.top().first <= W)
        {
            low.push(high.top().second);
            high.pop();
        }
    }
    return W;
}