#include <iostream>
#include <vector>
#include <queue>
#include <string>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Codec
{
  public:
    Codec() {}
    ~Codec() {}
    /*
	string serialize(TreeNode *root)
	{
		string s = "";
		if (!root)
			return "[null]";
		queue<TreeNode *> que;
		que.push(root);
		//s += to_string(root->val) + ",";
		while (!que.empty())
		{
			TreeNode *cur_node = que.front();
			if (cur_node)
			{
				s += to_string(cur_node->val) + ",";
				que.push(cur_node->left);
				que.push(cur_node->right);
			}
			else
			{
				s += "null,";
			}
			que.pop();
		}
		s.back() = ']';
		s.insert(s.begin(), '[');
		return s;
	}*/
    TreeNode *deserialize(string data)
    {
        vector<string> res = split(data);
        return deserialize(res, 0);
    }
    vector<string> split(string s)
    {
        string::iterator it = s.begin() + 1;
        vector<string> res;
        string str = "";
        while (it != s.end())
        {
            while (it != s.end() - 1 && *it != ',')
            {
                str += *it;
                it++;
            }
            it++;
            res.push_back(str);
            str = "";
        }
        return res;
    }

    void destory(TreeNode *_root)
    {
        TreeNode *temp = _root;
        //cout << "hello" << endl;
        if (!temp)
            return;
        destory(temp->left);
        destory(temp->right);
        delete temp;
        temp = NULL;
    }

  private:
    TreeNode *deserialize(vector<string> &Q, int i)
    {
        string str = Q[i];
        if (str == "null")
            return NULL;

        TreeNode *root = new TreeNode(stoi(str));
        if (!Q.empty() && (2 * i + 1) < Q.size())
            root->left = deserialize(Q, 2 * i + 1);
        if (!Q.empty() && (2 * i + 2) < Q.size())
            root->right = deserialize(Q, 2 * i + 2);
        return root;
    }
};
/*
class Solution {
public:
    int pathSum(TreeNode* root, int sum) {
        if(!root) return 0;
        return sumUp(root, 0, sum) + pathSum(root->left, sum) + pathSum(root->right, sum);
    }
private:
    int sumUp(TreeNode* root, int pre, int& sum){
        if(!root) return 0;
        int current = pre + root->val;
        return (current == sum) + sumUp(root->left, current, sum) + sumUp(root->right, current, sum);
    }
};
*/
void backtracking(TreeNode *root, int cur_sum, int target, int &cnt, bool prev_used)
{
    if (!root)
    {
        if (cur_sum == target)
            cnt++;
        return;
    }
    if (prev_used)
    {
        backtracking(root->left, cur_sum + root->val, target, cnt, true);
        backtracking(root->right, cur_sum + root->val, target, cnt, true);
        if (cur_sum == target)
            cnt++;
        return;
    }
    else
    {
        if (cur_sum == 0)
        {
            backtracking(root->left, cur_sum + root->val, target, cnt, true);
            backtracking(root->right, cur_sum + root->val, target, cnt, true);
            backtracking(root->left, cur_sum, target, cnt, false);
            backtracking(root->right, cur_sum, target, cnt, false);
        }
    }
}

int pathSum(TreeNode *root, int sum)
{
    int cnt = 0;
    backtracking(root, 0, sum, cnt, false);
    return cnt / 2;
}

int main()
{
    Codec codec;
    string s = "[10,5,-3,3,2,null,11,3,-2,null,1]";
    TreeNode *root = codec.deserialize(s);
    //cout << pathSum(root, 8) << endl;
    codec.destory(root);
    return 0;
}