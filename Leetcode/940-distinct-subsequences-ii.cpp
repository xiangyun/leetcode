#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
using namespace std;

/*
Given a string s count the number of distinct not empty subsequences of S
*/

int distinctSubseqII(string S)
{
    long long mod = 1000000007;
    int n = S.size();
    vector<int> dp(n + 1);
    dp[0] = 1;
    unordered_map<char, int> last(26);
    for (int i = 1; i <= n; i++)
    {
        dp[i] = (dp[i - 1] * 2) % mod;
        if (last[S[i - 1]] != 0)
            dp[i] = (dp[i] -  dp[last[S[i - 1]] - 1]) % mod;
        last[S[i - 1]] = i;
    }
    return dp[n] - 1;
}