#include <iostream>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution
{
  public:
    void inOrder(TreeNode *root)
    {
        if (root == NULL)
            return;
        inOrder(root->left);
        if (pre && pre->val > root->val)
        {
            if (!left)
                left = pre;
            right = root;
        }
        pre = root;
        if (pre && pre->val)
            pre = root;
        inOrder(root->right);
    }
    void recoverTree(TreeNode *root)
    {
        if (!root)
            return;
        inOrder(root);
        swap(left->val, right->val);
    }

  private:
    TreeNode *pre = NULL;
    TreeNode *left = NULL;
    TreeNode *right = NULL;
};
