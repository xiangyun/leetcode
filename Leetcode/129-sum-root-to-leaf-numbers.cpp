#include <iostream>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution
{
  public:
    void dfs(TreeNode *root, int cur_sum, vector<int> &arr)
    {
        if (!root)
            return;
        if (!root->left && !root->right)
        {
            arr.push_back(cur_sum * 10 + root->val);
            return;
        }
        dfs(root->left, cur_sum * 10 + root->val, arr);
        dfs(root->right, cur_sum * 10 + root->val, arr);
    }

    int sumNumbers(TreeNode *root)
    {
        vector<int> res;
        int sum = 0;
        dfs(root, 0, res);
        for (auto &i : res)
        {
            sum += i;
        }
        return sum;
    }
};