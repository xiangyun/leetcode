#include <iostream>
#include <string>
using namespace std;

bool backtracking(long long n1, long long n2, string cur_str)
{
    if (cur_str.empty())
        return false;
    bool flag = false;
    for (int i = 1; i <= cur_str.size(); i++)
    {
        string cur_substr = cur_str.substr(0, i);

        if (cur_substr[0] == '0' && cur_substr.size() > 1)
            continue;
        long n3 = stoll(cur_substr);
        if (n1 + n2 == n3)
        {
            if (i == cur_str.size())
                return true;
            flag = flag || backtracking(n2, n3, cur_str.substr(i, cur_str.size() - i));
            if (flag)
                return flag;
        }
    }
    return false;
}

bool isAdditiveNumber(string num)
{
    bool flag = false;
    if (num.size() <= 2)
        return false;
    for (int i = 1; i < num.size(); i++)
    {
        for (int j = 1; j + i < num.size(); j++)
        {
            string s1 = num.substr(0, i);
            string s2 = num.substr(i, j);
            if ((s1[0] == '0' && s1.size() > 1) || (s2[0] == '0' && s2.size() > 1))
                continue;
            flag = flag || backtracking(stoll(s1), stoll(s2), num.substr(j + i, num.size() - i - j));
            if (flag)
                return flag;
        }
    }
    return false;
}