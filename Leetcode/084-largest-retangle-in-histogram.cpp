#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>
using namespace std;
/*
Given n non-negative integers representing the histogram's bar height where the width of each bar is 1, find the area of largest rectangle in the histogram.
*/
/*int largestRectangleArea(vector<int> &heights)
{
    int max_area = 0;
    for (int t = 0; t < heights.size(); t++)
    {
        int i = t, j = t;
        while (i && heights[i] >= heights[t])
            i--;
        while (j < heights.size() && heights[j] >= heights[t])
            j++;
        max_area = max(max_area, (j - i - 1) * heights[t]);
    }
    return max_area;
}*/

int largestRectangleArea(vector<int> &heights)
{
    stack<int> stk;
    heights.push_back(0);
    int res = 0;
    for (int i = 0; i < heights.size(); i++)
    {
        while (!stk.empty() && stk.top() > heights[i])
        {
            int r = i, height = stk.top();
            stk.pop();
            int l = stk.empty() ? -1 : stk.top();
            res = max(res, (r - l - 1) * height);
        }
    }
    return res;
}
