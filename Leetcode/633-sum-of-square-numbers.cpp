#include <iostream>
#include <cmath>
using namespace std;
bool judgeSquareSum(int c)
{
    int t = int(sqrt(c));
    for (int i = 1; i < t; ++i)
    {
        int y = c - i * i;
        if (sqrt(y) - (int)sqrt(y) == 0)
            return true;
    }
    return false;
}