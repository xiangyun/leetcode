#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int integerBreak(int n)
{
    if (n == 1)
        return 0;
    if (n == 2)
        return 1;
    if (n == 3)
        return 2;
    vector<int> dp(n + 1, 0);
    dp[1] = 0;
    dp[2] = 1;
    dp[3] = 2;
    for (int i = 4; i <= n; i++)
    {
        for (int j = 2; j < i; j++)
        {
            dp[i] = max(max(dp[i - j], i - j) * j, dp[i]);
        }
    }
    return dp[n];
}