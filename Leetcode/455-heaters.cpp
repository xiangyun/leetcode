#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
using namespace std;
int findContentChildren(vector<int> &g, vector<int> &s)
{
    int cnt = 0;
    int gi = 0, si = 0;
    sort(g.begin(), g.end(), greater<int>());
    sort(s.begin(), s.end(), greater<int>());
    while (gi < g.size() && si < s.size())
    {
        if (g[gi] <= s[si])
        {
            ++gi;
            ++si;
            ++cnt;
        }
        else
            ++gi;
    }
    return cnt;
}