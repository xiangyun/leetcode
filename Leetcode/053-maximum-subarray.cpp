#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
int maxSubArray(vector<int> & nums)
{
    if(nums.size()==0)
    return 0;
    int max_sum=nums[0];
    int res = INT_MIN;
    for(int i=0;i<nums.size();i++)
    {
        max_sum=max(max_sum+nums[i],nums[i]);
        res=max(res,max_sum);
    }
    return res;
}