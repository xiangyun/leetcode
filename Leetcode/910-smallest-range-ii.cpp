#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int smallestRangeII(vector<int> &A, int k)
{
    sort(A.begin(), A.end());
    int min_len = INT_MAX;
    int n = A.size();
    for (int i = 0; i < A.size() - 1; i++)
    {
        int max_val = max(A[i] + k, A[n - 1] - k);
        int min_val = min(A[0] + k, A[i + 1] - k);
        min_len = min(min_len, max_val - min_val);
    }
    return min(A[n - 1] - A[0], min_len);
}

// struct TreeNode
// {
//     int val;
//     TreeNode *left;
//     TreeNode *right;
//     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
// };
// TreeNode *flip(TreeNode *root)
// {
//     if (!root)
//         return NULL;
//     TreeNode *left = flip(root->left);
//     TreeNode *right = flip(root->right);
//     root->left = right;
//     root->right = left;
//     return root;
// }