#include <iostream>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left, *right;
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};
TreeNode *bstFromPreorder(vector<int> &arr)
{
    if (arr.empty())
        return nullptr;
    else
    {
        TreeNode *root = new TreeNode(arr[0]);
        int i = 1;
        vector<int> _left, _right;
        for (; i < arr.size(); ++i)
        {
            if (arr[i] < arr[0])
                _left.push_back(arr[i]);
            else
            {
                _right.push_back(arr[i]);
            }
        }

        root->left = bstFromPreorder(_left);
        root->right = bstFromPreorder(_right);
        return root;
    }
}