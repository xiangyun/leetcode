#include <iostream>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left, *right;
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};
TreeNode *buildTree(vector<int> &inorder, vector<int> &postorder)
{
    if (inorder.empty())
        return nullptr;
    int val = postorder.back();
    TreeNode *root = new TreeNode(val);
    vector<int> left_in, right_in, left_post, right_post;
    int index;
    for (int i = 0; i < inorder.size(); ++i)
    {
        if (inorder[i] == val)
        {
            index = i;
            break;
        }
        left_in.push_back(inorder[i]);
        left_post.push_back(postorder[i]);
    }
    for (int i = index + 1; i < inorder.size(); ++i)
    {
        right_in.push_back(inorder[i]);
        right_post.push_back(postorder[i - 1]);
    }
    root->left = buildTree(left_in, left_post);
    root->right = buildTree(right_in, right_post);
    return root;
}