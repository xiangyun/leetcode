#include <iostream>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

int pathsum(TreeNode *root, int cur_sum, int sum)
{
    if (!root)
    {
            return 0;
    }
    return (root->val + cur_sum == sum) + pathsum(root->left, cur_sum + root->val, sum) + pathsum(root->right, cur_sum + root->val, sum);
}

int pathSumiii(TreeNode *root, int sum)
{
    if (!root)
        return 0;
    return pathsum(root, 0, sum) + pathSumiii(root->left, sum) + pathSumiii(root->right, sum);
}