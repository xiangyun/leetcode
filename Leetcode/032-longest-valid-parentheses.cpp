#include <iostream>
#include <string>
#include <stack>
#include <algorithm>
using namespace std;
int longestValidParentheses(string s)
{
	if(s.empty())
		return 0;
	stack<int> stk;
	int maxLen = 0;
	int count = s.length();
	for (int i = 0; i < s.length();i++)
	{
		if(!stk.empty()&&s[i]==')'&&s[stk.top()]=='(')
			stk.pop();
		else
			stk.push(i);
	}
	while(!stk.empty())
	{
		maxLen = max(count - stk.top() - 1, maxLen);
		count = stk.top();
		stk.pop();
	}
	maxLen = max(maxLen, count);
	return maxLen;
}