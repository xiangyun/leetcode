#include <iostream>
#include <vector>
#include <string>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
class Codec
{
  public:
    // Encodes a tree to a single string.
    string serialize(TreeNode *root)
    {
        string s = "";
        preOrder(root, s);
        s.insert(s.begin(), '[');
        s.back() = ']';
        return s;
    }

    // Decodes your encoded data to tree.
    TreeNode *deserialize(string data)
    {
        // cout << data << endl;
        vector<int> arr = split(data);
        // for (auto i : arr)
        // cout << i << " ";
        return buildTree(arr, 0, arr.size() - 1);
    }

  private:
    vector<int> split(string data)
    {
        if (data == "[]")
            return vector<int>();
        vector<int> digits;
        int val = 0;
        for (int i = 1; i < data.size() - 1; i++)
        {
            if (data[i] != ',')
                val = val * 10 + data[i] - '0';
            else
            {
                digits.push_back(val);
                val = 0;
            }
        }
        digits.push_back(val);
        return digits;
    }
    void preOrder(TreeNode *root, string &s)
    {
        if (!root)
            return;
        s += to_string(root->val) + ",";

        preOrder(root->left, s);

        preOrder(root->right, s);
    }
    TreeNode *buildTree(vector<int> &arr, int left, int right)
    {
        if (left > right)
            return NULL;
        cout << arr[left] << " left = " << left << " right = " << right << endl;
        TreeNode *root = new TreeNode(arr[left]);
        int i = left + 1;
        while (left <= right && arr[i] < arr[right])
            i++;
        root->left = buildTree(arr, left + 1, i - 1);
        root->right = buildTree(arr, i, right);
        return root;
    }
};