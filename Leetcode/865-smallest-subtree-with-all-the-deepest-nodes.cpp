#include <iostream>
#include <algorithm>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
int depth(TreeNode *root)
{
    if (!root)
        return 0;
    int left = depth(root->left);
    int right = depth(root->right);
    return max(left, right) + 1;
}

TreeNode *subtreeWithAllDeepest(TreeNode *root)
{
    if (!root)
        return root;
    int left = depth(root->left);
    int right = depth(root->right);
    if (left == right)
        return root;
    else if (left < right)
    {
        return subtreeWithAllDeepest(root->left);
    }
    else
        return subtreeWithAllDeepest(root->right);
}