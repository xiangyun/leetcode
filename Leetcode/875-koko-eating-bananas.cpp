#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
using namespace std;

bool valid(vector<int> &piles, int H, int K)
{
    int delta = 0;
    for (auto &i : piles)
        delta += (i - 1) / K + 1;
    return delta <= H;
}

int minEatingSpeed(vector<int> &piles, int H)
{
    // // int delta = H - piles.size();
    // // auto cmp = [](const int &a, const int &b) {
    // //     return a > b;
    // // };
    // // sort(piles.begin(), piles.end(),cmp);
    // int sum = accumulate(piles.begin(), piles.end(), 0);
    // int K = sum / H;
    // int delta;
    // do
    // {
    //     delta = 0;
    //     for (auto &i : piles)
    //     {
    //         delta += i / K;
    //     }
    //     K++;
    // } while (delta == H);
    // return --K;
    int low = 1, high = pow(10, 9);
    while (low < high)
    {
        int mid = low + (high - low) / 2;
        if (!valid(piles, H, mid))
        {
            low = mid + 1;
        }
        else
            high = mid;
    }
    return low;
}