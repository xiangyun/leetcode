#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int fun(vector<int> &arr, int i, int j, int turn)
{
    if (i == j)
    {
        if (turn == 1)
            return arr[i];
        else
            return -arr[i];
    }
    if (turn == 1)
        return max(fun(arr, i + 1, j, -1) + arr[i], fun(arr, i, j - 1, -1) + arr[j]);
    else
        return min(fun(arr, i + 1, j, 1) - arr[i], fun(arr, i, j - 1, 1) - arr[j]);
}

bool PredictTheWinner(vector<int> &nums)
{
    return fun(nums, 0, nums.size() - 1, 1) >= 0;
}