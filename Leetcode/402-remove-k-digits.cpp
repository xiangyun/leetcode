#include <iostream>
#include <string>
using namespace std;
string removeKdigits(string num, int k)
{
    if (k == 0)
    {
        if (num.empty())
            return "0";
        return num;
    }
    if (num.size() >= 2 && num[1] == '0')
    {
        if (num.size() == 2)
        {
            return "0";
        }
        string s = string(num.begin() + 2, num.end());
        if (k == 1)
        {
            return s;
        }
        return removeKdigits(s, k - 1);
    }
    int index = 1;
    for (; index < num.size(); index++)
    {
        if (num[index] - '0' < num[index - 1] - '0')
            break;
    }
    if (index != num.size() - 1 || num[index] - '0' < num[index - 1] - '0')
    {
        num.erase(num.begin() + index - 1);
        return removeKdigits(num, k - 1);
    }
    else
    {
        num.erase(num.begin() + index);
        return removeKdigits(num, k - 1);
    }
}