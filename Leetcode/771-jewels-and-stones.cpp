#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;
int numJewelsInStones(string J, string S)
{
    unordered_map<char, int> hash;
    for (auto c : J)
        hash[c]++;
    int cnt = 0;
    for (auto c : S)
        if (hash.count(c))
            cnt++;
    return cnt;
}