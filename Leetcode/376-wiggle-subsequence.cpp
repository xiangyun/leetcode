#include <iostream>
#include <vector>
using namespace std;
int wiggleMaxLength(vector<int> &nums)
{
    if (nums.size() <= 1)
        return nums.size();
    bool flag = true;
    int ans = 0;

    int start = 1;
    while (start < nums.size() && nums[start] == nums[start - 1])
        start++;

    for (int i = start; i < nums.size(); i++)
    {
        if (nums[i] > nums[i - 1])
        {
            if (i == start || !flag)
            {
                ans++;
                flag = false;
            }
        }
        if (nums[i] < nums[i - 1])
        {
            if (i == start || flag)
            {
                ans++;
                flag = true;
            }
        }
    }
    return ans;
}