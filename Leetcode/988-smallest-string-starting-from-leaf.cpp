#include <iostream>
#include <vector>
#include <string>
using namespace std;
struct TreeNode
{
    TreeNode *left, *right;
    int val;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void visit(TreeNode *root, vector<string> &res, string s)
{
    if (!root)
    {
        return;
    }
    if(!root->left&&!root->right)
    {
        res.push_back(s);
        return;
    }
    s += 'a' + root->val;
    visit(root->left, res, s);
    visit(root->right, res, s);
}

string smallestFromLeaf(TreeNode *root)
{
    vector<string> res;
    string s = "", smallest;
    visit(root, res, s);
    if (res.empty())
        return "";
    smallest = string(res[0].rbegin(), res[0].rend());
    for (auto str : res)
    {
        str = string(str.rbegin(), str.rend());
        if (str < smallest)
            smallest = str;
    }
    return smallest;
}