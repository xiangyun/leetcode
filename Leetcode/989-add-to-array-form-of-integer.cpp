#include <iostream>
#include <vector>
using namespace std;
vector<int> addToArrayForm(vector<int> &A, int K)
{
    vector<int> t;
    int res = 0;
    while (K)
    {
        t.insert(t.begin(), K % 10);
        K /= 10;
    }
    while (A.size() > t.size())
    {
        t.insert(t.begin(), 0);
    }
    while (t.size() > A.size())
    {
        A.insert(A.begin(), 0);
    }
    for (int i = A.size() - 1; i >= 0; --i)
    {
        res += A[i] + t[i];
        t[i] = res % 10;
        res /= 10;
    }
    if (res > 0)
    {
        t.insert(t.begin(), res);
    }
    return t;
}