#include <iostream>
#include <vector>
#include <set>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
//class BinaryTree
void insert(TreeNode *root, int val)
{
    TreeNode *node = new TreeNode(val);
    if (root == NULL)
        root = node;
    if (root->val < val)
        insert(root->left, val);
    else
        insert(root->right, val);
}

TreeNode *createBinaryTree(vector<int> arr)
{
    if (arr.empty())
        return NULL;
    TreeNode *root = new TreeNode(arr[0]);
    for (int i = 1; i < arr.size(); i++)
        insert(root, arr[i]);
    return root;
}

void permutations(vector<int> &num, vector<vector<int>> &arr, int index)
{
    if (index >= num.size())
    {
        arr.push_back(num);
        return;
    }
    for (int i = index; i < num.size(); i++)
    {
        swap(num[index], num[i]);
        permutations(num, arr, index + 1);
        swap(num[index], num[i]);
    }
}
vector<vector<int>> permute(vector<int> &nums)
{
    vector<vector<int>> arr;
    permutations(nums, arr, 0);
    return arr;
}

vector<TreeNode *> generateTrees(int n)
{
    vector<int> arr;
    for(int i=0;i<n;i++)
    arr.push_back(i+1);
    vector<vector<int>> per=permute(arr);
    vector<TreeNode *> forest={NULL};
    for(int i=0;i<per.size();i++)
    {
        forest.push_back(createBinaryTree(per[i]));
    }
    return forest;
}