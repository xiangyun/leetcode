#include <iostream>
#include <vector>
using namespace std;
vector<vector<int>> generate(int numRows)
{
    if (numRows == 0)
        return vector<vector<int>>();
    if (numRows == 1)
        return vector<vector<int>>({{1}});
    if (numRows == 2)
        return vector<vector<int>>({{1}, {1, 1}});
    vector<vector<int>> res;
    res.push_back({1});
    res.push_back({1, 1});
    for (int i = 3; i <= numRows; i++)
    {
        vector<int> arr = res.back();
        vector<int> temp;
        temp.push_back(1);
        for (int j = 2; j <= i - 1; j++)
            temp.push_back(arr[j - 2] + arr[j - 1]);
        temp.push_back(1);
        res.push_back(temp);
    }
    return res;
}