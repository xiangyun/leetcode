#include <iostream>
#include <vector>
#include <string>
using namespace std;
string convertToBase7(int num)
{
    string s = "";
    int fac = 1;
    if (num == 0)
        return "0";
    if (num < 0)
    {
        num *= -1;
        fac = -1;
    }
    while (num != 0)
    {
        int res = num % 7;
        num /= 7;
        s = to_string(res) + s;
    }
    if (fac == -1)
        s = "-1" + s;
    return s;
}