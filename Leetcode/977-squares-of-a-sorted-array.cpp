#include <iostream>
#include <vector>
using namespace std;
vector<int> sortedSquares(vector<int> &A)
{
    int i = 0, j = A.size() - 1, k = A.size() - 1;
    vector<int> res(A.size());
    while (i <= j)
    {
        if (abs(A[i]) > abs(A[j]))
        {
            res[k] = A[i] * A[i];
            i++;
            k--;
        }
        else
        {
            res[k] = A[j] * A[j];
            j--;
            k--;
        }
    }
    return res;
}