#include <iostream>
#include <string>
#include <vector>
using namespace std;
/*
board =
[
  ['A','B','C','E'],
  ['S','F','C','S'],
  ['A','D','E','E']
]
Given word = "ABCCED", return true.
Given word = "SEE", return true.
Given word = "ABCB", return false.
*/

bool backtarcking(vector<vector<char>> board, string s, vector<vector<bool>> &mat, int cur_index, int i, int j)
{
    if (cur_index == s.length())
        return true;
    if (i < 0 || j < 0 || i >= board.size() || j >= board[0].size())
        return false;
    if (mat[i][j])
    {
        mat[i][j] = false;
        if (board[i][j] != s[cur_index])
            return false;
        bool flag = backtarcking(board, s, mat, cur_index + 1, i + 1, j) || backtarcking(board, s, mat, cur_index + 1, i - 1, j) || backtarcking(board, s, mat, cur_index + 1, i, j + 1) || backtarcking(board, s, mat, cur_index + 1, i, j - 1);
        mat[i][j] = true;
        return flag;
    }
    return false;
}

bool exist(vector<vector<char>> &board, string word)
{
    for (int i = 0; i < board.size(); i++)
        for (int j = 0; j < board[0].size(); j++)
        {
            vector<vector<bool>> mat(board.size(), vector<bool>(board[0].size(), true));
            if (backtarcking(board, word, mat, 0, i, j))
                return true;
        }
    return false;
}