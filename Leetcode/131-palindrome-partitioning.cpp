#include <iostream>
#include <vector>
#include <string>
using namespace std;
class Solution
{
  public:
    vector<vector<string>> partition(string s)
    {
        vector<vector<string>> res;
        vector<string> str;
        backtracking(res, str, s, 0);
        return res;
    }

  private:
    bool isPalindrome(string s)
    {
        return s == string(s.rbegin(), s.rend());
    }

    void backtracking(vector<vector<string>> &res, vector<string> str, string s, int index)
    {
        if (index >= s.size())
        {
            res.push_back(str);
            return;
        }

        for (int i = index + 1; i <= s.size(); i++)
        {
            string cur_str = s.substr(index, i - index);
            if (isPalindrome(cur_str))
            {
                str.push_back(cur_str);
                backtracking(res, str, s, i);
                str.pop_back();
            }
        }
    }
};