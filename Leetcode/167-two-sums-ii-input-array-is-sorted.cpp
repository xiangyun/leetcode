#include <iostream>
#include <vector>
using namespace std;
vector<int> twoSum(vector<int> &numbers, int target)
{
    int i = 0, j = numbers.size() - 1;
    while (i < j)
    {
        while (i < j && numbers[i] + numbers[j] < target)
            i++;
        while (i < j && numbers[i] + numbers[j] > target)
            j--;
        if (i < j && numbers[i] + numbers[j] == target)
        {
            return vector<int>({i + 1, j + 1});
        }
    }
    return vector<int>();
}