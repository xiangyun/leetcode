#include <iostream>
#include <string>
#include <vector>
using namespace std;

string solution(string s)
{
	int n = s.size();
	if(n==0)
		return "";
	if(n==1)
		return s;
	vector<vector<bool>> dp(n, vector<bool>(n, false));
	int maxLen = 0;
	int left = 0;
	for (int i = 0; i < n; i++)
	{
		dp[i][i] = true;
		for (int j = 0; j < i; j++)
		{
			if (s[i] == s[j])
			{
				if ((i - j) < 2)
					dp[j][i] = true;
				else
					dp[j][i] = dp[j + 1][i - 1];
			}
			if (dp[j][i])
			{
				//cout << s.substr(j, i - j + 1) << endl;
				if (maxLen < i - j + 1)
				{
					maxLen = i - j + 1;
					left = j;
				}
			}
		}
	}
	return s.substr(left, maxLen);
}


int main()
{
	string s;
	cin >> s;
	cout << solution(s) << endl;
	return 0;
}