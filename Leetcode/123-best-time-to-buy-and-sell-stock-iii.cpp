#include <iostream>
#include <vector>
using namespace std;

/*
Design an algorithm to find the maximum profit. You may complete at most two transactions.
*/
int maxProfit(vector<int> &prices, int start, int end)
{
    int profit = 0, minPrice = INT_MAX;
    for (int i = start; i < end; i++)
    {
        if (minPrice > prices[i])
            minPrice = prices[i];
        if (prices[i] - minPrice > profit)
            profit = prices[i] - minPrice;
    }
    return profit;
}
int maxProfit(vector<int> &prices)
{
    if (prices.empty())
        return 0;
    int profit = 0;
    int n = prices.size();
    for (int i = 0; i < prices.size(); i++)
    {
        int temp = maxProfit(prices, 0, i) + maxProfit(prices, i, n - 1);
        if (temp > profit)
            profit = temp;
    }
    return profit;
}