#include <iostream>
#include <vector>
using namespace std;

void sortColor(vector<int> &nums)
{
    int zero_index = 0;
    int two_index = nums.size() - 1;
    int i = 0;
    while (i != two_index)
    {
        if (nums[i] == 0)
        {
            swap(nums[i], nums[zero_index]);
            i++;
            zero_index++;
        }
        else if (nums[i] == 2)
        {
            swap(nums[i], nums[two_index]);
            two_index--;
        }
        else
            i++;
    }
}