#include <iostream>
#include <string>
using std::string;
class TrieNode
{

  public:
    TrieNode()
    {
        memset(next, NULL, sizeof(next));
        isWord = false;
    }
    ~TrieNode()
    {
        for (int i = 0; i < 26; i++)
            if (next[i])
                delete next[i];
    }

    bool isWord;
    TrieNode *next[26];
};
class Trie
{
  public:
    Trie()
    {
        root = new TrieNode();
    }
    ~Trie()
    {
        delete root;
    }
    void insert(string word)
    {
        TrieNode *p = root;
        for (auto c : word)
        {
            if (p->next[c - 'a'])
                p = p->next[c - 'a'];
            else
            {
                p->next[c - 'a'] = new TrieNode();
                p = p->next[c - 'a'];
            }
        }
        p->isWord = true;
    }
    bool search(string word)
    {
        TrieNode *p = root;
        for (int i = 0; i < word.size() && p; i++)
        {
                p = p->next[word[i] - 'a'];
        }
        return p&&p->isWord;
    }
    bool startwith(string prefix)
    {
        TrieNode *p = root;
        for (int i = 0; i < prefix.size() && p; i++)
        {
                p = p->next[prefix[i] - 'a'];
        }
        return p;
    }

  private:
    TrieNode *root;
};