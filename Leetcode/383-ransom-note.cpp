#include <iostream>
#include <unordered_map>
using namespace std;
bool canConstruct(string ransomNote, string magazine)
{
    unordered_map<char, int> hash;
    for (auto &c : magazine)
    {
        hash[c]++;
    }
    for (auto &c : ransomNote)
    {
        if (hash[c] <= 0)
            return false;
        hash[c]--;
    }
    return true;
}