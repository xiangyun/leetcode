#include <iostream>
#include <vector>
#include <cassert>
using namespace std;

class UnionFind
{
  public:
    UnionFind(int n)
    {
        parent = new int[n];
        rank = new int[n];
        count = n;
        for (int i = 0; i < n; i++)
        {
            parent[i] = i;
            rank[i] = 1;
        }
    }
    ~UnionFind()
    {
        delete[] parent;
        delete[] rank;
    }
    int find(int x)
    {
        //assert(x >= 0 && x < count);
        if (x != parent[x])
            parent[x] = find(parent[x]);
        return parent[x];
    }
    void Union(int x, int y)
    {
        int fx = find(x);
        int fy = find(y);
        if (fx == fy)
            return;
        if (rank[fx] > rank[fy])
        {
            parent[fy] = fx;
            rank[fx] += rank[fy];
        }
        else
        {
            parent[fx] = fy;
            rank[fy] += rank[fx];
        }
        count--;
    }
    int size()
    {
        return count;
    }

  private:
    int *parent;
    int *rank;
    int count;
};

class Solution
{
  public:
    int findCircleNum(vector<vector<int>> &M)
    {
        if (M.empty())
            return 0;
        int n = M.size();
        UnionFind uf(n);
        for (int i = 0; i < M.size(); i++)
        {
            for (int j = i + 1; j < M[0].size(); j++)
            {
                if (M[i][j] == 1)
                {
                    uf.Union(i, j);
                }
            }
        }
        return uf.size();
    }
};

int main()
{
}