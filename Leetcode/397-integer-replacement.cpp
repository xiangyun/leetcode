#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
/*
int integerReplacement(int n)
{
	vector<int> dp(n + 1, 0);
	dp[1] = 0;
	for (int i = 2; i <= n; i++)
	{
		if (i %2==0)
		{
			dp[i] = 1 + dp[i / 2];
		}
		else
			dp[i] = min(dp[(i + 1) / 2], dp[(i - 1) / 2]) + 2;
	}
	return dp[n];
}*/
int integerReplacement(int n)
{
    if (n <= 1)
        return 0;
    if (n % 2 == 0)
        return integerReplacement(n / 2) + 1;
    else
        return min(integerReplacement((n + 1) / 2), integerReplacement((n - 1) / 2)) + 2;
}