#include <iostream>
#include <map>
#include <vector>
using namespace std;
class Solution{
    public:
    vector<int> twoSum(vector<int> &number, int target)
    {
	    vector<int> results;
	    map<int, int> m;
	    for (int i = 0; i < number.size(); i++)
	    {
		    m.insert(make_pair(number[i], i));
	    }
	    for (int i = 0; i < number.size(); i++)
	    {
		    int search = target - number[i];
		    if (m.find(search) != m.end() && i != m[search])
            {
			    results.push_back(i + 1);
		        results.push_back(m[search] + 1);
		        break;
            }
	    }
	    return results;
    }
}