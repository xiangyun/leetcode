#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>

using namespace std;
int projectArea(vector<vector<int>> &grid)
{
    int ans = 0;
    int maxvalrow = INT_MIN;
    int maxvalcol = INT_MIN;
    for (int i = 0; i < grid.size(); i++)
    {
        for (int j = 0; j < grid[i].size(); j++)
        {
            if (grid[i][j])
                ans++;
            maxvalcol = max(maxvalcol, grid[i][j]);
            maxvalrow = max(maxvalrow, grid[j][i]);
        }
        ans += maxvalcol;
        ans += maxvalrow;
        maxvalrow = INT_MIN;
        maxvalcol = INT_MIN;
    }
    return ans;
}