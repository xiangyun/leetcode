#include <iostream>
#include <string>
#include <vector>
using namespace std;
/*
kmp algorithm
p[k]==p[j]
next[j+1]=next[j]+1=k+1
p[k]!=p[j]
*/
vector<int> getNext(string s)
{
	vector<int> next(s.length());
	if (s.length() == 0)
		return next;
	next[0] = -1;
	for (int j = 0; j < s.length() - 1; j++)
	{
		int k = next[j];
		if (s[j] == s[k])
			next[j + 1] = next[j] + 1;
		else {
			while (k != 0)
			{
				k = next[k];
				if (s[j] == s[k])
				{
					next[j + 1] = k + 1;
					break;
				}
			}
			if (k == 0)
			{
				if (s[j] == s[0])
					next[j + 1] = 1;
				else
					next[j + 1] = 0;
			}
		}
	}
	return next;
}

int KMP(string s1, string s2)
{
	vector<int> next = getNext(s2);
	int i = 0, j = 0;
	while (i < s1.length() && j < s2.length()) {

		if (j == -1||s1[i]==s2[j])
		{
			i++;
			j++;
		}
		else {
			j = next[j];
		}
	}
	if (j == s2.length())
		return i - j;
	else
		return -1;
}
int strStr(string haystack, string needle) {
	return KMP(haystack, needle);
}