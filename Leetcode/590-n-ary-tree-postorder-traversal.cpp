#include <iostream>
#include <vector>
using namespace std;
class Node
{
  public:
    int val;
    vector<Node *> childern;
    Node() {}
    Node(int _val, vector<Node *> _childern) : val(_val), childern(_childern) {}
};

void post(Node *root, vector<int> &arr)
{
    if (!root)
        return;
    for (int i = 0; i < root->childern.size(); i++)
    {
        post(root->childern[i], arr);
    }
    arr.push_back(root->val);
}

vector<int> postorder(Node *root)
{
    vector<int> arr;
    post(root,arr);
    return arr;
}