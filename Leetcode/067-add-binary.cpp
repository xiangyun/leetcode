#include <iostream>
#include <algorithm>
using namespace std;
string addBinary(string a, string b)
{
    string ans = "";
    int n = max(a.length(), b.length());
    while (a.length() < n)
        a = '0' + a;
    while (b.length() < n)
        b = '0' + b;
    int j = 0;
    for (int i = n - 1; i >= 0; i--)
    {
        j += a[i] - '0' + b[i] - '0';
        ans = char('0' + j % 2) + ans;
        j /= 2;
    }
    if (j > 0)
        ans = char('0' + j) + ans;
    return ans;
}
/*
string add(string a, string b)
{
    string ans = "";
    int n = max(a.length(), b.length());
    while (a.length() < n)
        a = '0' + a;
    while (b.length() < n)
        b = '0' + b;
    int j = 0;
    for (int i = n - 1; i >= 0; i--)
    {
        j += a[i] - '0' + b[i] - '0';
        ans = char('0' + j % 10) + ans;
        j /= 10;
    }
    if (j > 0)
        ans = char('0' + j) + ans;
    return ans;
}*/