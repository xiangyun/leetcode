#include <vector>
#include <iostream>
#include <algorithm>
#include <unordered_map>
using namespace std;
bool canReorderDoubled(vector<int> &A)
{
	sort(A.begin(), A.end(), [](const int &a, const int &b) { return abs(a) < abs(b); });
	unordered_map<int, int> hash;
	for (auto &i : A)
		hash[i]++;
	for (auto &i : A)
	{
		if (hash[i])
		{
			if (hash[i] > hash[2 * i])
				return false;
			hash[i]--;
			hash[2 * i]--;
		}
	}
	return true;
}