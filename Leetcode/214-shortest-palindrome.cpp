#include <iostream>
#include <string>
#include <vector>
using namespace std;

vector<int> getNext(string s)
{
    int n = s.size();
    vector<int> next(n);
    next[0] = -1;
    int k = -1, j = 0;
    while (j < n - 1)
    {
        if (k == -1 || s[j] == s[k])
        {
            k++;
            j++;
            next[j] = k;
        }
        else
        {
            k = next[k];
        }
    }
    return next;
}

string shortestPalindrome(string s)
{
    if (s.size() <= 1)
        return s;
    string rev = s;
    std::reverse(rev.begin(), rev.end());
    string L = s + "#" + rev;
    vector<int> next = getNext(L);
    return rev.substr(0, s.size() - next.back() - 1) + s;
}