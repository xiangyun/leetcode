#include <iostream>
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};
ListNode *reverseLinkedList(ListNode *head)
{
    ListNode *pre, *cur = head, *next;
    pre = NULL;
    while (cur)
    {
        next = cur->next;
        cur->next = pre;
        pre = cur;
        cur = next;
    }
    return pre;
}

ListNode *findMiddle(ListNode *head)
{
    if (!head || !head->next)
        return head;
    ListNode *fast = head, *slow = head->next;
    while (fast && fast->next && slow)
    {
        fast = fast->next->next;
        slow = slow->next;
    }
    return slow;
}

bool isPalindrome(ListNode *head)
{
    ListNode *middle = findMiddle(head);
    middle->next = reverseLinkedList(middle->next);
    ListNode *cur = middle->next, *node = head;
    while (cur)
    {
        if (cur->val != node->val)
            return false;
        cur = cur->next;
        node = node->next;
    }
    return true;
}