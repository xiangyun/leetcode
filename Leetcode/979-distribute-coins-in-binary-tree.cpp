
#include <iostream>
using namespace std;
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

struct TreeNode
{
    int val;
    TreeNode *left, *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
class Solution
{
  public:
    int dfs(TreeNode *root, int &sum)
    {
        if (!root)
            return 0;
        int left = dfs(root->left, sum);
        int right = dfs(root->right, sum);
        sum += abs(left) + abs(right);
        return root->val + left + right - 1;
    }

    int distributeCoins(TreeNode *root)
    {
        int sum  = 0;
        dfs(root,sum);
        return sum;
    }
};