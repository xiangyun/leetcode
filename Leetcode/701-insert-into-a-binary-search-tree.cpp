#include <iostream>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
void insert(TreeNode *&root, int val)
{
    if (!root)
        root = new TreeNode(val);
    else
    {
        if (root->val > val)
            insert(root->left, val);
        else
            insert(root->right, val);
    }
}

TreeNode *insertIntoBST(TreeNode *root, int val)
{
    insert(root, val);
    return root;
}