#include <iostream>
#include <vector>
#include <string>
using namespace std;
string addStrings(string num1, string num2)
{
    while (num1.size() < num2.size())
    {
        num1 = '0' + num1;
    }
    while (num1.size() > num2.size())
    {
        num2 = '0' + num2;
    }
    string val = "";
    int res = 0;
    for (int i = num1.size() - 1; i >= 0; i--)
    {
        int temp = num1[i] - '0' + num2[i] - '0';
        res = res + temp;
        val = to_string(res % 10) + val;
        res /= 10;
    }
    if (res != 0)
        val = to_string(res) + val;
    return val;
}