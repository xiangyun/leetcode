#include <iostream>
#include <vector>

using namespace std;

int numSubarrayWithSum(vector<int> &A, int S)
{
    int i = 0, j = 0, count = 0, sum = 0;
    for (; j < A.size(); j++)
    {
        sum += A[j];
        if (sum == S)
            break;
    }
    if (j == A.size() - 1 && sum < S)
        return 0;
    count++;
    while (j != A.size() - 1 && i != 1)
    {
        while (j < A.size() - 1 && A[j + 1] == 0)
        {
            j++;
            count++;
        }
        while (i<=j&&A[i+1]==0)
        {
            i++;count++;
        }
    }
}