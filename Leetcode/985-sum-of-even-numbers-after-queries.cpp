#include <iostream>
#include <vector>
using namespace std;
vector<int> sumEvenAfterQueries(vector<int> &A, vector<vector<int>> &queries)
{
    int sum = 0;
    for (auto i : A)
    {
        if (i % 2 == 0)
            sum += i;
    }
    vector<int> res;
    for (int i = 0; i < queries.size(); i++)
    {
        int index = queries[i][1], val = queries[i][0];
        if (A[index] % 2 == 0)
        {
            if (val % 2 == 0)
                sum += val;
            else
                sum -= A[index];
        }
        else
        {
            if (val % 2 == 1)
                sum += A[index] + val;
        }
        A[index] += val;
        res.push_back(sum);
    }
    return res;
}