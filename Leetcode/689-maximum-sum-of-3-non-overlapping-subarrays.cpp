#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
using namespace std;
class Solution
{
  public:
    bool canPartitionKSubsets(vector<int> &nums, int k)
    {
        if (k == 1)
            return true;
        int sum = accumulate(nums.begin(), nums.end(), 0);
        if (sum % k != 0)
            return false;
        int target = sum / k;
        sort(nums.begin(), nums.end(), [](const int &a, const int &b) { return a > b; });
        if (nums.back() > target)
            return false;
        vector<bool> visited(nums.size(), false);
        return backtracking(nums, visited, k, target, 0, 0);
    }

  private:
    bool backtracking(vector<int> &arr, vector<bool> &visited, int k, int target, int cur_index, int cur_sum)
    {
        if (k == 1)
            return true;
        if (cur_sum == target)
            return backtracking(arr, visited, k - 1, target, 0, 0);
        for (int i = cur_index; i < arr.size(); i++)
        {
            if (!visited[i])
            {
                visited[i] = true;
                if (backtracking(arr, visited, k, target, i + 1, cur_sum + arr[i]))
                    return true;
                visited[i] = false;
            }
        }
        return false;
    }
};