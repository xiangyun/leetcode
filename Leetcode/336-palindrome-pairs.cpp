// #include <iostream>
// #include <vector>
// #include <string>
// // #include <stdio.h>
// using namespace std;
// struct TrieNode
// {
//     //bool isWord;
//     int index;
//     TrieNode *next[26];
//     TrieNode()
//     {
//         index = -1;
//         // isWord = false;
//         memset(next, NULL, sizeof(next));
//     }
//     ~TrieNode()
//     {
//         for (int i = 0; i < 26; i++)
//             if (next[i])
//                 delete next[i];
//     }
// };

// class TrieTree
// {
//   public:
//     TrieTree()
//     {
//         root = new TrieNode();
//     }
//     ~TrieTree()
//     {
//         delete root;
//     }
//     void insert(string s, int index)
//     {
//         // TrieNode *p = root;
//         // for (auto &c : s)
//         // {
//         // if(!p->next[c-'a'])
//         // {
//         // p->next[c - 'a'] = new TrieNode();
//         // }
//         // p = p->next[c - 'a'];
//         // }
//         // p->index = index;
//         TrieNode *p = root;
//         for (auto it = s.rbegin(); it != s.rend(); it++)
//         {
//             if (!p->next[*it - 'a'])
//                 p->next[*it - 'a'] = new TrieNode();
//             p = p->next[*it - 'a'];
//         }
//         p->index = index;
//     }
//     int search(string s)
//     {
//         TrieNode *p = root;
//         for (int i = 0; i < s.size() && p; i++)
//         {
//             p = p->next[s[i] - 'a'];
//         }
//         if (!p)
//             return -1;
//         return p->index;
//     }

//   private:
//     TrieNode *root;
// };

// // bool isPalindrome(string s)
// // {
// //     return s == string(s.rbegin(), s.rend());
// // }
// class Solution
// {
//   public:
//     vector<vector<int>> palindromePairs(vector<string> &words)
//     {
//         TrieTree trie = TrieTree();
//         vector<vector<int>> res;
//         for (int i = 0; i < words.size(); i++)
//         {
//             trie.insert(words[i], i);
//         }
//         for (int i = 0; i < words.size(); i++)
//         {
//             for (int j = 0; j < words[i].size(); j++)
//             {
//                 string s(words[i].begin(), words[i].begin() + j);
//                 int flag = trie.search(s);
//                 if (flag != -1 && flag != i)
//                 {
//                     res.push_back(vector<int>({i, flag}));
//                 }
//             }
//         }
//         return res;
//     }
// };