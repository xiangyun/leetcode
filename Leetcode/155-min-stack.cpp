#include <iostream>
#include <stack>
using namespace std;
class MinStack
{
  public:
    MinStack();
    ~MinStack();
    void push(int x)
    {
        if (stk.empty())
            stk.push(make_pair(x, x));
        else if (stk.top().second < x)
        {
            int y = stk.top().second;
            stk.push(make_pair(x, y));
        }
        else
            stk.push(make_pair(x, x));
    }
    void pop()
    {
        stk.pop();
    }
    int top()
    {
        return stk.top().first;
    }
    int getMin()
    {
        return stk.top().second;
    }

  private:
    stack<pair<int, int>> stk;
};