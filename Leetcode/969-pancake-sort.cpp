#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

// int findMax(vector<int> &arr, int n)
// {
//     int mi, i;
//     for (mi = 0, i = 0; i < n; ++i)
//         if (arr[i] > arr[mi])
//             mi = i;
//     return mi;
// }
// void flip(vector<int> &arr, int i)
// {
//     int temp, start = 0;
//     while (start < i)
//     {
//         temp = arr[start];
//         arr[start] = arr[i];
//         arr[i] = temp;
//         start++;
//         i--;
//     }
// }
// vector<int> pancakeSort(vector<int> &A)
// {
//     int n = A.size();
//     vector<int> res;
//     for (int cur_size = n; cur_size > 1; --cur_size)
//     {
//         int mi = findMax(A, cur_size);
//         if (mi != cur_size - 1)
//         {
//             flip(A, mi);
//             res.push_back(mi + 1);
//             flip(A, cur_size - 1);
//             res.push_back(cur_size);
//         }
//     }
//     return res;
// }
int findMax(vector<int> &arr, int cur_size)
{
    int index = 0;
    for (int i = 0; i < cur_size; i++)
    {
        if (arr[i] > arr[index])
            index = i;
    }
    return index;
}
vector<int> pancakeSort(vector<int> &arr)
{
    vector<int> res;
    for (int i = arr.size() - 1; i >= 1; i--)
    {
        int cur_index = findMax(arr, i + 1);
        if (cur_index != i)
        {
            res.push_back(cur_index + 1);
            reverse(arr.begin(), arr.begin() + cur_index + 1);
            res.push_back(i + 1);
            reverse(arr.begin(), arr.begin() + i + 1);
        }
    }
    return res;
}