#include <iostream>
#include <vector>
using namespace std;

// struct TrieNode{
//     TrieNode * next[26];
//     bool isWord ;
//     TrieNode(){
//         memset(next,NULL,sizeof(next));
//         isWord = false;
//     }
//     ~TrieNode()
//     {
//         for(int i=0;i<26;i++)
//         {
//             if(next[i])
//             delete next[i];
//         }
//     }
// };
bool isRectangleOverlap(vector<int> &rec1, vector<int> &rec2)
{
    return rec1[0] < rec2[2] && rec2[0] < rec1[2] && rec1[1] < rec2[3] && rec2[1] < rec1[3];
}