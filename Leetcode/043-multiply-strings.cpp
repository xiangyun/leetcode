#include <iostream>
#include <string>
using namespace std;

string multiply(string num1, string num2)
{
    if (!num1.length() || !num2.length())
        return "0";
    int num_1 = 0, num_2 = 0;
    for (int i = num1.length() - 1; i >= 0; i--)
        num_1 = num_1 * 10 + num1[i] - '0';
    for (int j = num2.length() - 1; j >= 0; j--)
        num_2 = num_2 * 10 + num2[j] - '0';
    return to_string(num_1 * num_2);
}