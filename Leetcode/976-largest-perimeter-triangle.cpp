#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int largestPerimeter(vector<int> &A)
{
    sort(A.begin(), A.end());
    int count = 0, size = A.size();
    int max_len = INT_MIN;
    int i = A.size() - 1;
    while (i >= 2)
    {
        if (A[i - 1] + A[i - 2] > A[i])
        {
            max_len = A[i - 1] + A[i - 2] + A[i];
            break;
        }
        else
            i--;
    }
    return max_len == INT_MIN ? 0 : max_len;
}