#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <queue>
using namespace std;
int largestSumAfterKNegations(vector<int> &A, int K)
{
    auto cmp = [](const int &a, const int &b) {
        return a > b;
    };
    priority_queue<int, vector<int>, decltype(cmp)>
        heap(cmp);
    for (auto i : A)
        heap.push(i);
    for (int i = 0; i < K; ++i)
    {
        int t = heap.top();
        t *= -1;
        heap.pop();
        heap.push(t);
    }
    int sum = 0;
    while (!heap.empty())
    {
        sum += heap.top();
        heap.pop();
    }
    return sum;
}