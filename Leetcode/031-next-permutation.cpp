/*
void permutation(int array[],int len,int index){
    if(index==len){
        ++sum;
        print(array,len);
    }
    else
        for(int i=index;i<len;++i){
            swap(array,index,i);
            permutation(array,len,index+1);
            swap(array,index,i);
        }
}
*/

/*
no next permutation for descending order
*/
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
void nextPermutation(vector<int> &num)
{
    int size = num.size();
    int pivot = size - 1;
    while(pivot>0)
    {
        if(num[pivot]>num[pivot-1])
        {
            int j = size - 1;
            while(j>=pivot)
            {
                if(num[j]>num[pivot-1])
                {
                    swap(num[j], num[pivot-1]);
                    break;
                }
                j--;
            }
            sort(num.begin() + pivot, num.end());
            break;
        }
        pivot--;
    }
    if(pivot==0)
        sort(num.begin(), num.end());
}