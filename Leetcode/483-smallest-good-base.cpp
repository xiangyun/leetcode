#include <iostream>
#include <vector>
#include <string>
#include <math.h>
using namespace std;
string smallestGoodBase(string n)
{
    using ull = unsigned long long;
    ull num = stoll(n);

    for (ull x = log(num + 1) / log(2); x >= 2; --x)
    {
        ull l = 2, r = pow(num + 1, 1.0 / (x - 1)) + 1;
        while (l < r)
        {
            ull sum = 0, base = l + (r - 1) / 2, val = 1;
            for (int i = 0; i < x; i++, val *= base)
                sum += val;
            if (sum == num)
                return to_string(base);
            else if (sum < num)
                l = base + 1;
            else
                r = base;
        }
    }
    return to_string(num - 1);
}