#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
// $c_0 * x^0 + c_1 * x^1 +...+ c_n * x^ n$,$x^0 = x/x$
// $target = parts[0] * x^0+parts[1]* x^1+...+parts[31]*x^{31}$
// $dp[i][0]$: minmmum operations needed to form an expression
// - $parts[i]*x^i + parts[i+1] * x^{(i+1)}+...+parts[31]*x^{31}$

// $dp[i][1]$: minmmum operations needed to form an expression
// - $(parts[i] + 1)*x^i + parts[i+1] * x^{(i+1)}+...+parts[31]*x^{31}$

// to generate $part[i]*x^i$
// - $x^i + x^i + ... + x^i$
// - $x^(i+1) - x^i - x^i - ... - x^i$
int leastOpsExpressTarget(int x, int target)
{
    vector<int> res;
    while (target)
    {
        res.push_back(target % x);
        target /= x;
    }
    int n = res.size();
    vector<int> l(n), r(n);
    l[0] = res[0] * 2;
    r[0] = (x - res[0]) * 2;
    for (int i = 1; i < n; i++)
    {
        l[i] = min(i * res[i] + l[i - 1], i * (res[i] + 1) + r[i - 1]);
        r[i] = min(i * (x - res[i]) + l[i - 1], i * (x - res[i] - 1) + r[i - 1]);
    }
    return min(l[n - 1], r[n - 1] + n) - 1;
}