#include <iostream>
using namespace std;
bool isPerfectSquare(int num)
{
    if (num == 1)
        return true;
    int left = 1, right = num / 2;
    while (left <= right)
    {
        int mid = (left + right) / 2;
        int midsqr = mid * mid;
        if (midsqr < num)
            left = mid + 1;
        else if (midsqr > num)
            right = mid - 1;
        else
            return true;
    }
    return false;
}