#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;
bool isAnagram(string s, string t)
{
	string s1, t1;
	if (s.length() > t.length())
	{
		s1 = t; t1 = s;
	}
	else
	{
		s1 = s; t1 = t;
	}
	unordered_map<char, int> hash;
	for (auto c : s1)
		hash[c]++;
	for (auto c : t1)
	{
		if (hash[c] == 0)
			return false;
		else
			hash[c]--;
	}
	return true;
}