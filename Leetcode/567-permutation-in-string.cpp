#include <iostream>
#include <vector>
#include <unordered_map>
#include <string>
using namespace std;
bool checkInclusion(string s1, string s2)
{
    if (s1.size() > s2.size())
        return false;
    vector<int> hash_1(256), hash_2(256);
    for (int i = 0; i < s1.size(); i++)
    {
        hash_1[s1[i]]++;
        hash_2[s2[i]]++;
    }
    if (hash_1 == hash_2)
        return true;
    for (int i = s1.size(); i < s2.size(); i++)
    {
        hash_2[s2[i]]++;
        hash_2[s2[i - s1.size()]]--;
        if (hash_1 == hash_2)
            return true;
    }
    return false;
}