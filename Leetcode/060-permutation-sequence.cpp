#include <iostream>
#include <string>
#include <vector>
using namespace std;
/*
int count=0;
void permutation(vector<int> &arr, int k, int index, string &s)
{
    if (index >= arr.size())
    {
        count++;
        if (count == k)
        {
            for (int i = 0; i < arr.size(); i++)
                s += to_string(arr[i]);
            return;
        }
        return;
    }
    for (int i = index; i < arr.size(); i++)
    {
        swap(arr[i], arr[index]);
        permutation(arr, k, index + 1, s);
        swap(arr[i], arr[index]);
    }
}

string getPermutation(int n, int k)
{
    vector<int> arr;
    for (int i = 0; i < n; i++)
        arr.push_back(i + 1);
    string s = "";
    permutation(arr, k, 0, s);
    return s;
}
*/
int fac(int n)
{
    if (n == 0 || n == 1)
        return 1;
    return n * fac(n - 1);
}
string getPermutation(int n, int k)
{
	string s = "";
	vector<int> arr;
	for (int i = 0; i < n; i++)
		arr.push_back(i + 1);
	int num = n - 1;
	while (num>=0)
	{
		int temp = fac(num);
		int q = k / temp;
		if (q != 0)
		{
			s += to_string(arr[q]);
			arr.erase(arr.begin() + q);
		}
		else
		{
			s += to_string(arr[0]);
			arr.erase(arr.begin());
		}
		k = k % temp;
		num--;
	}
	return s;
}