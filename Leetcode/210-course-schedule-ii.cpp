#include <iostream>
#include <vector>
#include <list>
#include <queue>
using namespace std;
vector<int> findOrder(int numCourse, vector<pair<int, int>> &prerequisites)
{
    vector<int> res;
    int *inDegree = new int[numCourse];
    list<int> *adjList = new list<int>[numCourse];
    for (int i = 0; i < numCourse; i++)
        inDegree[i] = 0;
    for (auto it : prerequisites)
    {
        inDegree[it.first]++;
        adjList[it.second].push_back(it.first);
    }
    queue<int> vec;
    for (int i = 0; i < numCourse; i++)
        if (inDegree[i] == 0)
            vec.push(i);
    while (!vec.empty())
    {
        int node = vec.front();
        res.push_back(node);
        vec.pop();
        for (auto it = adjList[node].begin(); it != adjList[node].end(); it++)
        {
            inDegree[*it]--;
            if (inDegree[*it] == 0)
                vec.push(*it);
        }
    }
    delete[] inDegree;
    delete[] adjList;
    if (res.size() == numCourse)
        return res;
    else
        return vector<int>();
}