#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left, *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void helper(TreeNode *root, int &pre, int &min_diff)
{
    if (!root)
        return;
    helper(root->left, pre, min_diff);
    // cout<<pre<<" "<<root->val<<endl;
    if (pre != -1)
        min_diff = min(abs(root->val - pre), min_diff);
    pre = root->val;
    helper(root->right, pre, min_diff);
}

int getMinimumDifference(TreeNode *root)
{
    int min_diff = INT_MAX;
    int pre = -1;
    helper(root, pre, min_diff);
    return min_diff;
}