#include <iostream>
#include <vector>
#include <unordered_set>
using namespace std;

// a^b = c -> a^c=b, b^c=a
// https://kingsfish.github.io/2017/12/15/Leetcode-421-Maximum-XOR-of-Two-Numbers-in-an-Array/s
class Solution
{
  public:
    int findMaximumXOR(vector<int> &nums)
    {
        int max = 0;
        int mask = 0;
        for (int i = 31; i >= 0; i--)
        {
            mask = mask | (1 << i);
            unordered_set<int> set;
            for (auto &num : nums)
            {
                set.emplace(mask & num);
            }
            int temp = max | (1 << i);
            for (auto &pre : set)
            {
                if (set.count(temp ^ pre))
                {
                    max = temp;
                    break;
                }
            }
        }
        return max;
    }
};