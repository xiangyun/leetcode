#include <iostream>
#include <vector>
using namespace std;
int mirrorReflection(int p, int q)
{
    while (p % 2 == 0 && q % 2 == 0)
    {
        p = p / 2;
        q = q / 2;
    }
    if (p % 2 == 0 && q % 2 == 1)
        return 0;
    else if (p % 2 == 1 && q % 2 == 1)
        return 1;
    else
        return 2;
}