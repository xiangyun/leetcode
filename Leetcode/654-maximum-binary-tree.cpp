#include <iostream>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

int findMaxIndex(vector<int> &nums, int start, int end)
{
    if (start == end)
        return start;
    int max_index = start;
    for (int i = start; i <= end; i++)
    {
        if (nums[max_index] < nums[i])
            max_index = i;
    }
    return max_index;
}

TreeNode *buildTree(vector<int> nums, int start, int end)
{
    if (start > end)
        return NULL;
    int max_index = findMaxIndex(nums, start, end);
    TreeNode *root = new TreeNode(nums[max_index]);
    root->left = buildTree(nums, start, max_index - 1);
    root->right = buildTree(nums, max_index + 1, end);
    return root;
}

TreeNode *constructMaximumBinaryTree(vector<int> &nums)
{
    return buildTree(nums, 0, nums.size() - 1);
}
