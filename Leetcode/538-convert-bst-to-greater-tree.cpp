#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left, *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void helper(TreeNode *&root, int &pre)
{
    if (!root)
        return;
    helper(root->right, pre);
    if (pre != -1)
        root->val += pre;
    pre = root->val;
    helper(root->left, pre);
}

TreeNode *convertBST(TreeNode *root)
{
    int pre = -1;
    helper(root, pre);
    return root;
}