#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

int height(TreeNode *root)
{
    if (!root)
        return 0;
    int left = 1 + height(root->left);
    int right = 1 + height(root->right);
    return left > right ? left : right;
}

void backtracking(TreeNode *root, vector<vector<string>> &res, int left, int right, int level)
{
    if (!root)
        return;
    if (left > right)
        return;
    int mid = (left + right) >> 1;
    res[level][mid] = to_string(root->val);
    backtracking(root->left, res, left, mid - 1, level + 1);
    backtracking(root->right, res, mid + 1, right, level + 1);
}

vector<vector<string>> printTree(TreeNode *root)
{
    if (!root)
        return vector<vector<string>>();
    int H = height(root);
    int size = (1 << H) - 1;
    vector<vector<string>> res(H, vector<string>(size, ""));
    backtracking(root, res, 0, size - 1, 0);
    return res;
}