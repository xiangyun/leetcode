#include <iostream>
using namespace std;
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

void push(ListNode *head, int val)
{
    ListNode *node = new ListNode(val);
    node->next = head;
    head = node;
}

ListNode *reverseList(ListNode *head)
{
    if (!head || !head->next)
        return head;
    ListNode *pre = head, *cur = head->next, *next;
    pre->next = NULL;
    while (cur)
    {
        next = cur->next;
        cur->next = pre;
        pre = cur;
        cur = next;
    }
    return pre;
}