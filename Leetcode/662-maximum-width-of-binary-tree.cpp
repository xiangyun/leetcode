#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <queue>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

// int depth(TreeNode *root)
// {
//     if (!root)
//         return 0;
//     int left = depth(root->left);
//     int right = depth(root->right);
//     return max(left, right) + 1;
// }

// void dfs(TreeNode *root, int depth, vector<vector<string>> &res, int max_depth)
// {
//     if (depth == res.size())
//         res.push_back(vector<string>());
//     max_depth = max(max_depth, depth);
//     if (!root)
//     {
//         if (depth + 1 < max_depth)
//             res[depth].push_back("NULL");
//         return;
//     }
//     res[depth].push_back(to_string(root->val));
//     dfs(root->left, depth + 1, res, max_depth);
//     dfs(root->right, depth + 1, res, max_depth);
// }

// int widthOfBinaryTree(TreeNode *root)
// {
//     int max_depth = depth(root);
//     vector<vector<string>> res;
//     dfs(root, 0, res, max_depth);
//     int max_width=INT_MIN;
//     for(auto & i : res)
//     {
//         max_width = max(max_width,static_cast<int>(i.size()));
//     }
//     return max_width;
// }

int widthOfBinaryTree(TreeNode *root)
{
    if (!root)
        return 0;
    queue<pair<TreeNode *, int>> q;
    q.emplace(root, 0);
    int res = 0;
    while (!q.empty())
    {
        int size = q.size();
        int begin, end;
        for (int i = 0; i < size; i++)
        {
            auto cur = q.front();
            q.pop();
            if (i == 0)
                begin = cur.second;
            if (i == size - 1)
                end = cur.second;
            if (cur.first->left != NULL)
                q.emplace(cur.first->left, cur.second * 2);
            if (cur.first->right != NULL)
                q.emplace(cur.first->right, cur.second * 2 + 1);
        }
        res = max(res, end - begin + 1);
    }
    return res;
}
