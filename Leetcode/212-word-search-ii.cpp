#include <iostream>
#include <string>
#include <vector>
using namespace std;
struct TrieNode
{
    bool isWord;
    TrieNode *next[26];
    TrieNode()
    {
        memset(next, NULL, sizeof(next));
        isWord = false;
    }

    ~TrieNode()
    {
        for (int i = 0; i < 26; i++)
            if (next[i])
                delete next[i];
    }
};
class solution
{

  public:
    solution()
    {
        root = new TrieNode();
    }
    ~solution()
    {
        delete root;
    }
    void addWord(string word)
    {
        TrieNode *p = root;
        for (auto c : word)
        {
            if (!p->next[c - 'a'])
                p->next[c - 'a'] = new TrieNode();
            p = p->next[c - 'a'];
        }
        p->isWord = true;
    }
    TrieNode * startwith(string prefix)
    {
        TrieNode *p = root;
        for (int i = 0; i < prefix.size() && p; i++)
        {
                p = p->next[prefix[i] - 'a'];
        }
        return p;
    }
    vector<string> findWords(vector<vector<char>> &board, vector<string> &words)
    {

    }

  private:
    TrieNode *root;
};