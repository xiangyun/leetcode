#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

// void averge(TreeNode *root, vector<double> &res, int cur_sum, int cnt, int depth)
// {
//     if (!root)
//         return;
//     if (depth == res.size())
//     {
//         res.push_back(cur_sum / (double)cnt);
//         cur_sum = 0;
//         cnt = 0;
//     }
//     cur_sum += root->val;
//     ++cnt;
//     averge(root->left, res, cur_sum, cnt, depth + 1);
//     averge(root->right, res, cur_sum, cnt, depth + 1);
// }

// void dfs(TreeNode *root, vector<vector<int>> &res, int depth)
// {
//     if (!root)
//     return;
//     if(res.size()==depth)
//     {
//         res.push_back(vector<int>());
//     }
//     res[depth].push_back(root->val);
//     dfs(root->left,res,depth+1);
//     dfs(root->right,res,depth+1);
// }

// vector<double> averageOfLevels(TreeNode *root)
// {
//     vector<vector<int>> arr;
//     dfs(root,arr,0);
//     vector<double> res;
//     for(int i=0;i<arr.size();i++)
//     {
//         double ave = std::accumulate(arr[i].begin(),arr[i].end(),0)/static_cast<double>(arr[i].size());
//         res.push_back(ave);
//     }
//     // averge(root, res, 0, 0, 0);
//     return res;
// }