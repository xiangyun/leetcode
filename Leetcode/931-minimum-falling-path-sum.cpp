#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int minFallingPathSum(vector<vector<int>> &A)
{
    int m = A.size(), n = A[0].size();
    int count=0;
    for(int i=0;i<m;i++)
    {
        int min_val=A[i][0];
        for(int j=0;j<n;j++)
        {
            min_val=min(A[i][j],min_val);
        }
        count+=min_val;
    }
    return count;
}