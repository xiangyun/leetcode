#include <iostream>
#include <stack>
#include <queue>
using namespace std;
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

void reorderList(ListNode *head)
{
    ListNode *cur = head;
    stack<ListNode *> stk;
    queue<ListNode *> que;
    while (cur)
    {
        stk.push(cur);
        que.push(cur);
        cur = cur->next;
    }
    int n = stk.size();
    ListNode *p = new ListNode(-1);
    ListNode *q = p;
    for (int i = 0; i < n / 2; i++)
    {
        ListNode *snode = stk.top();
        ListNode *qnode = que.front();
        snode->next = NULL;
        qnode->next = NULL;
        p->next = qnode;
        p->next->next = snode;
        p = p->next->next;
        stk.pop();
        que.pop();
    }
    if (n % 2 == 1)
    {
        ListNode *qnode = que.front();
        p->next = qnode;
        qnode->next = NULL;
    }
}