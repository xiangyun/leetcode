#include <iostream>
#include <vector>
#include <stack>
#include <unordered_map>
using namespace std;
class StockSpanner
{
  public:
    StockSpanner()
    {
        stk.push(-1);
    }

    int next(int price)
    {
        arr.push_back(price);
        while (stk.top() != -1 && arr[stk.top()] <= price)
        {
            stk.pop();
        }
        int res = arr.size() - 1 - stk.top();
        stk.push(arr.size() - 1);
        return res;
    }

  private:
    vector<int> arr;
    stack<int> stk;
};