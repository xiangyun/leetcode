#include <iostream>
#include <vector>
#include <set>
using namespace std;
void permute(vector<int> &num, set<vector<int>> &arr, int index)
{
    if (index >= num.size())
    {
        arr.insert(num);
        return;
    }
    for (int i = index; i < num.size(); i++)
    {
        swap(num[index], num[i]);
        permute(num, arr, index + 1);
        swap(num[index], num[i]);
    }
}
vector<vector<int>> permuteUnique(vector<int> &nums)
{
    set<vector<int>> arr;
    vector<vector<int>> unique;
    permute(nums, arr, 0);
    auto it = arr.begin();
    for (; it != arr.end(); it++)
    {
        unique.push_back(*it);
    }
    return unique;
}

