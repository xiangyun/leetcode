#include <iostream>
#include <string>
#include <queue>
using namespace std;

/*
int pos(string s, char c)
{
	string::iterator it = s.begin();
	int pos = 0;
	for (; it != s.end(); it++)
	{
		pos++;
		if (c == *it)
		{
			return pos - 1;
			break;
		}
	}
	return -1;
}


int solution(string s)
{
	string substr = "";
	int max_size = 0;
	for (int i = 0; i < s.size(); i++)
	{
		int p = pos(substr, s[i]);
		if (p == -1)
		{
			substr += s[i];
		}
		else
		{
			max_size = max_size > substr.size() ? max_size : substr.size();
			substr = substr.substr(p + 1, substr.size() - p - 1) + s[i];
		}
	}
	max_size = max_size > substr.size() ? max_size : substr.size();
	return max_size;
}
int main()
{
	string s;
	cin >> s;
	cout << solution(s) << endl;
	return 0;
}*/
int lengthOfLongestSubstring(string s)
{
	int map[256] = {0};

	int end = 0, start = 0, max_len = INT_MIN, count = 0;
	while (end < s.size())
	{
		map[s[end]]++;
		if (map[s[end]] > 1)
			count++;
		end++;
		while (count > 0)
		{
			if (map[s[start]] > 1)
				count--;
			map[start]--;
			start++;
		}
		max_len = max(max_len, end - start);
	}
	return max_len;
}