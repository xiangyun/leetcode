#include <iostream>
#include <vector>
using namespace std;
class UnionFind
{
  private:
    int count;
    int *parent;
    int *rank;

  public:
    UnionFind(int n)
    {
        count = n;
        parent = new int[n];
        rank = new int[n];
        for (int i = 0; i < n; i++)
        {
            parent[i] = i;
            rank[i] = 1;
        }
    }
    ~UnionFind()
    {
        delete[] parent;
        delete[] rank;
    }
    int find(int x)
    {
        if (x != parent[x])
            parent[x] = find(parent[x]);
        return parent[x];
    }
    void Union(int x, int y)
    {
        int fx = find(x), fy = find(y);
        if (fx == fy)
            return;
        if (rank[fx] > rank[fy])
        {
            parent[fy] = fx;
            rank[fx] += rank[fy];
        }
        else
        {
            parent[fx] = fy;
            rank[fy] += rank[fx];
        }
        count--;
    }
    bool isConnect(int x, int y)
    {
        return find(x) == find(y);
    }
};

vector<int> findRedundantConnection(vector<vector<int>> &edges)
{
    UnionFind uf(1001);
    for (int i = 0; i < edges.size(); i++)
    {
        if (uf.isConnect(edges[i][0], edges[i][1]))
        {
            return vector<int>({edges[i][0], edges[i][1]});
        }
        else
        {
            uf.Union(edges[i][0], edges[i][1]);
        }
    }
    return vector<int>();
}