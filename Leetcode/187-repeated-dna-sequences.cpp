#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
using namespace std;

vector<string> findRepeatedDnaSequences(string s)
{
    unordered_map<string, int> hash;
    for (int i = 0; i + 10 <= s.size(); i++)
    {
        hash[s.substr(i, 10)]++;
    }
    vector<string> res;
    for (auto it = hash.begin(); it != hash.end(); it++)
    {
        if (it->second >= 2)
        {
            res.push_back(it->first);
        }
    }
    return res;
}