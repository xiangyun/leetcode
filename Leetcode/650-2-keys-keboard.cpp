#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
vector<bool> get_prime()
{
    vector<bool> prime(1001, true);
    for (int i = 2; i <= 1000; i++)
    {
        if (prime[i])
        {
            for (int j = 2; j * i <= 1000; j++)
                prime[i * j] = false;
        }
    }
    return prime;
}

int minSteps(int n)
{
    vector<int> dp(n + 1, INT_MAX);
    vector<bool> prime = get_prime();
    dp[1] = 0;
    for (int i = 2; i <= n; i++)
    {
        if (prime[i])
            dp[i] = i;
        else
        {
            for (int j = 2; j * j <= i; j++)
            {
                if (i % j == 0)
                {
                    dp[i] = min(dp[i], min(dp[i / j] + j, i / j + dp[j]));
                }
            }
        }
    }
    return dp[n];
}