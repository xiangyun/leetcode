/*
divide and conquer:merge two linkedlists O(n)
*/
#include <iostream>
#include <vector>
#include <queue>
#include <functional>
using namespace std;

/*
ListNode * mergeTwoSortedLists(ListNode * l1, ListNode * l2)
{
    if(l1==NULL)
        return l2;
    if(l2==NULL)
        return l1;
    ListNode *cur1 = l1;
    ListNode *cur2 = l2;
    ListNode *head;
    if (l1->val<l2->val)
    {
        head = l1;
        l1 = l1->next;
    }
    else{
        head = l2;
        l2 = l2->next;
    }
    ListNode *node = head;
    while (cur1 != NULL && cur2 != NULL)
    {
        if (cur1->val < cur2->val)
        {
            node->next = cur1;
            node = cur1;
            cur1 = cur1->next;
        }
        else
        {
            node->next = cur2;
            node = cur2;
            cur2 = cur2->next;
        }
    }
    while(cur1!=NULL)
    {
        node->next = cur1;
        node = cur1;
        cur1 = cur1->next;
    }
    while(cur2!=NULL)
    {
        node->next = cur2;
        node = cur2;
        cur2 = cur2->next;
    }
    return head;
}*/
/*
ListNode * mergeTwoSortedLists(ListNode * l1, ListNode * l2)
{
    if(l1==NULL)
        return l2;
    if(l2==NULL)
        return l1;
    if(l1->val<=l2->val)
    {
        l1->next = mergeTwoSortedLists(l1->next, l2);
        return l1;
    }
    else
    {
        l2->next=mergeTwoSortedLists(l1,l2->next);
        return l2;
    }
}
ListNode *mergeKLists(vector<ListNode *> &lists)
{
    if(lists.empty())
        return NULL;
    while(lists.size()>1)
    {
        lists.push_back(mergeTwoSortedLists(lists[0], lists[1]));
        lists.erase(lists.begin());
        lists.erase(lists.begin());
    }
    return lists.front();
}*/
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};
struct cmp
{
    bool operator()(ListNode *a, ListNode *b) const
    {
        return a->val > b->val;
    }
};
ListNode *mergeKLists(vector<ListNode *> &lists)
{
    if (lists.empty())
        return NULL;
    priority_queue<ListNode *, vector<ListNode *>, cmp> heap;
    for (int i = 0; i < lists.size(); i++)
    {
        if (lists[i])
            heap.push(lists[i]);
    }
    if (heap.empty())
        return NULL;
    ListNode *head = heap.top();
    heap.pop();
    ListNode *tail = head;
    if (tail->next)
        heap.push(tail->next);
    while (!heap.empty())
    {
        tail->next = heap.top();
        heap.pop();
        tail = tail->next;
        if (tail->next)
            heap.push(tail->next);
    }
    return head;
}