#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

vector<vector<int>> threeSum(vector<int> nums)
{
	sort(nums.begin(), nums.end());
	vector<vector<int>> sol;
	for (int i = 0; i < nums.size(); i++)
	{
		if (i == 0 || nums[i] != nums[i - 1])
		{
			int left = i + 1;
			int right = nums.size() - 1;
			while (left < right)
			{
				if (left < right && nums[i] + nums[left] + nums[right] > 0)
					right--;
				if (left < right && nums[i] + nums[left] + nums[right] == 0)
				{
					vector<int> temp;
					temp.push_back(i);
					temp.push_back(left);
					temp.push_back(right);
					sol.push_back(temp);
				}
				if (left < right && nums[i] + nums[left] + nums[right] < 0)
					left++;
			}
		}
	}
	return sol;
}