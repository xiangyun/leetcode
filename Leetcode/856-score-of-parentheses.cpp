#include <iostream>
#include <vector>
#include <stack>
using namespace std;
int scoreOfParentheses(string S)
{
    stack<int> stk;
    for (int i = 0; i < S.size(); i++)
    {
        if (S[i] == '(')
        {
            stk.push(-1);
        }
        else
        {
            if (stk.top() == -1)
            {
                stk.pop();
                stk.push(1);
            }
            else
            {
                int temp_sum = 0;
                while (stk.top() != -1)
                {
                    int val = stk.top();
                    temp_sum += val;
                    stk.pop();
                }
                if (stk.empty())
                {
                    stk.push(temp_sum);
                }
                else
                {
                    stk.pop();
                    stk.push(2 * temp_sum);
                }
            }
        }
    }
    int sum = 0;
    while (!stk.empty())
    {
        sum += stk.top();
        stk.pop();
    }
    return sum;
}