#include <vector>
#include <numeric>
#include <iostream>
#include <algorithm>
using namespace std;
int part(vector<int> rods)
{
    int n = rods.size();
    vector<vector<bool>> dp(n, vector<bool>(10001, false));
    vector<vector<int>> max_val(n, vector<int>(10001, 0));
    dp[0][5000] = true;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j <= 10000; j++)
        {
            if (j - rods[i] >= 0 && dp[i][j - rods[i]])
            {
                dp[i + 1][j] = true;
                max_val[i + 1][j] = max(max_val[i][j], max_val[i][j - rods[i]] + rods[i]);
            }
            if (j + rods[i] <= 10000 && dp[i][j + rods[i]])
            {
                dp[i + 1][j] = true;
                max_val[i + 1][j] = max(max_val[i + 1][j], max_val[i][j + rods[j]]);
            }
            if (dp[i][j])
            {
                dp[i + 1][j] = true;
                max_val[i + 1][j] = max(max_val[i + 1][j], max_val[i][j]);
            }
        }
    }
    return max_val[n][5000];
}
