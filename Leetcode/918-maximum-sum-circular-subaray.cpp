#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
/*
int maxSubarraySumCircular(vector<int> &A)
{
    int max_sum = INT_MIN;
    int n = A.size();
    for (int i = 0; i < n; i++)
    {
        int cur_sum = A[i];
        max_sum = max(max_sum, cur_sum);
        for (int j = 1; j < n; j++)
        {
            int index = (i + j) % n;
            if (A[index] > cur_sum + A[index])
                cur_sum = A[index];
            else
                cur_sum += A[index];
            max_sum = max(max_sum,cur_sum);
        }
    }
    return max_sum;
}
*/
int maxSubarraySumCircular(vector<int> &A)
{
    int max_sum = INT_MIN, min_sum = INT_MAX, n = A.size(), total_sum = A[0];
    int cur_max = A[0], cur_min = A[0];

    for (int i = 1; i < A.size(); i++)
    {
        cur_max = max(A[i], cur_max + A[i]);
        cur_min = min(A[i], cur_min + A[i]);
        max_sum = max(max_sum, cur_max);
        min_sum = min(min_sum, cur_min);
        total_sum += A[i];
    }
    return max_sum > 0 ? max(max_sum, total_sum - min_sum) : max_sum;
}