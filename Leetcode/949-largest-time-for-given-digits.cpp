#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

class Solution {
public:
	bool isValid(string s)
	{
		return stoi(s.substr(0, 2)) <= 23 && stoi(s.substr(2, 2)) <= 59;
	}

	void backtracking(vector<string> &res, string str, vector<int> A, int cur_index)
	{
		if (cur_index >= 4)
		{
			if (isValid(str))
				res.push_back(str);
			return;
		}
		for (int i = cur_index; i < 4; i++)
		{
			str += to_string(A[i]);
			swap(A[i], A[cur_index]);
			backtracking(res, str, A, cur_index + 1);
			str.pop_back();
			swap(A[i], A[cur_index]);
		}
	}

	int cmp(string s1, string s2)
	{
		if (stoi(s1.substr(0, 2)) > stoi(s2.substr(0, 2)))
			return 1;
		else if (stoi(s1.substr(0, 2)) == stoi(s2.substr(0, 2)))
		{
			if (stoi(s1.substr(2, 2)) > stoi(s2.substr(2, 2)))
				return 1;
			else if (stoi(s1.substr(2, 2)) == stoi(s2.substr(2, 2)))
				return 0;
			else
				return -1;
		}
		else
			return -1;
	}

	string largestTimeFromDigits(vector<int> A)
	{

		vector<string> res;
		backtracking(res, "", A, 0);
		if (res.empty())
			return "";
		string max_str = res[0];
		for (int i = 1; i < res.size(); i++)
		{
			if (cmp(max_str, res[i]) == -1)
			{
				max_str = res[i];
			}
		}
		max_str.insert(max_str.begin() + 2, ':');
		return max_str;
	}
};