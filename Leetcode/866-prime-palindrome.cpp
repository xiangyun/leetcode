#include <iostream>
#include <vector>
#include <string>
using namespace std;

// bool isPalindrome(int x)
// {
//     if (x < 0)
//         return false;

//     vector<int> count;
//     while (x != 0)
//     {
//         count.push_back(x % 10);
//         x = x / 10;
//     }

//     for (int i = 0; i < count.size() / 2; ++i)
//         if (count[i] != count[count.size() - i - 1])
//             return false;

//     return true;
// }

// int primePalindrome(int N)
// {
//     int max_val = 2 * 1e8;
//     bool prime[2 * (int)1e8 + 1] = {true};
//     int n = 0;
//     for (int i = 2; i <= max_val; i++)
//     {
//         if (prime[i])
//         {
//             if (isPalindrome(i))
//             {
//                 ++n;
//                 if (N == n)
//                 {
//                     return i;
//                 }
//             }
//             for (int j = i * i; j < n; j += i)
//                 prime[j] = false;
//         }
//     }
//     return -1;
// }

bool isPrime(int x)
{
    if (x % 2 == 0)
        return false;
    for (int i = 3; i <= int(sqrt(x)) + 1; i += 2)
    {
        if (x % i == 0)
            return false;
    }
    return true;
}

int palindrome(int x)
{
    string s = to_string(x);
    string t(s.rbegin(), s.rend());
    s.pop_back();
    return stoi(s + t);
}

int primePalindrome(int N)
{
    if (N <= 2)
        return 2;
    if (N > 7 && N <= 11)
        return 11;
    for (int i = 1; i <= static_cast<int>(1e8); i++)
    {
        int x = palindrome(i);
        if (x >= N && isPrime(i))
            return x;
    }
    return -1;
}