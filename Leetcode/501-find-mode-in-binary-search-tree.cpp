#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

// void inOrder(TreeNode *root, TreeNode *pre_node, vector<int> &res, int max_time, int cur_time)
// {
//     if (!root)
//         return;
//     inOrder(root->left, pre_node, res, max_time, cur_time);
//     if (pre_node)
//     {
//         cur_time = (root->val == pre_node->val) ? cur_time + 1 : 1;
//     }
//     if (cur_time == max_time)
//     {
//         res.push_back(root->val);
//     }
//     else if (cur_time > max_time)
//     {
//         res.clear();
//         res.push_back(root->val);
//         max_time = cur_time;
//     }
//     pre_node = root;
//     inOrder(root->right, pre_node, res, max_time, cur_time);
// }

// vector<int> findMode(TreeNode *root)
// {
//     vector<int> res;
//     if (!root)
//         return res;
//     TreeNode *pre_node = NULL;
//     inOrder(root, pre_node, res, 0, 1);
//     return res;
// }

void inOrder(TreeNode *root, unordered_map<int, int> &hash)
{
    if (!root)
        return;
    inOrder(root->left, hash);
    hash[root->val]++;
    inOrder(root->right, hash);
}

vector<int> findMode(TreeNode *root)
{
    unordered_map<int, int> hash;
    inOrder(root, hash);
    vector<int, int> vec(hash.begin(), hash.end());
    auto cmp = [](const pair<int, int> &a, const pair<int, int> &b) {
        return a.second > b.second;
    };
    sort(vec.begin(), vec.end(), cmp);
}