#include <iostream>
#include <vector>
using namespace std;
class UnionFind
{
  private:
    int count;
    int *parent;
    int *rank;

  public:
    UnionFind(int n)
    {
        count = n;
        parent = new int[n];
        rank = new int[n];
        for (int i = 0; i < n; i++)
        {
            parent[i] = i;
            rank[i] = 1;
        }
    }
    ~UnionFind()
    {
        delete[] parent;
        delete[] rank;
    }
    int find(int x)
    {
        if (x != parent[x])
            parent[x] = find(parent[x]);
        return parent[x];
    }
    bool isConnnect(int x, int y)
    {
        return find(x) == find(y);
    }
    void Union(int x, int y)
    {
        int fx = find(x), fy = find(y);
        if (fx == fy)
            return;
        if (rank[fx > rank[fy]])
        {
            parent[fy] = fx;
            rank[fx] += rank[fy];
        }
        else
        {
            parent[fx] = fy;
            rank[fy] += rank[fx];
        }
        count--;
    }
    int size()
    {
        return count;
    }
};
class Solution
{
  public:
    int minSwapsCouples(vector<int> &row)
    {
        UnionFind uf(row.size() / 2);
        for (int i = 0; i < row.size() / 2; i++)
        {
            int x = row[2 * i] / 2, y = row[2 * i + 1] / 2;
            if (x == y)
                continue;
            else
                uf.Union(x, y);
        }
        return row.size() / 2 - uf.size();
    }
};
