#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;
class Solution
{
  public:
    int dist(pair<int, int> &p, pair<int, int> &q)
    {
        return (p.first - q.first) * (p.first - q.first) + (p.second - q.second) * (p.second - q.second);
    }

    int numberOfBoomerangs(vector<pair<int, int>> &points)
    {
        int ans = 0;
        unordered_map<int, int> hash;
        for (auto &p : points)
        {
            for (auto &q : points)
                hash[dist(p, q)]++;
            for (auto &c : hash)
                ans += c.second * (c.second - 1);
            hash.clear();
        }
        return ans;
    }
};