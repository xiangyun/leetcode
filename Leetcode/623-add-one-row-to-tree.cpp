#include <iostream>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left, *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
class Solution
{
  public:
    TreeNode *addOneRow(TreeNode *root, int v, int d)
    {
        if (d == 1)
        {
            TreeNode *r = new TreeNode(v);
            r->left = root;
            return r;
        }
        else
            helper(root, v, d, 1);
        return root;
    }

  private:
    void helper(TreeNode *&r, int v, int d, int depth)
    {
        if (!r)
            return;
        if (depth + 1 == d)
        {
            TreeNode *left = r->left, *right = r->right;
            r->left = new TreeNode(v);
            r->right = new TreeNode(v);
            r->left->left = left;
            r->right->right = right;
        }
        else if (depth + 1 < d)
        {
            helper(r->left, v, d, depth + 1);
            helper(r->right, v, d, depth + 1);
        }
    }
};