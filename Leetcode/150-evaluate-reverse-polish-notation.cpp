#include <iostream>
#include <string>
#include <stack>
#include <vector>
using namespace std;
int evalRPN(vector<string> &tokens)
{
    stack<int> stk;
    for (auto i : tokens)
    {
        if (i == "+")
        {
            int y = stk.top();
            stk.pop();
            int x = stk.top();
            stk.pop();
            stk.push(x + y);
        }
        else if (i == "-")
        {
            int y = stk.top();
            stk.pop();
            int x = stk.top();
            stk.pop();
            stk.push(x - y);
        }
        else if (i == "*")
        {
            int y = stk.top();
            stk.pop();
            int x = stk.top();
            stk.pop();
            stk.push(x * y);
        }
        else if (i == "/")
        {
            int y = stk.top();
            stk.pop();
            int x = stk.top();
            stk.pop();
            stk.push(x / y);
        }
        else
        {
            int val = 0;
            int fac = 1;
            for (int c = 0; c < i.size(); c++)
            {
                if (c == 0 && i[c] == '-')
                {
                    fac = -1;
                    continue;
                }
                val = 10 * val + i[c] - '0';
            }
            val = val * fac;
            stk.push(val);
        }
    }
    return stk.top();
}