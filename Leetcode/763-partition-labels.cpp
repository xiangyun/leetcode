#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
using namespace std;
vector<int> partitionLabels(string S)
{
    unordered_map<char, int> last;
    for (int i = 0; i < S.size(); i++)
    {
        last[S[i]] = i;
    }
    vector<int> res;
    int cut = -1;
    for (int i = 0; i < S.size(); i++)
    {
        if (last[S[i]] == i)
        {
            bool flag = true;
            for (int j = i - 1; j >= 0 && j > cut; j--)
            {
                if (last[S[j]] > i)
                {
                    flag = false;
                    break;
                }
            }
            if (flag)
            {
                res.push_back(i - cut);
                cut = i;
            }
        }
    }
    return res;
}