#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
/*
void backtracking(vector<vector<int>> &res, vector<int> arr, vector<int> nums, int target, int cur_sum)
{
    if (cur_sum < target)
    {
        for (int i = 0; i < nums.size(); i++)
        {
            if ((cur_sum + nums[i]) > target)
                return;
            arr.push_back(nums[i]);
            backtracking(res, arr, nums, target, cur_sum + nums[i]);
            arr.pop_back();
        }
    }
    else if (cur_sum > target)
        return;
    else
    {
        res.push_back(arr);
        return;
    }
}
int combinationSum4(vector<int> &nums, int target)
{
    sort(nums.begin(), nums.end());
    vector<vector<int>> res;
    backtracking(res, vector<int>(), nums, target, 0);
    return res.size();
}
*/
/*
int combinationSum4(vector<int> &nums, int target)
{
    if (nums.empty())
        return 0;
    std::sort(nums.begin(), nums.end());
    vector<int> dp(target + 1, 0);
    int pos = 0;
    for (int i = 0; i <= target && i < nums.size(); i++)
    {
        dp[nums[i]] = 1;
    }
    for (int i = 1; i <= target && i <= nums.back(); i++)
    {
        for (int j = 0; j < nums.size(); j++)
        {
            if (i < nums[j])
                break;
            dp[i] += dp[i - nums[j]];
        }
    }
    if (nums.back() < target)
    {
        for (int i = nums.back() + 1; i <= target; i++)
        {
            for (int j = 0; j < nums.size(); j++)
            {
                dp[i] += dp[i - nums[j]];
            }
        }
    }
    return dp[target];
}
*/
int combinationSum4(vector<int> &nums, int target)
{
    vector<int> dp(target + 1, 0);
    dp[0] = 1;
    sort(nums.begin(), nums.end());
    for (int i = 1; i <= target; i++)
    {
        for (int j = 0; j < nums.size(); j++)
        {
            if (i >= nums[j])
                dp[i] += dp[i - nums[j]];
            else
                break;
        }
    }
    return dp[target];
}