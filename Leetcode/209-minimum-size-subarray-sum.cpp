#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int minSubarrayLen(int s, vector<int> &nums)
{
    int sum = 0, res = INT_MAX;
    int i = 0, j = 0;
    while (j < nums.size())
    {
        sum += nums[j];
        j++;
        while (sum >= s)
        {
            res = min(res, j - i);
            sum -= nums[i];
            i++;
        }
    }
    return res == INT_MAX ? 0 : res;
}