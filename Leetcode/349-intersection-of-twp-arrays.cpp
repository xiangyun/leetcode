#include <iostream>
#include <vector>
#include <set>
#include <unordered_map>
using namespace std;
vector<int> intersection(vector<int> &nums1, vector<int> &nums2)
{
    set<int> s;
    unordered_map<int, int> hash;
    for (auto &i : nums1)
    {
        s.insert(i);
    }
    for (auto &j : nums2)
    {
        if (s.count(j))
        {
            hash[j]++;
        }
    }
    vector<int> res;
    for(auto it = hash.begin();it!=hash.end();it++)
    {
        res.push_back(it->first);
    }
    return res;
}