#include <iostream>
#include <vector>
using namespace std;

int removeDuplicates(vector<int> &nums)
{
    if (nums.size() <= 1)
        return nums.size();
    int i = 0, j = 1, count = 1;
    while (j < nums.size())
    {
        if (nums[i] == nums[j])
        {
            if (count == 1)
            {
                i++;
                nums[i] = nums[j];
                j++;
                count++;
            }
            else
            {
                j++;
                count++;
            }
        }
        else
        {
            i++;
            nums[i] = nums[j];
            j++;
            count = 1;
        }
    }
    return i + 1;
}