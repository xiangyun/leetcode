#include <iostream>
#include <vector>
#include <string>
using namespace std;

class UnionFind
{
  private:
    int count;
    int *parent;
    int *rank;

  public:
    UnionFind(int n)
    {
        count = n;
        parent = new int[n];
        rank = new int[n];
        for (int i = 0; i < n; i++)
        {
            parent[i] = i;
            rank[i] = 1;
        }
    }
    ~UnionFind()
    {
        delete[] parent;
        delete[] rank;
    }
    int find(int x)
    {
        if (x != parent[x])
            parent[x] = find(parent[x]);
        return parent[x];
    }
    bool isConnect(int x, int y)
    {
        return find(x) == find(y);
    }
    void Union(int x, int y)
    {
        int fx = find(x), fy = find(y);
        if (fx == fy)
            return;
        if (rank[fx] > rank[fy])
        {
            parent[fy] = fx;
            rank[fx] += rank[fy];
        }
        else
        {
            parent[fx] = fy;
            rank[fy] += rank[fx];
        }
        count--;
    }
    int size()
    {
        return count;
    }
};
class Solution
{
  private:
    int n;

  public:
    int index(int i, int j, int k)
    {
        return 4 * (i * n + j) + k;
    }

    int regionsBySlashes(vector<string> &grid)
    {
        n = grid.size();
        UnionFind uf(4 * n * n);
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                // int index = 4 * (i * n + j);
                char c = grid[i][j];
                if (c != '/')
                {
                    uf.Union(index(i, j, 0), index(i, j, 1));
                    uf.Union(index(i, j, 2), index(i, j, 3));
                }
                if (c != '\\')
                {
                    uf.Union(index(i, j, 0), index(i, j, 3));
                    uf.Union(index(i, j, 2), index(i, j, 1));
                }
                if (i > 0)
                {
                    uf.Union(index(i, j, 0), index(i - 1, j, 2));
                }
                if (j > 0)
                {
                    uf.Union(index(i, j, 3), index(i, j - 1, 1));
                }
            }
        }
        return uf.size();
    }
};