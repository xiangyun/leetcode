#include <iostream>
#include <vector>
#include <algorithm>/*???*/
struct Interval
{
    int start;
    int end;
    Interval() : start(0), end(0) {}
    Interval(int s, int e) : start(s), end(e) {}
};
std::vector<Interval> merge(std::vector<Interval> &intervals)
{
    if (intervals.empty())
        return {};
    sort(intervals.begin(), intervals.end(), [](const Interval &a, const Interval &b) { return a.start <= b.start; });
    std::vector<Interval> result;
    for (int i = 0; i < intervals.size(); i++)
    {
        if (result.empty() || result.back().end < intervals[i].start)
            result.push_back(intervals[i]);
        else
            result.back().end = fmax(result.back().end, intervals[i].end);
    }
    return result;
}