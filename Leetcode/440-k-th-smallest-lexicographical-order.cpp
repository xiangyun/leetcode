#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
/*
void backtracking(vector<int> &arr, int cur_index, int n,int k)
{
    if (cur_index > n)
        return;
    arr.push_back(cur_index);
    for (int i = 0; i <= 9; i++)
    {
        if (cur_index * 10 + i > n)
            return;
        backtracking(arr, cur_index * 10 + i, n);
    }
}

int findKthNumber(int n, int k)
{
    vector<int> arr;
    for (int i = 1; i <= 9; i++)
    {
        backtracking(arr, i, n);
    }
    return arr[k - 1];
}
*/
//TLE

// int numbersBeginWith(int n, long long prefix);

// int findKthNumber(int n, int k, int start = 0)
// {
//     if (k > 0)
//     {
//         for (int i = (start == 0) ? 1 : 0; i <= 9; i++)
//         {
//             int count = numbersBeginWith(n, 10 * start + i);
//             if (k <= count)//if k <= count, the answer is begin with the prefix
//                 return findKthNumber(n, k - 1, 10 * start + i);
//             k -= count;//if the answer does not begin with the prefix, remove all the numbers beign with this prefix
//         }
//     }
//     return start;
// }

int numbersBeginWith(int n, long long prefix)
{
    int cnt = 0;
    for (long long first = prefix, last = first + 1; first <= n; first *= 10, last *= 10)
    {
        cnt += static_cast<int>(min(n + 1LL, last) - first);
    }
    return cnt;
}

int findKthNumber(int n, int k)
{
    long long prefix = 1;
    --k;
    while (k > 0)
    {
        int count = numbersBeginWith(n, prefix);
        if (k >= count)
        {
            prefix++;
            k -= count;
        }
        else
        {
            prefix *= 10;
            --k;
        }
    }
    return static_cast<int>(prefix);
}