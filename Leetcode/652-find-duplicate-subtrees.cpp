#include <iostream>
#include <vector>
#include <unordered_map>
#include <string>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

string backtracking(TreeNode *root, unordered_map<string, int> &hash, vector<TreeNode *> &res)
{
    if (!root)
        return "";
    string s = to_string(root->val) + "," + backtracking(root->left, hash, res) + "," + backtracking(root->right, hash, res);
    hash[s]++;
    if (hash[s] > 1)
        res.push_back(root);
    return s;
}

vector<TreeNode *> findDuplicateSubtrees(TreeNode *root)
{
    unordered_map<string, int> hash;
    vector<TreeNode *> res;
    backtracking(root, hash, res);
    return res;
}