#include <iostream>
#include <vector>
using namespace std;

void backtracking(vector<vector<int>> &res, vector<int> arr, int index, int cur_sum, int size, int k, int n)
{
    if (size == k)
    {
        if (cur_sum == n)
            res.push_back(arr);
        return;
    }
    for (int i = index; i < 9; i++)
    {
        arr.push_back(i + 1);
        backtracking(res, arr, i + 1, cur_sum + i + 1, size + 1, k, n);
        arr.pop_back();
    }
}

vector<vector<int>> combinationSum3(int k, int n)
{
    vector<vector<int>> res;
    vector<int> arr;
    backtracking(res, arr, 0, 0, 0, k, n);
    return res;
}