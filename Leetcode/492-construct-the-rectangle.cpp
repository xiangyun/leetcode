#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
vector<int> constructRectangle(int area)
{
    vector<int> res;
    int min_val = INT_MAX;
    for (int i = 1; i <= area / 2; i++)
    {
        if (area % i == 0)
        {
            if (min_val > area / i - i)
            {
                min_val = area / i - i;
                res = {area / i, i};
            }
        }
    }
    return res;
}