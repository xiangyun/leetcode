#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
void permutations(vector<int> &num, vector<vector<int>> &arr, int index)
{
    if(index>=num.size())
    {
        arr.push_back(num);
        return;
    }
    for (int i = index; i < num.size(); i++)
    {
        swap(num[index], num[i]);
        permutations(num, arr, index + 1);
        swap(num[index], num[i]);
    }
}
vector<vector<int>> permute(vector<int> &nums)
{
    vector<vector<int>> arr;
    permutations(nums, arr, 0);
    return arr;
}
