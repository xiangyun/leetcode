#include <iostream>
#include <string>
#include <vector>
using namespace std;

/*
Given n pairs of parentheses,
write a function to generate
all combinations of well-formed parentheses.
*/
void solution(int i, int j, int n, string s, vector<string> &str);
vector<string> generateParenthesis(int n)
{
    vector<string> str;
    solution(0, 0, n, "", str);
    return str;
}

void solution(int i,int j,int n, string s,vector<string> &str)
{
    if(i==n&&j==n)
    {
        str.push_back(s);
        return;
    }
    if(j>i||j>n||i>n)
        return;
    solution(i + 1, j, n, s + '(', str);
    solution(i, j + 1, n,s + ')', str);
}