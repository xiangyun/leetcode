#include <iostream>
#include <vector>

using namespace std;

// void backtracking(vector<int> &arr, int N)
// {
//     if (arr.size() <= 2)
//         return;
//     vector<int> left, right;
//     int res_1 = arr[0] % N, res = arr[1] % N;
//     for (int i = 0; i < arr.size(); i++)
//     {
//         if (arr[i] % N == res_1)
//             left.push_back(arr[i]);
//         else
//             right.push_back(arr[i]);
//     }
//     backtracking(left, 2 * N);
//     backtracking(right, 2 * N);
//     left.insert(left.end(), right.begin(), right.end());
//     arr = left;
// }

// vector<int> beautifulArray(int N)
// {
//     vector<int> arr;
//     for (int i = 0; i < N; i++)
//         arr.push_back(i + 1);
//     backtracking(arr, 2);
//     return arr;
// }

vector<int> helper(vector<int> &arr, int t)
{
    if (arr.size() <= 2)
        return arr;
    int mod = 1 << t;
    int r1 = arr[0] % mod, r2 = arr[1] % mod;
    vector<int> left, right;
    for (auto &i : arr)
        if (i % mod == r1)
            left.push_back(i);
        else
            right.push_back(i);
    left = helper(left, t + 1);
    right = helper(right, t + 1);
    left.insert(left.end(), right.begin(), right.end());
    return left;
}

vector<int> beautifulArray(int N)
{
    vector<int> arr;
    for (int i = 0; i < N; i++)
        arr.push_back(i + 1);
    return helper(arr, 1);
}