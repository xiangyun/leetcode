#include <iostream>
#include <string>
#include <vector>
using namespace std;

bool isValid(string s, vector<string> words)
{
    for (auto st : words)
        if (s == st)
            return true;
    return false;
}

bool wordBreak(string s, vector<string> &wordDict)
{
    vector<bool> dp(s.size() + 1, false);
    dp[0] = true;
    for (int i = 1; i <= s.size(); i++)
    {
        for (int j = 0; j < i; j++)
        {
            if (dp[j] && isValid(s.substr(j, i - j + 1), wordDict))
            {
                dp[i] = true;
                break;
            }
        }
    }
    return dp[s.size()];
}