#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
using namespace std;
// bool canPartition(vector<int> &nums)
// {
//     int sum = std::accumulate(nums.begin(), nums.end(), 0);
//     if (sum & 1)
//         return false;
//     vector<bool> dp(sum + 1, 0);
//     dp[0] = true;
//     for (auto &i : nums)
//     {
//         for (int j = sum; j >= 0; j--)
//         {
//             if (dp[j])
//                 dp[i + j] = true;
//         }
//         if (dp[sum / 2])
//             return true;
//     }
//     return false;
// }

bool canPartition(vector<int> &nums)
{
    int sum = std::accumulate(nums.begin(), nums.end(), 0);
    if (sum % 2 == 1)
        return false;
    int target = sum / 2, n = nums.size();
    vector<vector<int>> dp(n + 1, vector<int>(target + 1));
    for (int i = 0; i <= n; i++)
    {
        for (int j = 0; j <= target; j++)
        {
            if (i == 0)
                dp[i][j] = 0;
            else if (j >= nums[i - 1])
                dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - nums[i - 1]] + nums[i - 1]);
            else
                dp[i][j] = dp[i - 1][j];
        }
    }
    return dp[n][target] == target;
}