#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
using namespace std;

vector<int> split(string s, string t)
{
    unordered_map<char, int> hash(t.begin(), t.end());
    string str = "";
    vector<int> res;

    for (auto c : s)
    {
        if (hash.find(c) == hash.end())
            str += c;
        else
        {
            res.emplace_back(stoi(str));
            str = "";
        }
    }
    if (!str.empty())
        res.emplace_back(stoi(str));
    return res;
}

string complexNumberMultiply(string a, string b)
{
    vector<int> x = split(a, "+i");
    vector<int> y = split(b, "+i");
    int z = x[0] * y[0] - x[1] * y[1];
    int w = x[1] * y[0] + x[0] * y[1];
    return to_string(z)+"+"+to_string(w)+"i";
}