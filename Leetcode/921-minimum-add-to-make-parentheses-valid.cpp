#include <iostream>
#include <vector>
#include <string>
#include <stack>
using namespace std;
int minAddMakeValid(string S)
{
    stack<char> stk;
    int cnt = 0;
    for (auto c : S)
    {
        if (!stk.empty() && c == ')')
            stk.pop();
        else if (stk.empty() && c == ')')
            ++cnt;
        else
            stk.push('(');
    }
    cnt += stk.size();
    return cnt;
}