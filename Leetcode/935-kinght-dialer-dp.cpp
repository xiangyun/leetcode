#include <iostream>
#include <vector>
using namespace std;

int knightDialer(int N)
{
    vector<vector<int>> arr = {{4, 6}, {6, 8}, {7, 9}, {4, 8}, {0, 3, 9}, {}, {1, 7, 0}, {2, 6}, {1, 3}, {2, 4}};
    if (N == 1)
        return 10;
    vector<vector<int>> dp(N, vector<int>(10));
    for (int i = 0; i <= 9; i++)
        dp[0][i] = 1;
    for (int i = 1; i < N; i++)
    {
        for (int j = 0; j <= 9; j++)
        {
            if (arr[j].empty())
                dp[i][j] = 0;
            else
            {
                for (int k = 0; k < arr[j].size(); k++)
                {
                    dp[i][j] += dp[i - 1][arr[j][k]];
                    dp[i][j] %= 1000000007;
                }
            }
        }
    }
    int sum = 0;
    for (int j = 0; j <= 9; j++)
    {
        sum += dp[N - 1][j];
        sum %= 1000000007;
    }
    return sum;
}