#include <iostream>
#include <string>
#include <unordered_map>
#include <map>
using namespace std;
class TimeMap
{
  public:
    TimeMap()
    {
    }

    void set(string key, string value, int timestamp)
    {
        hash[key].emplace(value, timestamp);
    }

    string get(string key, int timestamp)
    {
        if (!hash.count(key))
            return "";
        else
        {
            auto it = hash[key].upper_bound(timestamp);
            if (it == hash[key].begin())
                return "";
            else
            {
                --it;
                return it->second;
            }
        }
    }

  private:
    unordered_map<string, map<int, string>> hash;
};