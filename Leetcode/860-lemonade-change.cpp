#include <iostream>
#include <vector>
using namespace std;

bool backtracking(vector<int> &bills, int cur_index, int x1, int x2, int x3)
{
    if (cur_index == bills.size() - 1)
    {
        return true;
    }
    ++cur_index;
    int x = bills[cur_index];
    if (x == 5)
    {
        ++x1;
        return backtracking(bills, cur_index, x1, x2, x3);
    }
    else if (x == 10)
    {
        if (x1 == 0)
            return false;
        --x1;
        ++x2;
        return backtracking(bills, cur_index, x1, x2, x3);
    }
    else
    {
        return (x1 >= 3 && backtracking(bills, cur_index, x1 - 3, x2, x3 + 1)) || ((x1 >= 1 && x2 >= 1) && backtracking(bills, cur_index, x1 - 1, x2 - 1, x3 + 1));
    }
}

bool lemonadeChange(vector<int> &bills)
{
    return backtracking(bills, -1, 0, 0, 0);
}