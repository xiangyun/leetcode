#include <iostream>
#include <vector>
// #include <map>
#include <unordered_map>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
class Solution
{
  public:
    int sumOfBT(TreeNode *root)
    {
        if (!root)
            return 0;
        int left = sumOfBT(root->left);
        int right = sumOfBT(root->right);
        int sum = left + right + root->val;
        hash[sum]++;
        max_cnt = max_cnt > hash[sum] ? max_cnt : hash[sum];
        return sum;
    }

    vector<int> findFrequentTreeSum(TreeNode *root)
    {
        sumOfBT(root);
        vector<int> res;
        for (auto &i : hash)
        {
            if (i.second == max_cnt)
                res.push_back(i.first);
        }
        return res;
    }

  private:
    unordered_map<int, int> hash;

    int max_cnt = 0;
};
