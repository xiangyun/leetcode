#include <iostream>
#include <vector>
#include <unordered_map>
#include <unordered_set>
using namespace std;

void dfs(vector<unordered_set<int>> &vec, int depth, int &sum, int cur_node, unordered_map<int, int> &visited, vector<int> &count)
{
    visited[cur_node]++;
    for (auto i : vec[cur_node])
    {
        if (visited.find(i) == visited.end())
        {
            sum += depth;
            dfs(vec, depth + 1, sum, i, visited, count);
            count[cur_node] += count[i]; // depth of tree
        }
    }
}

void dfs2(vector<unordered_set<int>> &vec, vector<int> &res, int cur_node, unordered_map<int, int> &visited, vector<int> &count)
{
    visited[cur_node]++;
    for (auto i : vec[cur_node])
    {
        if (visited.find(i) == visited.end())
        {
            res[i] = res[cur_node] + res.size() - 2 * count[i];
            dfs2(vec, res, i, visited, count);
        }
    }
}

vector<int> sumOfDistancesInTree(int N, vector<vector<int>> &edges)
{
    vector<unordered_set<int>> vec(N);
    for (auto arr : edges)
    {
        vec[arr[0]].insert(arr[1]);
        vec[arr[1]].insert(arr[0]);
    }
    vector<int> res(N, 0), count(N, 1);
    unordered_map<int, int> visited;
    int sum = 0;
    dfs(vec, 1, sum, 0, visited, count);
    res[0] = sum;
    visited.clear();
    dfs2(vec, res, 0, visited, count);
    return res;
}