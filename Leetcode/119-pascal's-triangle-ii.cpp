#include <iostream>
#include <vector>
using namespace std;

vector<int> getRow(int rowIndex)
{
    if (rowIndex == 0)
        return vector<int>({1});
    vector<int> arr(rowIndex + 1);
    arr[0] = 1;
    for (int i = 1; i <= rowIndex; i++)
        arr[i] = (unsigned long)arr[i - 1] * (unsigned long)(rowIndex - i + 1) / i;
    return arr;
}