#include <iostream>
#include <vector>
#include <stack>
using namespace std;
bool validateStackSequences(vector<int> &pushed, vector<int> &poped)
{
    stack<int> stk;
    int i = 0, j = 0;
    while (i < pushed.size() && j < poped.size())
    {
        stk.push(pushed[i]);
        while (!stk.empty() && stk.top() == poped[j])
        {
            j++;
            stk.pop();
        }
        i++;
    }
    return i == j;
}