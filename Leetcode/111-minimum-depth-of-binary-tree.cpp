#include <iostream>
#include <algorithm>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
class Solution
{
  public:
    int minDepth(TreeNode *root)
    {
        if (root == NULL)
            return 0;
        if (!root->left && !root->right)
            return 1;
        int left = minDepth(root->left);
        int right = minDepth(root->right);
        if (!root->right)
            return left + 1;
        if (!root->left)
            return right + 1;
        return min(left, right) + 1;
    }
};