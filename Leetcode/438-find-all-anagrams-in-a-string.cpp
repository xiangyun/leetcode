#include <iostream>
#include <string>
#include <vector>
using namespace std;
vector<int> findAnagrams(string s, string t)
{
    vector<int> res;
    int map[256] = {0};
    for (auto &c : t)
        map[c]++;
    int start = 0, end = 0, count = t.size();
    while (end < s.size())
    {
        map[s[end]]--;
        if (map[s[end]] >= 0)
            count--;
        end++;
        while (count == 0)
        {
            if (map[s[start]] == 0)
                count++;
            if (end - start == t.size())
                res.push_back(start);
            map[s[start]]++;
            start++;
        }
    }
    return res;
}