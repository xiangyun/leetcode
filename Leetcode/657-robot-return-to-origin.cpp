#include <iostream>
#include <string>
using namespace std;
class Point2D
{
  public:
    Point2D() {}
    Point2D(int _x, int _y) : x(_x), y(_y) {}
    ~Point2D() {}
    Point2D operator+(const Point2D &p) const
    {
        return Point2D(this->x + p.x, this->y + p.y);
    }
    Point2D operator-(const Point2D &p) const
    {
        return Point2D(this->x - p.x, this->y - y);
    }
    Point2D &operator+=(const Point2D &p)
    {
        this->x += p.x;
        this->y += p.y;
        return *this;
    }    
    Point2D &operator-=(const Point2D &p)
    {
        this->x -= p.x;
        this->y -= p.y;
        return *this;
    }
    bool operator==(const Point2D &p) const
    {
        return this->x == p.x && this->y == p.y;
    }
    
  private:
    int x, y;
};
bool judgeCircle(string move)
{
    Point2D p(0, 0);
    for (auto &c : move)
    {
        if (c == 'U')
            p += Point2D(0, 1);
        else if (c == 'D')
            p += Point2D(0, -1);
        else if (c == 'L')
            p += Point2D(-1, 0);
        else
            p += Point2D(1, 0);
    }
    return p == Point2D(0, 0);
}