/*
Given an input string (s) and a pattern (p), 
implement wildcard pattern matching with support for '?' and '*'.
*/
#include <iostream>
#include <vector>
#include <string>
using namespace std;
bool isMatch(string s, string p)
{
    if (p.empty())
        return s.empty();
    int len1 = s.length();
    int len2 = p.length();
    vector<vector<bool>> dp(len1 + 1, vector<bool>(len2 + 1, false));
    dp[0][0] = true;
    for (int j = 1; j <= len2; j++)
    {
        if (p[j - 1] != '*')
            break;
        dp[0][j] = true;
    }
    for (int i = 1; i <= len1; i++){
        for (int j = 1; j <= len2; j++)
        {
            if (p[j - 1] == '*')
                dp[i][j] = dp[i - 1][j - 1] || dp[i][j - 1] || dp[i - 1][j];
            else
            {
                if (p[j - 1] == s[i - 1] || p[j - 1] == '?')
                    dp[i][j] = dp[i - 1][j - 1];
            }
        }
    }
    return dp[len1][len2];
}