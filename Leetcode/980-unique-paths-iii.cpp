#include <iostream>
#include <vector>
using namespace std;
class Solution
{
  private:
    int cnt = 0;

    bool isValid(int i, int j, int m, int n)
    {
        return i >= 0 && j >= 0 && i < m && j < n;
    }

    void backtracking(vector<vector<int>> &grid, int i, int j, vector<vector<bool>> visited, int zero_cnt)
    {
        if (isValid(i, j, grid.size(), grid[0].size()) && !visited[i][j])
        {
            if (grid[i][j] == 2)
            {
                if (zero_cnt == 0)
                    ++cnt;
                return;
            }
            if (grid[i][j] == -1)
                return;
            visited[i][j] = true;
            --zero_cnt;
            backtracking(grid, i + 1, j, visited, zero_cnt);
            backtracking(grid, i - 1, j, visited, zero_cnt);
            backtracking(grid, i, j + 1, visited, zero_cnt);
            backtracking(grid, i, j - 1, visited, zero_cnt);
        }
    }

  public:
    int uniquePathsIII(vector<vector<int>> &grid)
    {
        int zero_cnt = 0;
        pair<int, int> start_index;
        for (int i = 0; i < grid.size(); i++)
        {
            for (int j = 0; j < grid[0].size(); j++)
            {
                if (grid[i][j] == 0)
                    zero_cnt++;
                if (grid[i][j] == 1)
                    start_index = make_pair(i, j);
            }
        }
        vector<vector<bool>> visited(grid.size(), vector<bool>(grid[0].size(), false));
        backtracking(grid, start_index.first, start_index.second, visited, zero_cnt + 1);
        return cnt;
    }
};