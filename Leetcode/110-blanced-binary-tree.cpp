#include <iostream>
#include <algorithm>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
int maxDepth(TreeNode *root)
{
    if (root == NULL)
        return 0;
    int left = maxDepth(root->left);
    int right = maxDepth(root->right);
    return max(left, right) + 1;
}
bool isBlanced(TreeNode *root)
{
    if (root == NULL)
        return true;
    int left = maxDepth(root->left);
    int right = maxDepth(root->right);
    if (abs(left - right) > 1)
        return false;
    return isBlanced(root->left) && isBlanced(root->right);
}