#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int max_area(vector<int> container)
{
    int maxArea=0;
    int j=container.size()-1;
    int i=0;
    while(i<j)
    {
        if(container[i]<container[j]&&i<j)
        {
            maxArea=max(maxArea,container[i]*(j-i));
            i++;
        }
        else{
            maxArea=max(maxArea,container[j]*(j-i));
            j--;
        }
    }
    return maxArea;
}