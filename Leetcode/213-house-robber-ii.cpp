#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int rob(vector<int> &nums)
{
    if (nums.empty())
        return 0;
    if (nums.size() == 1)
        return nums[0];
    int n = nums.size();
    vector<int> dp_1(n + 1, 0);
    vector<int> dp_2(n + 1, 0);
    dp_1[1] = nums[0];
    for (int i = 2; i <= n - 1; i++)
    {
        dp_1[i] = max(dp_1[i - 1], dp_1[i - 2] + nums[i - 1]);
    }
    dp_2[2] = nums[1];
    for (int i = 3; i <= n; i++)
    {
        dp_2[i] = max(dp_2[i - 1], dp_2[i - 2] + nums[i - 1]);
    }
    return max(dp_1[n - 1], dp_2[n]);
}