#include <iostream>
#include <vector>
using namespace std;

void backtracking(vector<vector<int>> &graph, vector<int> arr, vector<vector<int>> &res, int cur_node)
{
    if (cur_node == graph.size() - 1)
    {
        res.push_back(arr);
        return;
    }
    for (auto vec : graph[cur_node])
    {
        arr.push_back(vec);
        backtracking(graph, arr, res, vec);
        arr.pop_back();
    }
}

vector<vector<int>> allPathsSourceTarget(vector<vector<int>> &graph)
{
    vector<vector<int>> res;
    backtracking(graph, vector<int>({0}), res, 0);
    return res;
}