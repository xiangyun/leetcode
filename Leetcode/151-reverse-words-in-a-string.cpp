#include <iostream>
#include <string>
#include <stack>
using namespace std;
void reverseWords(string &s)
{
    stack<string> stk;
    string res = "", temp = "";
    for (int i = 0; i < s.size(); i++)
    {
        if (s[i] != ' ')
            temp += s[i];
        else
        {
            if (temp != "")
                stk.push(temp);
            temp = "";
        }
    }
    if (temp != "")
        stk.push(temp);
    while (!stk.empty())
    {
        res += stk.top() + " ";
        stk.pop();
    }
    if (res != "")
        res = res.substr(0, res.size() - 1);
    s = res;
}