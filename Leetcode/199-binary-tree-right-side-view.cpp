#include <iostream>
#include <vector>
#include <queue>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
/*
vector<int> rightSideView(TreeNode *root)
{
    if (!root)
        return vector<int>();
    queue<pair<TreeNode *, int>> que;
    vector<int> res;
    int depth, prev_val = INT_MIN;
    if (root)
    {
        que.emplace(root, 1);
        prev_val = root->val;
        depth = 1;
    }
    while (!que.empty())
    {
        pair<TreeNode *, int> cur = que.front();
        if (cur.first->left)
        {
            que.emplace(cur.first->left, cur.second + 1);
        }
        if (cur.first->right)
        {
            que.emplace(cur.first->right, cur.second + 1);
        }
        if (depth != cur.second)
        {
            res.push_back(prev_val);
            depth++;
        }
        prev_val = cur.first->val;
        que.pop();
    }
    res.push_back(prev_val);
    return res;
}*/

void view(TreeNode *root, vector<int> &arr, int depth)
{
    if (root)
    {
        if (arr.size() < depth)
            arr.push_back(root->val);
    }
    if (root->right)
        view(root->right, arr, depth + 1);
    if (root->left)
        view(root->left, arr, depth + 1);
}

vector<int> rightSideView(TreeNode *root)
{
    vector<int> res;
    view(root, res, 1);
    return res;
}