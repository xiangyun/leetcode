#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

void backtracking(string s, vector<string> &res, int cur_index)
{
    if (cur_index == s.size())
    {
        res.push_back(s);
        return;
    }
    if (s[cur_index] >= 'a' && s[cur_index] <= 'z')
    {
        backtracking(s, res, cur_index + 1);
        s[cur_index] -= 'a' - 'A';
        backtracking(s, res, cur_index + 1);
    }
    else if (s[cur_index] >= 'A' && s[cur_index] <= 'Z')
    {
        backtracking(s, res, cur_index + 1);
        s[cur_index] += 'a' - 'A';
        backtracking(s, res, cur_index + 1);
    }
    else
        backtracking(s, res, cur_index + 1);
}

vector<string> letterCasePermutation(string S)
{
    vector<string> res;
    backtracking(S, res, 0);
    sort(res.begin(), res.end());
    return res;
}