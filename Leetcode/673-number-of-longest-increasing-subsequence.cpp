#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int findNumberOfLIS(vector<int> &nums)
{
    int n = nums.size();
    vector<int> length(n, 0);
    vector<int> count(n, 0);
    length[0] = 1;
    count[0] = 1;
    for (int i = 1; i < n; i++)
    {
        int max_len = 1;
        for (int j = 0; j < i; j++)
        {
            if (nums[i] > nums[j])
                max_len = max(max_len, length[j] + 1);
        }
        length[i] = max_len;
        for (int j = 0; j < i; j++)
        {
            if (nums[i] > nums[j] && length[i] == length[j] + 1)
                count[i]++;
        }
    }
    return count[n - 1];
}