#include <iostream>
#include <vector>
using namespace std;

bool isValid(int r, int s, int m, int n)
{
    return r >= 0 && s >= 0 && r < m && s < n;
}

int findPaths(int m, int n, int N, int i, int j)
{
    if (N == 0)
        return 0;
    vector<vector<vector<int>>> dp(N, vector<vector<int>>(m, vector<int>(n, 0)));
    int mod = pow(10, 9) + 7;
    int dx[4] = {1, -1, 0, 0}, dy[4] = {0, 0, 1, -1};
    dp[0][i][j] = 1;
    int res = 0;
    for (int t = 0; t < N; t++)
    {
        for (int r = 0; r < m; r++)
        {
            for (int s = 0; s < n; s++)
            {
                if (t > 0)
                {
                    for (int p = 0; p < 4; p++)
                    {
                        int x = r + dx[p], y = s + dy[p];
                        if (isValid(x, y, m, n))
                            dp[t][r][s] = (dp[t][r][s] + dp[t - 1][x][y]) % mod;
                    }
                }
                if (r == 0)
                    res = (res + dp[t][0][s]) % mod;
                if (r == m - 1)
                    res = (res + dp[t][m - 1][s]) % mod;
                if (s == 0)
                    res = (res + dp[t][r][0]) % mod;
                if (s == n - 1)
                    res = (res + dp[t][r][n - 1]) % mod;
            }
        }
    }
    return res;
}