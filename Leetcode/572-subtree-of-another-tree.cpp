#include <iostream>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left, *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

bool isSameTree(TreeNode *r1, TreeNode *r2)
{
    if (!r1 && !r2)
        return true;
    else if (!r1 || !r2)
        return false;
    else
    {
        if (r1->val != r2->val)
            return false;
        else
            return isSameTree(r1->left, r2->left) && isSameTree(r1->right, r2->right);
    }
}

bool isSubTree(TreeNode *s, TreeNode *t)
{
    if (!s)
        return false;
    if (s->val == t->val)
    {
        return isSameTree(s, t) || isSubTree(s->left, t) || isSubTree(s->right, t);
    }
    else
        return isSubTree(s->left, t) || isSubTree(s->right, t);
}