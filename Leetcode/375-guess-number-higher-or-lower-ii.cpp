#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int getMoneyAmount(int n)
{
    vector<vector<int>> dp(n + 1, vector<int>(n + 1, 0));
    for (int i = n; i >= 1; i--)
    {
        for (int j = i; j <= n; j++)
        {
            if (i == j)
                dp[i][j] = 0;
            else
            {
                dp[i][j] = INT_MAX;
                for (int x = i; x < j; x++)
                {
                    dp[i][j] = min(dp[i][j], max(dp[i][x - 1], dp[x + 1][j]) + x);
                }
            }
        }
    }
    return dp[1][n];
}
