#include <iostream>
#include <vector>
using namespace std;

void backtracking(vector<int> &nums, int cur_sum, int cur_index, int S, int &cnt)
{
    if (cur_index >= nums.size())
    {
        if (cur_sum == S)
            ++cnt;
        return;
    }
    backtracking(nums, cur_sum + nums[cur_index], cur_index + 1, S, cnt);
    backtracking(nums, cur_sum - nums[cur_index], cur_index + 1, S, cnt);
}

int findTargetSumWays(vector<int> &nums, int S)
{
    int cnt = 0;
    backtracking(nums,0,0,S,cnt);
    return cnt;
}