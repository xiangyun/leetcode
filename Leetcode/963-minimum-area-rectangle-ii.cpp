#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <string>
using namespace std;
double dist(vector<int> &p1, vector<int> &p2)
{
    return (p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]);
}

double square(vector<int> &p1, vector<int> &p2, vector<int> &p3)
{
    return sqrt(dist(p1, p2)) * sqrt(dist(p2, p3)); //p1->p2, p2->p3
}

bool dot(vector<int> &p1, vector<int> &p2, vector<int> &p3)
{
    return (p2[0] - p1[0]) * (p3[0] - p2[0]) + (p2[1] - p1[1]) * (p3[1] - p2[1]) == 0;
}

double minAreaFreeRect(vector<vector<int>> &points)
{
    unordered_map<string, int> hash;
    for (auto &i : points)
        hash[to_string(i[0]) + " " + to_string(i[1])]++;
    double min_area = 1.79769e+308;
    for (auto &p1 : points)
    {
        for (auto &p2 : points)
        {
            if (dist(p1, p2) == 0)
                continue;
            for (auto &p3 : points)
            {
                if (dist(p2, p3) == 0 || !dot(p1, p2, p3))
                    continue;
                int x = p3[0] + p1[0] - p2[0];
                int y = p3[1] + p1[1] - p2[1];
                string s = to_string(x) + " " + to_string(y);
                if (hash[s] == 0)
                    continue;
                double area = square(p1, p2, p3);
                min_area = min(min_area, area);
            }
        }
    }
    return min_area == 1.79769e+308 ? 0 : min_area;
}