#include <iostream>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder)
{
    if (preorder.empty())
        return NULL;
    TreeNode *root = new TreeNode(preorder[0]);
    if (preorder.size() == 1)
        return root;
    vector<int> pre_left, pre_right, in_left, in_right;
    int pivot;
    for (int i = 0; i < inorder.size(); i++)
    {
        if (inorder[i] == preorder[0])
        {
            pivot = i;
            break;
        }
    }
    for (int i = 0; i < pivot; i++)
    {
        in_left.push_back(inorder[i]);
        pre_left.push_back(preorder[i + 1]);
    }
    for (int i = pivot + 1; i < inorder.size(); i++)
    {
        in_right.push_back(inorder[i]);
        pre_right.push_back(preorder[i]);
    }
    root->left = buildTree(pre_left, in_left);
    root->right = buildTree(pre_right, in_right);
    return root;
}
