#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int numRescueBoats(vector<int> &people, int limit)
{
    std::sort(people.begin(), people.end());
    //vector<vector<int>> res;
    int cnt = 0;
    int i = 0, j = people.size() - 1;
    while (i < j)
    {
        while (i < j && people[i] + people[j] > limit)
        {
            cnt++;
            j--;
        }
        i++;
        j--;
        cnt++;
    }
    return i == j ? cnt + 1 : cnt;
}