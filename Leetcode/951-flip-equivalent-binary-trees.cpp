#include <iostream>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
/*
bool isEqual(TreeNode *root1, TreeNode *root2)
{
    if (root1 && root2)
    {
        if (root1->val == root2->val)
        {
            return isEqual(root1->left, root2->left) && isEqual(root1->right, root2->right);
        }
    }
    if (!root2 && !root2)
        return true;
    return false;
}
*/
bool flipEquiv(TreeNode *root1, TreeNode *root2)
{
    if (!root1 && !root2)
        return true;
    else if (root1 && root2)
    {
        if (root1->val == root2->val)
        {
            return (flipEquiv(root1->left, root2->right) && flipEquiv(root1->right, root2->left)) || (flipEquiv(root1->left, root2->left) && flipEquiv(root1->right, root2->right));
        }
        return false;
    }
    else
        return false;
}