#include <iostream>
#include <vector>
using namespace std;
vector<int> sortArrayByParityII(vector<int> &A)
{
    int i = 0, j = A.size() - 1;
    while (i < j)
    {
        while (i < j && A[i] % 2 == 1)
            ++i;
        while (i < j && A[j] % 2 == 0)
            --j;
        if (i < j)
            swap(A[i], A[j]);
    }
    return A;
}