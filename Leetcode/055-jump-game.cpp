#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

bool canJump(vector<int> &nums)
{
    int reach = nums[0];
    for (int i = 1; i < nums.size() && reach >= i; i++)
    {
        reach = max(reach, i + nums[i]);
    }
    return reach >= nums.size() - 1;
}