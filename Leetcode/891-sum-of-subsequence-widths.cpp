#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>
using namespace std;
int sumSubseqWidths(vector<int> &A)
{
    sort(A.begin(), A.end());
    using ll = long long;
    int n = A.size(), lambda = 1;
    ll res = 0, mod = pow(10, 9) + 7;
    for (int i = 0; i < A.size(); i++)
    {
        res = (res + A[i] * lambda - A[n - i - 1] * lambda) % mod;
        lambda = (lambda << 1) % mod;
    }
    res = (res + mod) % mod;
    return res;
}