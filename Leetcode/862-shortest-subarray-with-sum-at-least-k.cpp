#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>
// #include <queue>
#include <deque>
using namespace std;
// int shortestSubarray(vector<int> &A, int K)
// {
//     priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> pq;
//     int sum = 0;
//     int ans = INT_MAX;
//     for (int i = 0; i < A.size(); i++)
//     {
//         sum += A[i];
//         if (sum >= K)
//             ans = min(ans, i + 1);
//         while (pq.size() && sum - pq.top().first >= K)
//         {
//             auto &p = pq.top();
//             ans = min(ans, i - p.second);
//             pq.pop();
//         }
//         pq.push({sum, i});
//     }
//     return ans == INT_MAX ? -1 : ans;
// }

int shortestSubarray(vector<int> A, int K)
{
    int N = A.size(), res = N + 1;
    vector<int> B(N + 1, 0);
    for (int i = 0; i < N; i++)
        B[i + 1] = B[i] + A[i];
    deque<int> d;
    for (int i = 0; i < N + 1; i++)
    {
        while (d.size() && B[i] - B[d.front()] >= K)
        {
            res = min(res, i - d.front());
            d.pop_front();
        }
        while (d.size() && B[i] <= B[d.back()])
            d.pop_back();
        d.push_back(i);
    }
    return res <= N ? res : -1;
}