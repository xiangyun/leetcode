#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
using namespace std;

string decap(string s)
{
    for (auto &c : s)
    {
        if (c >= 'A' && c <= 'Z')
            c += 32;
    }
    return s;
}

string devow(string s)
{
    s = decap(s);
    for (auto &c : s)
    {
        if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u')
            c = '#';
    }
    return s;
}

vector<string> spellchecker(vector<string> &wordlist, vector<string> &queries)
{
    unordered_map<string, int> hash;
    unordered_map<string, string> cap, vow;
    for (auto &s : wordlist)
    {
        hash[s]++;
        cap.insert({decap(s), s});
        vow.insert({devow(s), s});
    }
    vector<string> res;
    for (auto &s : queries)
    {
        if (hash[s] > 0)
            res.push_back(s);
        else
        {
            string lower = decap(s);
            string devower = devow(s);
            if (cap.count(lower))
                res.push_back(cap[lower]);
            else if (vow.count(devower))
                res.push_back(vow[devower]);
            else
                res.push_back("");
        }
    }
    return res;
}