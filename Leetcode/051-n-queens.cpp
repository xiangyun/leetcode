#include <iostream>
#include <string>
#include <vector>
using namespace std;

bool isSafe(vector<string> arr, int row, int j)
{
    for (int i = 0; i < arr.size(); i++)
    {
        if (arr[row][i] == 'Q' && i != j)
            return false;
        if (arr[i][j] == 'Q' && i != row)
            return false;
        if (i + j - row >= 0 && arr[i][i + j - row] == 'Q' && i != row)
            return false;
        if (j + row - i >= 0 && arr[i][j - i + row] == 'Q' && i != row)
            return false;
    }
    return true;
}

void solve(vector<vector<string>> &matrix, vector<string> &arr, int n, int row)
{

    if (row == n)
    {
        matrix.push_back(arr);
        return;
    }
    for (int j = 0; j < n; j++)
    {
        if (isSafe(arr, row, j))
        {
            arr[row][j] = 'Q';
            solve(matrix, arr, n, row + 1);
            arr[row][j] = '.';
        }
    }
}

vector<vector<string>> solveNQueens(int n)
{
    vector<vector<string>> nQueens;
    vector<string> arr(n, string(n, '.'));
    solve(nQueens, arr, n, 0);
    return nQueens;
}