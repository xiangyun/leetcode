#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
using namespace std;
//https://leetcode.com/problems/sum-of-subarray-minimums/discuss/178876/stack-solution-with-very-detailed-explanation-step-by-step
//monotonous increase stack
//
// for(int i = 0; i < A.size(); i++){
//   while(!in_stk.empty() && in_stk.top() > A[i]){
//     in_stk.pop();
//   }
//   in_stk.push(A[i]);
// }
//
//find the previous less element of each element in a vector with O(n) time
int sumSubarrayMins(vector<int> &A)
{
	stack<int> s1, s2;
	vector<int> left(A.size()), right(A.size());
	int mod = pow(10, 9) + 7, sum = 0;
	s1.push(-1);
	for (int i = 0; i < A.size(); ++i)
	{
		while (s1.top() != -1 && A[s1.top()] > A[i])
			s1.pop();
		left[i] = i - s1.top();
		s1.push(i);
	}
	s2.push(A.size());
	for (int i = A.size() - 1; i >= 0; --i)
	{
		while (s2.top() != A.size() && A[s2.top()] >= A[i])
			s2.pop();
		right[i] = s2.top() - i;
		s2.push(i);
	}
	for (int i = 0; i < A.size(); i++)
		sum = (sum + (A[i] * left[i] * right[i]) % mod) % mod;
	return sum;
}