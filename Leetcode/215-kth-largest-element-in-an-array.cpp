#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int findKthLargest(vector<int> &nums, int k)
{
    std::sort(nums.begin(), nums.end(), [](const int &a, const int &b) { return a > b; });
    return nums[k - 1];
}