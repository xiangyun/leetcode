#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <list>
#include <queue>
using namespace std;
struct Interval
{
    int start;
    int end;
    Interval() : start(0), end(0) {}
    Interval(int s, int e) : start(s), end(e) {}
};
/*
int eraseOverlapIntervals(vector<Interval> &intervals)
{
    auto cmp = [](const Interval &a, const Interval &b) {
        return a.start < b.start;
    };
    std::sort(intervals.begin(), intervals.end());
    vector<Interval> merged;
    int cnt=0;
    for (int i = 0; i < intervals.size(); i++)
    {
        if (i == 0 || merged.back().end <= intervals[i].start)
        {
            merged.push_back(intervals[i]);
        }
        else
        {
            merged.back().end=max(merged.back().end,intervals[i].end);
        }
    }
}*/

// int eraseOverlapIntervals(vector<Interval> &intervals)
// {
//     int n = intervals.size();
//     auto cmp = [](const Interval &a, const Interval &b) {
//         return a.start < b.start;
//     };
//     std::sort(intervals.begin(), intervals.end(), cmp);
//     int res = 0, prev = 0;
//     for (int i = 1; i < n; i++)
//     {
//         if (intervals[prev].end > intervals[i].start)
//         {
//             res++;
//             if (intervals[i].end < intervals[prev].end)
//             {
//                 prev = i;
//             }
//         }
//         else
//         {
//             prev = i;
//         }
//     }
//     return res;
// }
int eraseOverlapIntervals(vector<Interval> &intervals)
{
    auto cmp = [](const Interval &a, const Interval &b) {
        if (a.start == b.start)
            return a.end < b.end;
        return a.start < b.start;
    };
    sort(intervals.begin(), intervals.end(), cmp);
    int res = 0, prev = 0, n = intervals.size();
    for (int i = 1; i < n; i++)
    {
        if (intervals[i].start < intervals[prev].end)
        {
            res++;
            if (intervals[i].end < intervals[prev].end)
                prev = i;
        }
        else
            prev = i;
    }
    return res;
}