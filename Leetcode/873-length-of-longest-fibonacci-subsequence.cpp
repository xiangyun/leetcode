#include <iostream>
#include <vector>
#include <set>
using namespace std;
int lenLongestFibSubseq(vector<int> &A)
{
    set<int> S(A.begin(), A.end());
    int len = 0, max_len = 0;
    for (int i = 0; i < A.size(); i++)
    {
        for (int j = i + 1; j < A.size(); j++)
        {
            int x = A[i];
            int y = A[j];
            int z = x + y;
            while (S.find(z) != S.end())
            {
                x = y;
                y = z;
                z = x + y;
                if (len == 0)
                    len = 3;
                else
                    len++;
            }
            max_len = max_len > len ? max_len : len;
            len = 0;
        }
    }
    return max_len;
}