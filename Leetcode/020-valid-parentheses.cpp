#include <iostream>
#include <stack>
using namespace std;

bool isPair(const char s1, const char s2);
bool isLeftPar(char s);
bool isValidParenthese(string s)
{
    if(s.length()==0)
        return true;
    stack<char> stk;
    stk.push(s[0]);
    for (int i = 1; i < s.length();i++)
    {
        if(isLeftPar(s[i])){
            stk.push(s[i]);
        }
        else
        {
            if(isPair(stk.top(), s[i]))
                stk.pop();
            else
                return false;
        }
    }
    if(stk.size()==0)
        return true;
    return false;
}
bool isPair(const char s1,const char s2)
{
    if(s1=='('&&s2==')')
        return true;
    if(s1=='['&&s2==']')
        return true;
    if(s1=='{'&&s2=='}')
        return true;
    return false;
}
bool isLeftPar(char s)
{
    return s == '(' || s == '{' || s == '[';
}