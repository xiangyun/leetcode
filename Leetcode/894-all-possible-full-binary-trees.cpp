#include <iostream>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left, *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
class Solution
{
  public:
    vector<TreeNode *> allPossibleFBT(int N)
    {
        vector<TreeNode *> res;
        if (N == 1)
        {
            TreeNode *root = new TreeNode(0);
            res.push_back(root);
            return res;
        }
        if (N < 1 || N % 2 == 0)
            return res;
        for (int i = 1; i < N; i += 2)
        {
            vector<TreeNode *> left = allPossibleFBT(i), right = allPossibleFBT(N - i - 1);
            for (int j = 0; j < left.size(); j++)
            {
                for (int k = 0; k < right.size(); k++)
                {
                    TreeNode *root = new TreeNode(0);
                    root->left = left[j];
                    root->right = right[k];
                    res.push_back(root);
                }
            }
        }
        return res;
    }
};