#include <iostream>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left, *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
TreeNode *mergeTrees(TreeNode *t1, TreeNode *t2)
{
    TreeNode *root;
    if (t1 && t2)
    {
        root = new TreeNode(t1->val + t2->val);
        TreeNode *left = mergeTrees(t1->left, t2->left), *right = mergeTrees(t1->right, t2->right);
        root->left = left;
        root->right = right;
        return root;
    }
    else
    {
        return t1 == NULL ? t2 : t1;
    }
}