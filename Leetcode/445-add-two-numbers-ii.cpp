#include <iostream>
#include <string>
using namespace std;
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

string calc(ListNode *head)
{
    ListNode *cur = head;
    string s = "";
    while (cur)
    {
        s += to_string(cur->val);
        cur = cur->next;
    }
    return s;
}

ListNode *addTwoNumbers(ListNode *l1, ListNode *l2)
{
    string s1 = calc(l1);
    string s2 = calc(l2);
    while (s1.size() < s2.size())
        s1 = "0" + s1;
    while (s1.size() > s2.size())
        s2 = "0" + s2;
    int res = 0;
    ListNode *head = NULL;
    for (int i = s1.size() - 1; i >= 0; i--)
    {
        int cur_val = s1[i] - '0' + s2[i] - '0' + res;
        ListNode *node = new ListNode(cur_val % 10);
        node->next = head;
        head = node;
        res = cur_val / 10;
    }
    if(res!=0)
    {
        ListNode * node = new ListNode(res);
        node->next = head;
        head=node;
    }
    return head;
}