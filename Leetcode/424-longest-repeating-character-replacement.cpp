#include <iostream>
#include <string>
#include <algorithm>
#include <unordered_map>
using namespace std;
int characterReplacement(string s, int k)
{
    int map[256] = {0};
    for(auto &c:s)
    map[c]++;
    int start = 0, end = 0, head = 0, max_len = INT_MIN, count = 0;
    while (end < s.size())
    {
        map[s[end]]++;
        count = max(count, map[s[end]]);
        end++;
        while (end - start - count > k)
        {
            map[s[start]]--;
            start++;
        }
        max_len = max(max_len, end - start);
    }
    return max_len;
}