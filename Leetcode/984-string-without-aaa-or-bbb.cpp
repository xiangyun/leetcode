#include <iostream>
#include <vector>
#include <string>
using namespace std;
string strWithout3a3b(int A, int B)
{
    string s = "";
    if (A > B)
    {
        while (A && B)
        {
            if (A > B)
            {
                s += "aab";
                A -= 2;
                --B;
            }
            else if (A == B)
            {
                s += "ab";
                --A;
                --B;
            }
        }
        while (A)
        {
            s += 'a';
            --A;
        }
    }
    else if (B > A)
    {
        while (A && B)
        {
            if (B > A)
            {
                s += "bba";
                B -= 2;
                --A;
            }
            else if (A == B)
            {
                s += "ba";
                --A;
                --B;
            }
        }
        while (B)
        {
            s += 'b';
            --B;
        }
    }
    else
    {
        while (A)
        {
            s += "ab";
            A--;
        }
    }
    return s;
}