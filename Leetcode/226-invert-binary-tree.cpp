#include <iostream>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
TreeNode *invertTree(TreeNode *root)
{
    if (!root)
        return NULL;
    swap(root->left, root->right);
    root->left = invertTree(root->left);
    root->right = invertTree(root->right);
    return root;
}