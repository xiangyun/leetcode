#include <iostream>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left, *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
void getleaf(TreeNode *root, vector<int> &res)
{
    if (!root)
        return;
    if (!root->left && !root->right)
        res.push_back(root->val);
    getleaf(root->left, res);
    getleaf(root->right, res);
}

bool leafSimilar(TreeNode *root1, TreeNode *root2)
{
    vector<int> res1, res2;
    getleaf(root1, res1);
    getleaf(root2, res2);
    return res1 == res2;
}