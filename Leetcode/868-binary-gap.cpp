#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int binaryGap(int N)
{
    int count = 0;
    int prev = -1;
    int max_len = 0;
    while (N)
    {
        int res = N % 2;
        if (res == 1)
        {
            if (prev == -1)
            {
                prev = count;
            }
            else
            {
                max_len = max(max_len, count - prev);
                prev = count;
            }
        }
        N /= 2;
        count++;
    }
    return max_len;
}