#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;
class Employee
{
  public:
    int id;
    int importance;
    vector<int> subordinates;
};

void helper(unordered_map<int, Employee *> hash, int id, int &cur_sum)
{
    Employee *employee = hash[id];
    cur_sum += employee->importance;
    if (employee->subordinates.empty())
        return;
    for (int i = 0; i < employee->subordinates.size(); i++)
    {
        helper(hash, employee->subordinates[i], cur_sum);
    }
}

int getImportance(vector<Employee *> employees, int id)
{
    unordered_map<int, Employee *> hash;
    for (int i = 0; i < employees.size(); i++)
    {
        hash[employees[i]->id] = employees[i];
    }
    int sum = 0;
    helper(hash, id, sum);
    return sum;
}