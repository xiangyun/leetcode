#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
using namespace std;
/*
int candy(vector<int> &ratings)
{
    int left = 0, right = 0;
    int i = 0;
    int sum = 0;
    while (i < ratings.size() - 1)
    {
        while (i + 1 < ratings.size() && ratings[i] < ratings[i + 1])
        {
            i++;
            left++;
        }
        while (i + 1 < ratings.size() && ratings[i] >= ratings[i + 1])
        {
            i++;
            right++;
        }
        if (left < right)
        {
            sum += (left + 1) * (left) / 2 + (right + 2) * (right + 1) / 2;
        }
        else
        {
            sum += (left + 2) * (left + 1) / 2 + (right) * (right + 1) / 2;
        }
        left = 0;
        right = 0;
    }
    return sum;
}*/
class Solution
{
  public:
    int candy(vector<int> &ratings)
    {
        vector<int> dp(ratings.size(), 1);
        for (int i = 1; i < ratings.size(); i++)
        {
            if (ratings[i - 1] < ratings[i])
                dp[i] = max(dp[i], dp[i - 1] + 1);
        }
        for (int i = ratings.size() - 2; i >= 0; i--)
        {
            if (ratings[i] > ratings[i + 1])
                dp[i] = max(dp[i], dp[i + 1] + 1);
        }
        return std::accumulate(dp.begin(), dp.end(), 0);
    }
};