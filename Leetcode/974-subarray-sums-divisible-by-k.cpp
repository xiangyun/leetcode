#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;
int subarraysDivByK(vector<int> &A, int K)
{
    // unordered_map<int, int> hash;
    // int sum = 0;
    // for (auto i : A)
    // {
    //     sum = (sum + i) % K;
    //     if (sum < 0)
    //         sum += K;
    //     hash[sum]++;
    // }
    // int cnt = 0;
    // for (auto c : hash)
    // {
    //     if (c.first != 0)
    //         cnt += c.second * (c.second - 1) / 2;
    //     else
    //     {
    //         cnt += (hash[0] + 1) * hash[0] / 2;
    //     }
    // }
    // return cnt;
    unordered_map<int, int> hash;
    int sum = 0;
    for (auto i : A)
    {
        sum += ((sum + i) % K + K) % K;
        hash[sum]++;
    }
    int cnt = 0;
    if (hash.count(0))
        hash[0]++;
    for (auto m : hash)
        cnt += m.second * (m.second - 1) / 2;
    return cnt;
}