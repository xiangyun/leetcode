#include <iostream>
#include <string>
#include <vector>
using namespace std;

bool is_valid(char i)
{
    return (i != '0');
}

bool valid(char i, char j)
{
    return (i == '1' || (i == '2' && j <= '6'));
}

int numDecodings(string s)
{
    if (s.size() == 0 || s[0] == '0')
        return 0;
    if (s.size() == 1)
        return 1;
    int n = s.size();
    vector<int> dp(n, 0);
    dp[0] = 1;
    if (is_valid(s[1] && valid(s[0], s[1])))
        dp[1] = 2;
    else
        dp[1] = 1;
    if (!is_valid(s[1]) && !valid(s[0], s[1]))
        return 0;

    for (int i = 2; i < n; i++)
    {
        if (is_valid(s[i]) && valid(s[i - 1], s[i]))
            dp[i] = dp[i - 1] + dp[i - 2];
        if (is_valid(s[i]) && !valid(s[i - 1], s[i]))
            dp[i] = dp[i - 1];
        if (!is_valid(s[i]) && valid(s[i - 1], s[i]))
            dp[i] = dp[i - 2];
        if (!is_valid(s[i]) && !valid(s[i - 1], s[i]))
            return 0;
    }
    return dp[s.size() - 1];
}