#include <iostream>
#include <vector>
#include <string>
using namespace std;
class UnionFind
{
  private:
    int *parent;
    int *rank;
    int count;

  public:
    UnionFind(int N);
    ~UnionFind();
    bool isConnect(int x, int y);
    void Union(int x, int y);
    int find(int x);
    int size();
};

UnionFind::UnionFind(int N)
{
    parent = new int[N];
    rank = new int[N];
    for (int i = 0; i < N; i++)
    {
        parent[i] = i;
        rank[i] = 1;
    }
    count = N;
}
UnionFind::~UnionFind()
{
    delete[] parent;
    delete[] rank;
}

bool UnionFind::isConnect(int x, int y)
{
    return parent[x] == parent[y];
}
int UnionFind::find(int x)
{
    if (x != parent[x])
        parent[x] = find(parent[x]);
    return parent[x];
}
void UnionFind::Union(int x, int y)
{
    int xx = parent[x], yy = parent[y];
    if (xx == yy)
        return;
    else
    {
        if (rank[xx] > rank[yy])
        {
            parent[y] = xx;
            rank[xx] += rank[yy];
        }
        else
        {
            parent[x] = yy;
            rank[xx] += rank[yy];
        }
        --count;
    }
}
int UnionFind::size()
{
    return count;
}

bool equationsPossible(vector<string> &equations)
{
    
}