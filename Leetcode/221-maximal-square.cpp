#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int maximalSquare(vector<vector<char>> matrix) {
	if (matrix.empty())
		return 0;
	int m = matrix.size(), n = matrix[0].size(), max_size = 0;
	vector<vector<int>> dp(m, vector<int>(n, 0));
	for (int i = 0; i < m; i++) {
		dp[i][0] = matrix[i][0] - '0';
		max_size = max(max_size, dp[i][0]);
	}
	for (int j = 0; j < n; j++) {
		dp[0][j] = matrix[0][j] - '0';
		max_size = max(max_size, dp[0][j]);
	}
	for (int i = 1; i < m; i++)
	{
		for (int j = 1; j < n; j++)
		{
			if (matrix[i][j] == '1')
			{
				dp[i][j] = min(min(dp[i][j - 1], dp[i - 1][j]), dp[i - 1][j - 1]) + 1;
				max_size = max(max_size, dp[i][j]);
			}
		}
	}
	return max_size * max_size;
}