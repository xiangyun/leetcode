#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int maxDistatToClosest(vector<int> &seats)
{
    int min_dist = 0;
    vector<int> index;
    for (int i = 0; i < seats.size(); i++)
    {
        if (seats[i] == 1)
            index.push_back(i);
    }
    for (int i = 0; i < seats.size(); i++)
    {
        if (seats[i] == 0)
        {
            int temp_min = 0;
            for (int j = 0; j < index.size(); j++)
            {
                temp_min = min(temp_min, abs(i - index[j]));
            }
            min_dist=max(temp_min,min_dist);
        }
    }
    return min_dist;
}