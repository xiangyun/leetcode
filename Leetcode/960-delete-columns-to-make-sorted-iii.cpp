#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;
int minDeletionSize(vector<string> &A)
{
    int m = A.size(), n = A[0].size(), k, res = n - 1;
    vector<int> dp(n, 1);
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < i; j++)
        {
            for (k = 0; k < m; k++)
            {
                if (A[k][j] > A[k][i])
                    break;
            }
            if (k == m)
                dp[i] = max(dp[j] + 1, dp[i]);
        }
        res = min(res, n - dp[i]);
    }
    return res;
}