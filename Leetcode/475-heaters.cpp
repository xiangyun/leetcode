#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int findRadius(vector<int> &houses, vector<int> &heaters)
{
    sort(houses.begin(), houses.end());
    sort(heaters.begin(), heaters.end());
    vector<int> arr(houses.size(), INT_MAX);
    int i = 0, j = 0;
    for (int i = 0; i < houses.size(); i++)
    {
        while (j < heaters.size() && heaters[j] <= houses[i])
            j++;
        if (j > 0)
            arr[i] = min(arr[i], houses[i] - heaters[j - 1]);
        if (j < heaters.size())
            arr[i] = min(arr[i], heaters[j] - houses[i]);
    }
    return *max_element(arr.begin(), arr.end());
}