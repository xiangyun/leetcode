#include <iostream>
#include <vector>
using namespace std;
int lastRemaining(int n)
{
    vector<int> res;
    for (int i = 0; i < n; i++)
        res.push_back(i + 1);
    int i = 0;
    while (res.size() > 1)
    {
        if (i % 2 == 1)
        {
            vector<int> temp;
            for (int i = 0; i < res.size() / 2; i++)
            {
                temp.push_back(res[2 * i]);
                res = temp;
            }
        }
        else
        {
            vector<int> temp;
            for (int i = 0; i < res.size() / 2; i++)
            {
                temp.push_back(res[2 * i + 1]);
                res = temp;
            }
        }
        i++;
    }
    return res.back();
}