#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
using namespace std;

bool reorderedPowerOf2(int N)
{
	vector<unordered_map<int, int>> hash(32);
	unordered_map<int, int> hash_map;
	while (N)
	{
		hash_map[N % 10]++;
		N /= 10;
	}
	for (int i = 0; i < 32; i++)
	{
		long power_2 = static_cast<long>(1 << i);
		//cout << power_2 << endl;
		while (power_2)
		{
			int res = power_2 % 10;
			hash[i][res]++;
			power_2 /= 10;
		}
		if (hash[i] == hash_map)
			return true;
	}
	return false;
}