#include <iostream>
#include <string>
#include <vector>
/*
input: ["flower","flow","flight"]
output: "fl"
input: ["dog","racecar","car"]
output: ""
*/
using namespace std;
string longestCommonPrefix(vector<string> &strs)//O(n^2)
{
    if(strs.size()==0)
        return "";
    string s = "";
	int min = strs[0].size();
	for (int i = 0; i < strs.size(); i++)
		if (min > strs[i].size())
			min = strs[i].size();
    for (int pos = 0; pos < min;pos++){
        for (int i = 0; i < strs.size() - 1; i++)
        {
            if (strs[i][pos] != strs[i + 1][pos])
                break;
        }
	s += strs[0][pos];
    }
    return s;
}