#include <iostream>
#include <vector>
#include <queue>
#include <functional>
using namespace std;
class MedianFinder
{
  public:
    MedianFinder() {}
    ~MedianFinder() {}
    void addNum(int num)
    {
        if (max_heap.empty() || num < max_heap.top())
            max_heap.push(num);
        else
            min_heap.push(num);
        if (max_heap.size() > min_heap.size() + 1)
        {
            min_heap.push(max_heap.top());
            max_heap.pop();
        }
        else
        {
            max_heap.push(min_heap.top());
            min_heap.pop();
        }
    }
    double findMedain()
    {
        return max_heap.size() == min_heap.size() ? (max_heap.top() + min_heap.top()) / 2.0 : max_heap.top();
    }

  private:
    function<bool(int, int)> cmp = [](int const a, int const b) -> bool { return a > b; };
    priority_queue<int> max_heap;
    priority_queue<int, vector<int>, decltype(cmp)> min_heap;
};