#include <iostream>
#include <vector>
using namespace std;
void backtracking(vector<vector<int>> arr, int index, int cur_sum, int N, int &num)
{

	if (arr.empty() || cur_sum >= N) {
		num ++;
		return;
	}

	for (int j = 0; j < arr[index].size(); j++)
		backtracking(arr, arr[index][j], cur_sum + 1, N, num);
}

int knightDialer(int N)
{
	vector<vector<int>> arr;
	arr.push_back({ 4, 6 });
	arr.push_back({ 6, 8 });
	arr.push_back({ 7, 9 });
	arr.push_back({ 4, 8 });
	arr.push_back({ 3, 9 ,0 });
	arr.push_back({});
	arr.push_back({ 1, 7,0 });
	arr.push_back({ 2, 6 });
	arr.push_back({ 1, 3 });
	arr.push_back({ 2, 4 });

	int count = 0;
	for (int i = 0; i <= 9; i++)
	{
		int num = 0;
		backtracking(arr, i, 0, N - 1, num);
		count = count + num % (int(10e9 + 7));
	}
	return count;
}