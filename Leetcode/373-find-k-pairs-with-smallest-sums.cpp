#include <iostream>
#include <vector>
#include <queue>
using namespace std;
class Solution
{
  public:
    vector<pair<int, int>> kSmallestPairs(vector<int> &nums1, vector<int> &nums2, int k)
    {
        if (nums1.empty() || nums2.empty())
            return vector<pair<int, int>>();
        auto cmp = [](const pair<int, int> a, const pair<int, int> b) {
            return (a.first + a.second) > (b.first + b.second);
        };
        priority_queue<pair<int, int>, vector<pair<int, int>>, decltype(cmp)> heap(cmp);
        for (int i = 0; i < nums1.size(); i++)
        {
            for (int j = 0; j < nums2.size(); j++)
            {
                heap.emplace(nums1[i], nums2[j]);
            }
        }
        vector<pair<int, int>> res;
        int min_k = min(k, (int)(heap.size()));
        for (int i = 0; i < min_k; i++)
        {
            auto it = heap.top();
            heap.pop();
            res.push_back(it);
        }
        return res;
    }
};