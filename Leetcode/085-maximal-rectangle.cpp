#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>
using namespace std;

int max_rec_in_hist(vector<int> arr)
{
    stack<int> stk;
    int res = 0;
    for (int i = 0; i < arr.size(); i++)
    {
        while (!stk.empty() && arr[stk.top()] > arr[i])
        {
            int height = arr[stk.top()];
            int right = i;
            stk.pop();
            int left = stk.empty() ? -1 : stk.top();
            res = max(res, height * (right - left - 1));
        }
        stk.push(i);
    }
    return res;
}

int maximalRectangle(vector<vector<char>> &matrix)
{
    if (matrix.empty())
        return 0;
    int m = matrix.size();
    int n = matrix[0].size();
    vector<vector<int>> mat(m, vector<int>(n, 0));
    mat[0].push_back(0);
    for (int i = 0; i < matrix[0].size(); i++)
        if (matrix[0][i] == '1')
            mat[0][i] = 1;
    for (int i = 1; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (matrix[i][j] == '1')
                mat[i][j] = mat[i - 1][j] + 1;
        }
        mat[i].push_back(0);
    }
    int res = 0;
    for (int i = 0; i < mat.size(); i++)
        res = max(res, max_rec_in_hist(mat[i]));
    return res;
}