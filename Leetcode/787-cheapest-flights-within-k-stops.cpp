#include <iostream>
#include <vector>
using namespace std;

int findCheapestPrice(int n, vector<vector<int>> &flights, int src, int dst, int K)
{
    const int INF = 1e9;
    vector<vector<int>> dp(K + 2, vector<int>(n, INF));
    dp[0][src] = 0;
    for (int i = 1; i <= K + 1; ++i)
    {
        dp[i][src] = 0;
        for (const auto &p : flights)
            dp[i][p[1]] = fmin(dp[i][p[1]], dp[i - 1][p[0]] + p[2]);
    }
    return dp[K + 1][dst] >= INF ? -1 : dp[K + 1][dst];
}
//bellman-ford's algorithm