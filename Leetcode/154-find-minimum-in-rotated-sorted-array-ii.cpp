#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int finMin(vector<int> &nums)
{
	if (nums.size() == 0)
		return 0;
	if (nums.size() == 1)
		return nums[0];
	int left = 0, right = nums.size() - 1, res = INT_MAX;
	if (nums[left] < nums[right])
		return nums[left];
	while (left < right)
	{
		int mid = (left + right) / 2;
		if (nums[left] < nums[mid])
		{
			res = min(nums[left], res);
			left = mid;

		}
		else if (nums[left] > nums[mid])
		{
			res = min(nums[mid], res);
			right = mid;

		}
		else
		{
			left++;
		}
	}
	res = min(res, min(nums[left], nums[right]));
	return res;
}