#include <iostream>
#include <vector>
#include <random>
#include <cmath>
using namespace std;
class Solution
{
  public:
    Solution(double radius, double x_center, double y_center)
    {
        r = radius;
        x_0 = x_center;
        y_0 = y_center;
        pi = atan(1) * 4;
    }

    vector<double> randPoint()
    {
        double t = randZeroToOne();
        double len = r * sqrt(randZeroToOne());
        double x = x_0 + len * cos(2 * pi * t);
        double y = y_0 + len * sin(2 * pi * t);
        return vector<double>({x, y});
    }

    double randZeroToOne()
    {
        return rand() / (RAND_MAX + 1.0);
    }

  private:
    double r, x_0, y_0, pi;
};