#include <iostream>
#include <string>
#include <vector>
using namespace std;
vector<int> diStringMatch(string S)
{
    int min = 0, max = S.size();
    vector<int> arr;
    for (int i = 0; i < S.size(); i++)
    {
        if (S[i] == 'D')
        {
            arr.push_back(max);
            max--;
        }
        else
        {
            arr.push_back(min);
            min++;
        }
    }
    arr.push_back(max);
    return arr;
}