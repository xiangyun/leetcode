#include <iostream>
using namespace std;
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

ListNode *rotate(ListNode *head)
{
    ListNode *p = head;
    while (p->next->next != NULL)
        p = p->next;
    ListNode *q = p->next;
    q->next = head;
    p->next = NULL;
    head = q;
    return head;
}
int length(ListNode * head)
{
    ListNode * p=head;
    int count=0;
    while(p)
    {
        count++;
        p=p->next;
    }
    return count;
}
ListNode *rotateRight(ListNode *head, int k)
{

    if (head == NULL || head->next == NULL)
        return head;
    int num=length(head);
    num=k%num;
    for (int i = 1; i <= num; i++)
        head = rotate(head);
    return head;
}