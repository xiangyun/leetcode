#include <iostream>
#include <string>
#include <stack>
using namespace std;

string helper(int &pos, string s)
{
    string str = "";
    int num = 0;
    for (; pos < s.size(); pos++)
    {
        if (s[pos] == '[')
        {
            pos++;
            string cur = helper(pos, s);
            //cout<<cur<<endl;
            while (num > 0)
            {
                str += cur;
                num--;
            }
        }
        else if (s[pos] == ']')
        {
            return str;
        }
        else if (s[pos] >= '0' && s[pos] <= '9')
        {
            num = num * 10 + s[pos] - '0';
        }
        else
        {
            str += s[pos];
        }
    }
    return str;
}
string decodeString(string s)
{
    int pos = 0;
    return helper(pos, s);
}