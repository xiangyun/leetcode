#include <iostream>
#include <vector>
#include <queue>
using namespace std;
/*
int binarySearch(vector<int> arr, int k)
{
    int left = 0, right = arr.size() - 1;
    while (left <= right)
    {
        int mid = (right - left) / 2 + left;
        if (arr[mid] > left)
        {
            right = mid - 1;
        }
        else if (arr[mid] < right)
        {
            left = mid + 1;
        }
        else
            return mid;
    }
    return -1;
}
*/
/*
int kthSmallest(vector<vector<int>> &matrix, int k)
{
    priority_queue<int> heap;
    for (int i = 0; i < matrix.size(); i++)
    {
        for (int j = 0; j < matrix[0].size(); j++)
        {
            heap.emplace(matrix[i][j]);
            if (heap.size() > k)
                heap.pop();
        }
    }
    return heap.top();
}*/
int get_less_count(vector<vector<int>> &matrix, int target)
{
    int n = matrix.size();
    int i = n - 1, j = 0;
    int result = 0;
    while (i >= 0 && j < n)
    {
        if (matrix[i][j] <= target)
        {
            result += i + 1;
            j++;
        }
        else
        {
            i--;
        }
    }
    return result;
}
int kthSmallest(vector<vector<int>> &matrix, int k)
{
    int low = matrix[0][0];
    int high = matrix.back().back();
    while (low < high)
    {
        int mid = (low + high) / 2;
        int cnt = get_less_count(matrix, mid);
        if (cnt < k)
        {
            low = mid + 1;
        }
        else
            high = mid;
    }
    return low;
}