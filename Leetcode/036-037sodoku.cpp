#include <iostream>
#include <string>
#include <vector>
using namespace std;
/*
    typical backtracking algorithm
*/
#define UNCOMPLETED 0;

class SodokuSolver{
    public:
      SodokuSolver(vector<vector<char>> board);

      SodokuSolver(vector<vector<int>> board);

      ~SodokuSolver();

    bool solveSodoku()
    {
        int Row, Col;
        if(!findUnassignedLocation(Row,Col))
            return true;
        for (int i = 1; i < 9;i++)
        {
            if(isSafe(Row,Col,i))
            {
                sodoku[Row][Col] = i;
                if(solveSodoku())
                    return true;
                sodoku[Row][Col] = UNCOMPLETED;
            }
        }
        return false;
    }
    void printSodoku();

  private:
    bool usedInRow(int Row, int num);

    bool usedInCol(int Col, int num);

    bool usedInBox(int boxStartRow, int boxStartCol, int num);

    bool isSafe(int Row, int Col, int num);

    bool findUnassignedLocation(int &Row, int &Col);

    vector<vector<int>> sodoku;
};



int main()
{
    vector<vector<char>> board(9, vector<char>(9));
    for (int i = 0; i < 9;i++)
    {
        for (int j = 0; i < 9;j++)
        {
            char temp;
            cin >> temp;
            board[i][j] = temp;
        }
    }
}