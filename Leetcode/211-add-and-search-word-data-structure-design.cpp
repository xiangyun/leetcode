#include <iostream>
#include <string>
using std::string;

struct TrieNode
{
    bool isWord;
    TrieNode *next[26];
    TrieNode()
    {
        isWord = false;
        memset(next, NULL, sizeof(next));
    }
    ~TrieNode()
    {
        for (int i = 0; i < 26; i++)
            if (next[i])
                delete next[i];
    }
};

class WordDictionary
{
  public:
    WordDictionary()
    {
        root = new TrieNode();
    }
    ~WordDictionary()
    {
        delete root;
    }
    void addWord(string word)
    {
        TrieNode *p = root;
        for (auto c : word)
        {
            if (!p->next[c - 'a'])
                p->next[c - 'a'] = new TrieNode();
            p = p->next[c - 'a'];
        }
        p->isWord = true;
    }
    bool search(string word)
    {
        TrieNode *p = search_word(root, word);
        return p && p->isWord;
    }

  private:
    TrieNode *root;
    TrieNode *search_word(TrieNode *p, string word)
    {
        for (int i = 0; i < word.size() && p; i++)
        {
            if (word[i] == '.')
            {
                for (int j = 0; j < 26; j++)
                {
                    if (p->next[j])
                    {
                        int len = word.size() - i - 1;
                        if (len >= 1)
                        {
                            TrieNode *res = search_word(p->next[j], word.substr(i + 1, len));
                            if (res && res->isWord)
                                return res;
                        }
                        else if (p->next[j]->isWord)
                            return p->next[j];
                    }
                }
                return NULL;
            }
            else
            {
                p = p->next[word[i] - 'a'];
            }
        }
        return p;
    }
};