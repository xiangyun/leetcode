#include <iostream>
#include <vector>
#include <string>
using namespace std;
int findNthDigit(int n)
{
    long long cnt = 1, base = 9, sum = 0;
    sum += cnt * base;
    while (sum < n)
    {
        cnt++;
        base *= 10;
        sum += cnt * base;
    }
    sum -= cnt * base;
    n -= sum;
    if (cnt == 1)
    {
        return n;
    }
    string s = to_string((n + 1) / cnt + pow(10, cnt - 1) - 1);
    long long index = (n - 1) % cnt;
    return s[index] - '0';
}