#include <iostream>
#include <vector>
using namespace std;

int powerMod(int x, int n, int mod)
{
    int ans = 1;
    x = x % mod;
    while (n)
    {
        if (n % 2 == 1)
            ans = (ans * x) % mod;
        n /= 2;
        x = (x * x) % mod;
    }
    return ans;
}

int superPow(int a, vector<int> &b)
{
    int res = 0;
    for (auto &i : b)
        res = (res * 10 + i) % 1140;
    if (res == 0)
        return 1;
    return powerMod(a, res, 1140);
}