#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;
int firstUniqChar(string s)
{
    if (s.empty())
        return -1;
    unordered_map<char, int> hash;
    for (auto &i : s)
    {
        hash[i]++;
    }
    for (int i = 0; i < s.size(); i++)
    {
        if (hash[s[i]] == 1)
            return i;
        continue;
    }
    return -1;
}