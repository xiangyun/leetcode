#include <iostream>
#include <unordered_map>
#include <vector>
#include <algorithm>
using namespace std;
int numFactoredBinaryTrees(vector<int> &A)
{
    unordered_map<int, long> hash, dp;
    for (auto &i : A)
    {
        hash[i]++;
        dp[i] = 1;
    }
    sort(A.begin(), A.end());
    long mod = pow(10, 9) + 7;
    // vector<int> dp(A.back() + 1, 1);
    long sum = 0;
    for (int k = 0; k < A.size(); k++)
    {
        int cur = A[k];
        for (int j = 0; j < k; j++)
        {
            if (cur % A[j] == 0 && hash[cur / A[j]])
                dp[cur] = (dp[cur] + dp[A[j]] * dp[cur / A[j]]) % mod;
        }
        sum = (sum + dp[cur]) % mod;
    }
    return sum;
}