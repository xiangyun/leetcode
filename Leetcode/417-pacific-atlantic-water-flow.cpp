#include <iostream>
#include <vector>
using namespace std;

void backtracking(int x, int y, vector<vector<bool>> &visited, vector<vector<int>> &matrix, int prev)
{
    if (x < 0 || y < 0 || x >= visited.size() || y >= visited[0].size() || visited[x][y] || matrix[x][y] < prev)
        return;
    visited[x][y] = true;
    int cur_prev = matrix[x][y];
    backtracking(x, y - 1, visited, matrix, cur_prev);
    backtracking(x, y + 1, visited, matrix, cur_prev);
    backtracking(x - 1, y, visited, matrix, cur_prev);
    backtracking(x + 1, y, visited, matrix, cur_prev);
}

vector<pair<int, int>> pacificAtlantic(vector<vector<int>> &matrix)
{
    vector<pair<int, int>> res;
    if (matrix.empty())
        return res;
    vector<vector<bool>> Pacific(matrix.size(), vector<bool>(matrix[0].size(), false));
    vector<vector<bool>> Atlantic(matrix.size(), vector<bool>(matrix[0].size(), false));
    for (int i = 0; i < matrix.size(); i++)
    {
        backtracking(i, matrix[0].size() - 1, Atlantic, matrix, matrix[i].back());
        backtracking(i, 0, Pacific, matrix, matrix[i][0]);
    }
    for (int j = 0; j < matrix[0].size(); j++)
    {
        backtracking(0, j, Pacific, matrix, matrix[0][j]);
        backtracking(matrix.size() - 1, j, Atlantic, matrix, matrix.back()[j]);
    }
    for (int i = 0; i < matrix.size(); i++)
    {
        for (int j = 0; j < matrix[0].size(); j++)
        {
            if (Atlantic[i][j] && Pacific[i][j])
                res.emplace_back(i, j);
        }
    }
    return res;
}