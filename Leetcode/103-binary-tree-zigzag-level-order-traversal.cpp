#include <iostream>
#include <vector>
#include <stack>
#include <queue>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

/*vector<vector<int>> zigzagLevelOrder(TreeNode *root)
{
    vector<vector<int>> res;
    if (!root)
        return res;
    queue<TreeNode *> que;
    stack<TreeNode *> stk;
    que.push(root);
    int count = 1;
    while (!que.empty() && !stk.empty())
    {
        if (count % 2 == 1)
        {
            if (!que.empty())
            {
                int size = que.size() - 1;
                vector<int> level;
                for (; size >= 0; size--)
                {
                    TreeNode *node = que.front();
                    level.push_back(node->val);
                    if (node->left)
                        stk.push(node->left);
                    if (node->right)
                        stk.push(node->right);
                    que.pop();
                }
                res.push_back(level);
            }
        }
        else
        {
            if (!stk.empty())
            {
                int size = stk.size() - 1;
                vector<int> level;
                for (; size >= 0; size--)
                {
                    TreeNode *node = stk.top();
                    level.push_back(node->val);
                    if (node->left)
                        que.push(node->left);
                    if (node->right)
                        que.push(node->right);
                    stk.pop();
                }
                res.push_back(level);
            }
        }
        count++;
    }
    return res;
}*/

void dfs(TreeNode *root, int depth, vector<vector<int>> &res)
{
    if (!root)
        return;
    if (res.size() == depth)
        res.push_back(vector<int>());
    if (depth % 2 == 0)
        res[depth].push_back(root->val);
    else
        res[depth].insert(res[depth].begin(), root->val);
    dfs(root->left, depth + 1, res);
    dfs(root->right, depth + 1, res);
}

vector<vector<int>> zigzagLevelOrder(TreeNode *root)
{
    vector<vector<int>> res;
    dfs(root, 0, res);
    return res;
}