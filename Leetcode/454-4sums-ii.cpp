#include <iostream>
#include <unordered_map>
#include <vector>
#include <algorithm>
using namespace std;
int fourSumCount(vector<int> &A, vector<int> &B, vector<int> &C, vector<int> &D)
{
    int cnt = 0;
    unordered_map<int, int> hash;
    for (auto &a : A)
        for (auto &b : B)
            hash[a + b]++;
    for (auto &c : C)
        for (auto &d : D)
            cnt += hash[-c - d];
    return cnt;
}