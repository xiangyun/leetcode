#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int longestOnes(vector<int> &A, int K)
{
    int i = 0, j = 0, zero_cnt = 0, max_len = 0;
    for (; j < A.size(); ++j)
    {
        if (A[j] == 0)
            ++zero_cnt;
        while (zero_cnt > K)
        {
            if (A[i] == 0)
                --zero_cnt;
            ++i;
        }
        max_len = max(max_len, j - i + 1);
    }
    return max_len;
}