#include <iostream>
#include <string>
using namespace std;
int titleToNumber(string s)
{
    int val = 0;
    for (auto &c : s)
    {
        val = val * 26 + (c - 'A' + 1);
    }
    return val;
}