#include <iostream>
#include <vector>
using namespace std;
vector<int> findDiagonalOrder(vector<vector<int>> &matrix)
{
    if (matrix.empty())
        return vector<int>();
    vector<int> res;
    int m = matrix.size(), n = matrix[0].size();
    int i = 0, j = 0, cnt = 0;
    while (i >= 0 && j >= 0 && i < m && j < n)
    {
        if (cnt % 2 == 1)
        {
            while (i >= 0 && j >= 0 && i < m && j < n)
            {
                res.push_back(matrix[i][j]);
                i++;
                j--;
            }
            if (i < m)
            {
                j++;
            }
            else
            {
                i--;
                j += 2;
            }
        }
        else
        {
            while (i >= 0 && j >= 0 && i < m && j < n)
            {
                res.push_back(matrix[i][j]);
                i--;
                j++;
            }
            if (j < n)
            {
                i++;
            }
            else
            {
                j--;
                i += 2;
            }
        }
        cnt++;
    }
    return res;
}