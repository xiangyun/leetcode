#include <iostream>
using namespace std;
struct ListNode{
    int val;
    ListNode *next;
    ListNode(int x):val(x),next(NULL){}
};
ListNode *reverseK(ListNode *start, ListNode *end);
ListNode * reverseKGroup(ListNode * head, int k)
{
    if(!head)
        return NULL;
    ListNode *node = head;
    for (int i = 0; i < k;i++)
    {
        if(node)
            node = node->next;
        else
            return head;
    }
    ListNode * head_ = reverseK(head, node);
    head->next = reverseKGroup(node, k);
    return head_;
}

ListNode * reverseK(ListNode * start, ListNode * end)
{
    ListNode *head = end;
    while(start!=end)
    {
        ListNode * temp=start->next;
        start->next = head;
        head = start;
        start = temp;
    }
    return head;
}
