#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int maxProfit(vector<int> &prices)
{
    if(prices.empty())
    return 0;
    int profit = 0, minPrice = INT_MAX;
    for (int i = 0; i < prices.size(); i++)
    {
        if (minPrice > prices[i])
            minPrice = prices[i];
        if (prices[i] - minPrice > profit)
            profit = prices[i] - minPrice;
    }
    return profit;
}