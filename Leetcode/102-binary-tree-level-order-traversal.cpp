#include <iostream>
#include <vector>
#include <queue>
using std::queue;
using std::vector;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
vector<vector<int>> levelOrder(TreeNode *root)
{
    vector<vector<int>> res;
    if (!root)
        return res;
    queue<TreeNode *> que;
    que.push(root);
    //res.push_back(vector<int>(root->val));=>[0,0,0,0,0,0,0,0,0]
    while (!que.empty())
    {
        int size = que.size() - 1;
        vector<int> level;
        for (; size >= 0; size--)
        {
            TreeNode *node = que.front();
            level.push_back(node->val);
            if (node->left)
                que.push(node->left);
            if (node->right)
                que.push(node->right);
            que.pop();
        }
        res.push_back(level);
    }
    return res;
}