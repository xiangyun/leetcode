#include <iostream>
#include <vector>
#include <queue>
#include <list>
using namespace std;
/*
DAG,directed acyclic graph
Toplogical sorting
*/

bool canFinish(int numCourses, vector<pair<int, int>> &prerequisites)
{
    int *inDegree = new int[numCourses];
    list<int> *adjList = new list<int>[numCourses];
    queue<int> vec;
    for (int i = 0; i < numCourses; i++)
        inDegree[i] = 0;
    for (auto c : prerequisites)
    {
        adjList[c.first].push_back(c.second);
        inDegree[c.second]++;
    }
    for (int i = 0; i < numCourses; i++)
    {
        if (inDegree[i] == 0)
            vec.push(i);
    }
    int count = 0;
    while (!vec.empty())
    {
        int v = vec.front();
        vec.pop();
        for (auto it = adjList[v].begin(); it != adjList[v].end(); it++)
        {
            inDegree[*it]--;
            if (inDegree[*it] == 0)
                vec.push(*it);
        }
        count++;
    }
    delete[] adjList;
    delete[] inDegree;
    return count == numCourses;
}