#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
// int maxTurbulenceSize(vector<int> &A)
// {
//     if (A.size() == 1)
//         return 1;
//     if (A.size() == 2)
//     {
//         if (A[0] != A[1])
//             return 2;
//         else
//             return 1;
//     }
//     int i = 2;
//     int max_len = 0, start = -1, len = 0;
//     if (A[0] != A[1] || A[1] != A[2])
//     {
//         max_len = 2;
//     }
//     while (i <= A.size() - 1)
//     {
//         if (((A[i] - A[i - 1]) > 0 && (A[i - 1] - A[i - 2]) < 0) || ((A[i] - A[i - 1]) > 0 && (A[i - 1] - A[i - 2]) < 0))
//         {
//             if (start != -1)
//                 start = i - 2;
//         }
//         else
//         {
//             if (start != -1)
//             {
//                 len = i - start;
//                 max_len = max(len, max_len);
//                 start = -1;
//             }
//         }
//         i++;
//     }
//     if (start != -1)
//     {
//         len = i - start;
//         max_len = max(len, max_len);
//         start = -1;
//     }
//     return max_len;
// }

int maxTurbulenceSize(vector<int> &A)
{
    int inc = 1, dec = 1, res = 1;
    for (int i = 1; i < A.size(); i++)
    {
        if (A[i] > A[i - 1])
        {
            inc = dec + 1;
            dec = 1;
        }
        else if (A[i] < A[i - 1])
        {
            dec = inc + 1;
            inc = 1;
        }
        else
        {
            inc = 1;
            dec = 1;
        }
        res = max(res, max(inc, dec));
    }
    return res;
}