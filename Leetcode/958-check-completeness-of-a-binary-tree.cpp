#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};
/*
int minDepth(TreeNode *root)
{
    if (root == NULL)
        return 0;
    if (root->left == NULL && root->right == NULL)
        return 1;
    if (!root->left)
        return minDepth(root->right) + 1;
    if (!root->right)
        return minDepth(root->left) + 1;
    return min(minDepth(root->left), minDepth(root->right)) + 1;
}
int maxDepth(TreeNode *root)
{
    if (!root)
        return 0;
    int left = maxDepth(root->left);
    int right = maxDepth(root->right);
    return max(left, right) + 1;
}

bool isCompleteTree(TreeNode *root)
{
    if (!root)
        return true;
    int min_l = minDepth(root->left);
    int min_r = minDepth(root->right);
    int max_l = maxDepth(root->left);
    int max_r = maxDepth(root->right);
    if (max_l == max_r)
    {
        if (max_l == min_l)
        {
            if (max_r == min_r)
                return true;
            else if (max_r == min_r + 1)
                return isCompleteTree(root->right);
        }
    }
    else if (max_l == max_r + 1)
    {
        if (max_r == min_r)
        {
            if(max_l==min_l+1)
            return isCompleteTree(root->left);
        }
    }
    return false;
}*/

bool isCompleteTree(TreeNode *root)
{
    vector<TreeNode *> vec;
    vec.push_back(root);
    int i = 0;
    while(vec[i])
    {
        vec.push_back(vec[i]->left);
        vec.push_back(vec[i]->right);
        i++;
    }
    while(!vec[i])
    {
        i++;
    }
    return i == vec.size();
}
