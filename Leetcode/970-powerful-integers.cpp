#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>
using namespace std;
vector<int> powerfulIntegers(int x, int y, int bound)
{
    if (x == 1 && y == 1)
    {
        if (bound >= 2)
            return vector<int>({2});
        else
            return vector<int>();
    }
    if (x == 1 || y == 1)
    {
        vector<int> res;
        x = max(x, y);
        int a = 1, b = 1;
        while (a + b <= bound)
        {
            res.push_back(a + b);
            a = a * x;
        }
        return res;
    }
    int i = 0, j = 0;
    unordered_map<int, int> hash;
    vector<int> res;
    int a = 1, b = 1;
    while (a + b <= bound)
    {
        while (a + b <= bound)
        {
            hash[a + b]++;
            a = a * x;
        }
        a = 1;
        b = b * y;
    }
    for (auto &i : hash)
        res.push_back(i.first);
    return res;
}