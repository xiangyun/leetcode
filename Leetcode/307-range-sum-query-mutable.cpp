#include <iostream>
#include <vector>
using namespace std;
struct SegmentTreeNode
{
    int start, end, sum;
    SegmentTreeNode *left, *right;
    SegmentTreeNode(int a, int b) : start(a), end(b), sum(0), left(nullptr), right(nullptr) {}
};
class NumArray
{
  private:
    SegmentTreeNode *root;

    SegmentTreeNode *buildTree(vector<int> &nums, int start, int end)
    {
        if (start > end)
            return nullptr;
        SegmentTreeNode *_root = new SegmentTreeNode(start, end);
        if (start == end)
        {
            _root->sum = nums[start];
            return _root;
        }
        int mid = start + (end - start) / 2;
        _root->left = buildTree(nums, start, mid);
        _root->right = buildTree(nums, mid + 1, end);
        _root->sum = _root->left->sum + _root->right->sum;
        return _root;
    }

    int modifyTree(int i, int val, SegmentTreeNode *&_root)
    {
        if (_root == nullptr)
            return 0;
        int diff;
        if (_root->start == i && _root->end == i)
        {
            diff = val - _root->sum;
            _root->sum = val;
            return diff;
        }
        int mid = (_root->start + _root->end) / 2;
        if (i > mid)
            diff = modifyTree(i, val, _root->right);
        else
            diff = modifyTree(i, val, _root->left);
        _root->sum = _root->sum + diff;
        return diff;
    }
    int queryTree(int i, int j, SegmentTreeNode *&_root)
    {
        if (_root == nullptr)
            return 0;
        if (_root->start == i && _root->end == j)
            return _root->sum;
        int mid = (_root->start + _root->end) / 2;
        if (i > mid)
            return queryTree(i, j, _root->right);
        if (j <= mid)
            return queryTree(i, j, _root->left);
        return queryTree(i, mid, _root->left) + queryTree(mid + 1, j, _root->right);
    }

  public:
    NumArray(vector<int> nums)
    {
        int n = nums.size();
        root = buildTree(nums, 0, n - 1);
    }
    ~NumArray()
    {
        delete root;
    }
    void update(int i, int val)
    {
        modifyTree(i, val, root);
    }
    int sumRange(int i, int j)
    {
        return queryTree(i, j, root);
    }
};

class SegmentTree
{
  public:
  private:
    SegmentTreeNode *buildTree(vector<int> &arr, int start, int end)
    {
        if (start > end)
            return NULL;
        SegmentTreeNode *_root = new SegmentTreeNode(start, end);
        if (start == end)
        {
            _root->sum = arr[start];
            return _root;
        }
        int mid = (end - start) / 2 + start;
        _root->left = buildTree(arr, start, mid);
        _root->right = buildTree(arr, mid + 1, end);
        _root->sum = _root->left->sum + _root->right->sum;
        return _root;
    }

    int modifyTree(int index, int val, SegmentTreeNode *&_root)
    {
        if (!_root)
            return 0;
        int diff;
        if (_root->start == index && _root->end == index)
        {
            diff = val - _root->sum;
            _root->sum = val;
            return diff;
        }
        int mid = (_root->start + _root->end) / 2;
        if (index > mid)
            diff = modifyTree(index, val, _root->right);
        else
            diff = modifyTree(index, val, _root->left);
        _root->sum = _root->sum + diff;
        return diff;
    }
    int queryTree(int i, int j, SegmentTreeNode *_root)
    {
        if (!_root)
            return NULL;
        if (_root->start == i && _root->end == j)
            return _root->sum;
        int mid = (_root->start + _root->end) / 2;
        if (i > mid)
            return queryTree(i, j, _root->right);
        if (j <= mid)
            return queryTree(i, j, _root->left);
        return queryTree(i, mid, _root->left) + queryTree(mid + 1, j, _root->right);
    }

    SegmentTree *root;
};