#include <iostream>
#include <vector>
#include <algorithm>
using namespace std; //height.size()>=2;

int maxArea(vector<int> &height)
{
	int n = height.size();
	vector<vector<int>> dp(n, vector<int>(n, 0));
	for (int i = 0; i < n - 1; i++)
	{
		dp[i][i + 1] = min(height[i], height[i + 1]);
	}
	for (int i = 1; i < n; i++)
	{
		for (int j = i - 1; j >= 0; j--)
		{
			int H = min(height[i], height[j]);
			dp[i][j] = max(max(max(dp[i][j + 1], dp[i - 1][j]), dp[i - 1][j + 1]), H * (i - j));
			//cout << " i = " << i << " j = " << j << " " << dp[i][j] << endl;
		}
	}
	return dp[n-1][0];
}