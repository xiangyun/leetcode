#include <iostream>
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

ListNode *deleteDuplicates(ListNode *head)
{
    if (head == NULL)
        return NULL;
    ListNode *p = head;
    ListNode *q = head->next;
    p->next = NULL;
    while (q)
    {
        if (p->val == q->val)
        {
            ListNode *temp = q;
            q = q->next;
            temp->next = NULL;
            delete temp;
        }
        else
        {
            p->next = q;
            q = q->next;
            p = p->next;
        }
    }
    p->next = NULL;
    return head;
}