#include <iostream>
#include <unordered_map>
#include <algorithm>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

// tuple<int, int, int> helper(TreeNode *root)
// {
//     if (!root)
//         return {0, 0, 1};
//     int left_not_covered, left_covered, left_parent_covered;
//     tie(left_not_covered, left_covered, left_parent_covered) = helper(root->left);
//     int right_not_covered, right_covered, right_parent_covered;
//     tie(right_not_covered, right_covered, right_parent_covered) = helper(root->right);
//     return {left_covered + right_covered,
//             min(left_not_covered + right_not_covered + 1,
//                 min(left_parent_covered + right_covered, left_covered + right_parent_covered)),
//             min(left_not_covered, left_covered) + min(right_not_covered, right_covered) + 1};
// }

// int minCameraCover(TreeNode *root)
// {
// }
int minCameraCover(TreeNode *root, int &res)
{
    if (!root)
        return 2;
    int left = minCameraCover(root->left, res);
    int right = minCameraCover(root->right, res);
    if (left == 0 || right == 0)
    {
        res++;
        return 1;
    }
    if (left == 1 || right == 1)
        return 2;
    return 0;
}
int minCameraCover(TreeNode *root)
{
    int res = 0;
    int flag = minCameraCover(root, res);
    return flag == 0 ? res + 1 : res;
}
