#include <iostream>
#include <string>
#include <vector>
using namespace std;
vector<string> split(string s)
{
    vector<string> res;
    string cur_str = "";
    for (auto &c : s)
    {
        if (c != ',')
            cur_str += c;
        else
        {
            res.push_back(cur_str);
            cur_str = "";
        }
    }
    return res;
}
bool isValidSerialization(string preorder)
{
    vector<string> seq = split(preorder);
    int capacity = 1;
    for (int i = 0; i < seq.size(); i++)
    {
        capacity--;
        if (capacity < 0)
            return false;
        if (seq[i] != "#")
            capacity += 2;
    }
    return capacity == 0;
}