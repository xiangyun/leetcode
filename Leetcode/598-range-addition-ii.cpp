#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int maxCount(int m, int n, vector<vector<int>> &ops)
{
    int min_x = INT_MAX, min_y = INT_MAX;
    for (auto op : ops)
    {
        int x = op[0], y = op[1];
        min_x = min(x, min_x);
        min_y = min(y, min_y);
    }
    return min_x * min_y;
}