#include <iostream>
#include <unordered_map>
using namespace std;
bool containsDuplicate(vector<int> &nums)
{
    unordered_map<int, int> hash;
    for (auto i : nums)
    {
        if (hash[i] > 1)
            return true;
        else
            hash[i]++;
    }
    return false;
}