#include <iostream>
#include <vector>
#include <numeric>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left, *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
class Solution
{
  public:
    int helper(TreeNode *root)
    {
        if (!root)
            return 0;
        int left = helper(root->left);
        int right = helper(root->right);
        arr.push_back(abs(left - right));
        return left + root->val + right;
    }

    int findTilt(TreeNode *root)
    {
        helper(root);
        return accumulate(arr.begin(), arr.end(), 0);
    }

  private:
    vector<int> arr;
};