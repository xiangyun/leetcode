#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
using namespace std;
void backtracking(set<vector<int>> &res, vector<int> nums, vector<int> &arr, int index)
{
    for (int i = index; i <= nums.size(); i++)
    {
        res.insert(arr);
        arr.push_back(nums[i]);
        backtracking(res, nums, arr, i + 1);
        arr.pop_back();
    }
}
vector<vector<int>> subsetsWithDup(vector<int> &nums)
{
    set<vector<int>> s;
    vector<vector<int>> res;
    vector<int> arr;
    sort(nums.begin(), nums.end());
    backtracking(s, nums, arr, 0);
    for (auto it = s.begin(); it != s.end(); it++)
        res.push_back(*it);
    return res;
}