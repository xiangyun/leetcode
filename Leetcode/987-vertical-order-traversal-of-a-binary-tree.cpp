#include <iostream>
#include <vector>
#include <queue>
#include <numeric>
#include <algorithm>
#include <set>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution
{
  private:
    int min_val = 0, max_val = 0;

    void dfs(TreeNode *root, int cur)
    {
        if (!root)
            return;
        min_val = min(min_val, cur);
        max_val = max(max_val, cur);
        dfs(root->left, cur - 1);
        dfs(root->right, cur + 1);
    }

  public:
    vector<vector<int>> verticalTraversal(TreeNode *root)
    {
        queue<TreeNode *> que;
        queue<int> vec;
        vector<vector<int>> res;
        if (!root)
            return res;
        dfs(root, 0);
        res.resize(max_val + abs(min_val) + 1);
        que.push(root);
        vec.push(0);
        while (!que.empty())
        {
            TreeNode *cur = que.front();
            int id = vec.front();
            res[id + abs(min_val)].push_back(cur->val);
            if (cur->left)
            {
                que.push(cur->left);
                vec.push(id - 1);
            }
            if (cur->right)
            {
                que.push(cur->right);
                vec.push(id + 1);
            }
            que.pop();
            vec.pop();
        }
        for (int i = 0; i < res.size(); i++)
            sort(res[i].begin(), res[i].end());
        return res;
    }
};