#include <iostream>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <queue>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

// void getLevel(TreeNode *root, vector<vector<int>> &res, int depth)
// {
//     if (!root)
//         return;
//     if (res.size() < depth)
//         res.push_back(vector<int>());
//     res[depth].push_back(root->val);
//     getLevel(root->left, res, depth + 1);
//     getLevel(root->right, res, depth + 1);
// }

// void getDownNode(TreeNode *target, int K, int depth, vector<int> &res)
// {
//     if (!target)
//         return;
//     if (depth == K)
//         res.push_back(target->val);
//     getDownNode(target->left, K, depth + 1, res);
//     getDownNode(target->right, K, depth + 1, res);
// }

// void findTarget(TreeNode *root, TreeNode *target, int depth, int dist)
// {
//     if (!root)
//         return;
//     if (root->val == target->val)
//     {
//         dist = depth;
//         return;
//     }
//     findTarget(root->left, target, depth + 1, dist);
//     findTarget(root->right, target, depth + 1, dist);
// }

// vector<int> distanceK(TreeNode *root, TreeNode *target, int K)
// {
//     int dist;
//     vector<int> res;
//     getDownNode(target, K, 0, res);
//     findTarget(root, target, 0, dist);
//     if (dist >= K)
//     {
//         getDownNode(root, K, 0, res);
//     }
//     else
//     {
//     }
//     return res;
// }
// void getBelowNode(TreeNode *target, int K, int depth, vector<int> &arr)
// {
//     if (!target)
//         return;
//     if (depth == K)
//         arr.push_back(target->val);
//     getBelowNode(target->left, K,depth + 1, arr);
//     getBelowNode(target->right,K, depth + 1, arr);
// }

class Solution
{
  public:
    vector<int> distanceK(TreeNode *root, TreeNode *target, int K)
    {
        buildGraph(nullptr, root);
        unordered_set<TreeNode *> visited;
        queue<TreeNode *> que;
        visited.insert(target);
        int k = 0;
        que.push(target);
        vector<int> ans;
        while (!que.empty() && k <= K)
        {
            int s = que.size();
            while (s)
            {
                TreeNode *node = que.front();
                que.pop();
                if (k == K)
                    ans.push_back(node->val);
                for (TreeNode *child : hash[node])
                {
                    if (visited.count(child))
                        continue;
                    que.push(child);
                    visited.insert(child);
                }
                s--;
            }
            k++;
        }
        return ans;
    }

  private:
    unordered_map<TreeNode *, vector<TreeNode *>> hash;
    void buildGraph(TreeNode *parent, TreeNode *child)
    {
        if (parent)
        {
            hash[parent].push_back(child);
            hash[child].push_back(parent);
        }
        if (child->left)
            buildGraph(child, child->left);
        if (child->right)
            buildGraph(child, child->right);
    }
};