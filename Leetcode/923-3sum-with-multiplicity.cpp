#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
using namespace std;
int threeSumMulti(vector<int> &A, int target)
{
    unordered_map<int, int> hash;
    for (auto i : A)
        hash[i]++;
    using ll = long long;
    ll res = 0, mod = pow(10, 9) + 7;
    for (auto it_1 : hash)
    {
        for (auto it_2 : hash)
        {
            int i = it_1.first;
            int j = it_2.first;
            int k = target - i - j;
            if (!hash.count(k))
                continue;
            if (i == j && j == k)
                res = (res + hash[i] * (hash[i] - 1) * (hash[i] - 2) / 6) % mod;
            else if (i == j && j != k)
                res = (res + hash[i] * (hash[i] - 1) / 2 * hash[k]) % mod;
            else if (i < j && j < k)
                res = (res + hash[i] * hash[j] * hash[k]) % mod;
        }
    }
    return res;
}