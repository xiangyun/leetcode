#include <iostream>
#include <vector>
#include <set>
#include <unordered_map>
using namespace std;

void backtracking(vector<int> &arr, vector<vector<int>> &res, int cur_index, vector<int> cur_arr)
{
    if (cur_arr.size() > 1)
        res.push_back(cur_arr);
    unordered_map<int, int> hash;
    for (int i = cur_index; i < arr.size(); i++)
    {
        if (hash.find(arr[i]) != hash.end())
            continue;
        if (cur_arr.empty() || cur_arr.back() <= arr[i])
        {
            cur_arr.push_back(arr[i]);
            backtracking(arr, res, i + 1, cur_arr);
            cur_arr.pop_back();
        }
        hash[arr[i]]++;
    }
}

vector<vector<int>> findSubsequences(vector<int> &nums)
{
    vector<vector<int>> res;
    backtracking(nums, res, 0, vector<int>());
    return res;
}