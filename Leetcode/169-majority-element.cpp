#include <iostream>
#include <vector>
using namespace std;
int majorityElement(vector<int> &nums)
{
    int majority = INT_MIN, count = 0;
    for (auto &n : nums)
    {
        if (majority == n)
            count++;
        else if (count == 0)
        {
            majority = n;
            count = 1;
        }
        else
            count--;
    }
    return majority;
}