#include <iostream>
using namespace std;
struct ListNode{
    int val;
    ListNode *next;
    ListNode (int x):val(x),next(NULL){}
};

ListNode * mergeTwoSortedLists(ListNode * l1, ListNode * l2)
{
    if(l1==NULL)
        return l2;
    if(l2==NULL)
        return l1;
    ListNode *cur1 = l1;
    ListNode *cur2 = l2;
    ListNode *head;
    if (l1->val<l2->val)
    {
        head = l1;
        l1 = l1->next;
    }
    else{
        head = l2;
        l2 = l2->next;
    }
    ListNode *node = head;
    while (cur1 != NULL && cur2 != NULL)
    {
        if (cur1->val < cur2->val)
        {
            node->next = cur1;
            node = cur1;
            cur1 = cur1->next;
        }
        else
        {
            node->next = cur2;
            node = cur2;
            cur2 = cur2->next;
        }
    }
    while(cur1!=NULL)
    {
        node->next = cur1;
        node = cur1;
        cur1 = cur1->next;
    }
    while(cur2!=NULL)
    {
        node->next = cur2;
        node = cur2;
        cur2 = cur2->next;
    }
    return head;
}