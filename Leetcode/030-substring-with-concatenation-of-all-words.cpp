#include <iostream>
#include <vector>
#include <map>
using namespace std;
/*
Hey LeetCode what's wrong with you?
*/
bool isValid(string s,map<string,int> m,int cnt);
vector<int> findSubstring(string s, vector<string>& words)
{
    int N = words.size();
    int cnt = words.front().size();
    int length = N * cnt;
    map<string, int> m;
    vector<int> res;
    for (int i = 0; i < N;i++)
    {
        auto it = m.find(words[i]);
        if(it==m.end())
            m.insert(make_pair(words[i], 1));
        else
            it->second++;
    }
    for (int i = 0; i < s.length()-length; i++)
    {
        if(isValid(s.substr(i,length),m,cnt))
            {
                res.push_back(i);
                res.push_back(i + length);
            }
    }
    return res;
}
bool isValid(string s,map<string,int> m,int cnt)
{
    map<string, int> temp = m;
    for (int i = 0; i < s.length();i+=cnt)
    {
        string sbustr = s.substr(i, cnt);
        auto it = temp.find(sbustr);
        if(it!=temp.end())
        {
            if(it->second==1)
                temp.erase(it);
            else
                it->second--;
        }
        else
            return false;
    }
    return true;
}