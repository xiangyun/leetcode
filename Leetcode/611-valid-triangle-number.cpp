#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int triangleNumber(vector<int> &nums)
{
    sort(nums.begin(), nums.end());
    int k = nums.size() - 1, cnt = 0;
    while (k >= 2)
    {
        int right = k - 1;
        while (right >= 1)
        {
            int left = 0;
            while (left < right && nums[left] + nums[right] <= nums[k])
                ++left;
            if (left < right)
                cnt += right - left;
            --right;
        }
        --k;
    }
    return cnt;
}