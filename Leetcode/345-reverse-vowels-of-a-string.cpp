#include <iostream>
#include <vector>
using namespace std;
bool isVowels(char ch)
{
    return ch == 'a' || ch == 'A' ||
           ch == 'e' || ch == 'E' ||
           ch == 'i' || ch == 'I' ||
           ch == 'o' || ch == 'O' ||
           ch == 'u' || ch == 'U';
}

string reverseVowels(string s)
{
    int i = 0, j = s.size() - 1;
    while (i < j)
    {
        while (i < j && !isVowels(s[i]))
            i++;
        while (i < j && !isVowels(s[j]))
            j--;
        if (i < j)
        {
            swap(s[i], s[j]);
            i++;
            j--;
        }
    }
    return s;
}