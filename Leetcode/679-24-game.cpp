#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

bool isValid(float a, float b)
{
    float delta = 0.0001;
    if (fabs(a + b - 24.0) < delta)
        return true;
    if (fabs(a - b - 24.0) < delta)
        return true;
    if (fabs(a * b - 24.0) < delta)
        return true;
    if (b && fabs(a / b - 24.0) < delta)
        return true;
    return false;
}

bool isValid(float a, float b, float c)
{
    if (isValid(a + b, c) || isValid(a - b, c) || isValid(a * b, c) || (b && isValid(a / b, c)))
        return true;
    if (isValid(a, b + c) || isValid(a, b - c) || isValid(a, b * c) || (c && isValid(a, b / c)))
        return true;
    return false;
}

bool isValid(vector<int> &arr)
{
    float a = arr[0];
    float b = arr[1];
    float c = arr[2];
    float d = arr[3];
    if (isValid(a + b, c, d) || isValid(a - b, c, d) || isValid(a * b, c, d) || isValid(a / b, c, d))
        return true;
    if (isValid(a, b + c, d) || isValid(a, b - c, d) || isValid(a, b * c, d) || isValid(a, b / c, d))
        return true;
    if (isValid(a, b, c + d) || isValid(a, b, c - d) || isValid(a, b, c * d) || isValid(a, b, c / d))
        return true;
    return false;
}

bool judgePoint24(vector<int> &nums)
{
    std::sort(nums.begin(), nums.end());
    do
    {
        if (isValid(nums))
            return true;
    } while (std::next_permutation(nums.begin(), nums.end()));
    return false;
}