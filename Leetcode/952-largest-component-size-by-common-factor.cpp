#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
using namespace std;
class UnionFind
{
  private:
    int count;
    int *parent;
    int *rank;
    int max_rank;

  public:
    UnionFind(int n)
    {
        count = n;
        parent = new int[n];
        rank = new int[n];
        for (int i = 0; i < n; i++)
        {
            parent[i] = i;
            rank[i] = 1;
        }
        max_rank = 0;
    }
    ~UnionFind()
    {
        delete[] parent;
        delete[] rank;
    }
    int find(int x)
    {
        if (x != parent[x])
            parent[x] = find(parent[x]);
        return parent[x];
    }
    bool isConnnect(int x, int y)
    {
        return find(x) == find(y);
    }

    void Union(int x, int y)
    {
        int fx = find(x), fy = find(y);
        if (fx == fy)
            return;
        if (rank[fx > rank[fy]])
        {
            parent[fy] = fx;
            rank[fx] += rank[fy];
            max_rank = max(max_rank, rank[fx]);
        }
        else
        {
            parent[fx] = fy;
            rank[fy] += rank[fx];
            max_rank = max(max_rank, rank[fy]);
        }
        count--;
    }
    int size()
    {
        return count;
    }
    int getMaxRank()
    {
        return max_rank;
    }
};
class Solution
{
  public:
    /*
    vector<int> get_prime()
    {
        vector<bool> isPrime(100001, true);
        vector<int> prime;
        for (int i = 2; i < 100000; i++)
        {
            if (isPrime[i])
            {
                prime.emplace_back(i);
                for (int j = 2; j * i <= 100000; j++)
                {
                    isPrime[j * i] = false;
                }
            }
        }
        return prime;
    }*/

    int largestComponentSize(vector<int> &A)
    {
        //vector<int> prime = get_prime();
        int n = A.size();
        unordered_map<int, int> hash;//hash(key,value):key->factor, value->node_index
        UnionFind uf(n);
        for (int i = 0; i < n; i++)
        {
            int a = A[i];
            for (int j = 2; j * j <= a; j++)
            {
                if (a % j == 0)
                {
                    if (hash.find(j) == hash.end())
                        hash[j] = i;
                    else
                        uf.Union(i, hash[j]);
                    if (hash.find(a / j) == hash.end())
                        hash[a / j] = i;
                    else
                        uf.Union(i, hash[a / j]);
                }
            }
            if (hash.find(a) == hash.end())
                hash[a] = i;
            else
                uf.Union(i, hash[a]);
        }
        return uf.getMaxRank();
    }
};