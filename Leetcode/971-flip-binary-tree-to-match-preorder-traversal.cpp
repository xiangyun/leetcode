#include <iostream>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

// void preOrder(TreeNode *root, vector<int> &arr)
// {
//     if (!root)
//         return;
//     arr.push_back(root->val);
//     preOrder(root->left, arr);
//     preOrder(root->right, arr);
// }

// void backtracking(TreeNode *root, vector<int> &voyage, vector<int> &res, int index)
// {
//     if (!root)
//         return;
//     if (root->left)
//     {
//         if (root->left->val == voyage[index + 1])
//             backtracking(root->left, voyage, res, index + 1);
//         else
//         {

//             TreeNode *l = root->left, *r = root->right;
//             root->left = r;
//             root->right = l;
//             if (r && r->val == voyage[index + 1])
//             {
//                 res.push_back(root->val);
//                 backtracking(root->left, voyage, res, index + 1);
//             }
//             else
//             {
//                 res = {-1};
//                 return;
//             }
//         }
//     }
//     else
//     {
//         TreeNode *r = root->right;
//         if (r && r->val == voyage[index + 1])
//             backtracking(root->right, voyage, res, index + 1);
//         if (!r)
//         {
//             return;
//         }
//     }
// }

// vector<int> flipMatchVoyage(TreeNode *root, vector<int> &voyage)
// {
//     if (!root)
//     {
//         if (voyage.empty())
//             return vector<int>();
//         else
//             return vector<int>({-1});
//     }
//     vector<int> res, arr;

//     preOrder(root, arr);
//     if (arr.size() != voyage.size())
//         return {-1};
//     if (root->val == voyage[0])
//         backtracking(root, voyage, res, 0);
//     return res;
// }
//mark
vector<int> res;
int i = 0;

bool dfs(TreeNode *root, vector<int> &arr)
{
    if (!root)
        return true;
    if (root->val != arr[i++])
        return false;
    if (root->left && root->left->val != arr[i])
    {
        res.push_back(root->val);
        return dfs(root->right, arr) && dfs(root->left, arr);
    }
    return dfs(root->left, arr) && dfs(root->right, arr);
}

vector<int> flipMatchVoyage(TreeNode *root, vector<int> &voyage)
{
    return dfs(root, voyage) ? res : vector<int>{-1};
}