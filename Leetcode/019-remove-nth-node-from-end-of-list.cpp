#include <iostream>
using namespace std;
struct ListNode {
	int val;
	ListNode *next;
	ListNode(int x) : val(x), next(NULL) {}
};

ListNode *removeNthFromEnd(ListNode *head, int n)
{
	ListNode *pre = head, *node_n = head, *node = head;
	if (head == NULL)
		return NULL;
	if (head->next == NULL)
	{
		ListNode * Node = head;
		head = NULL;
		delete Node;
		return head;
	}
	for (int i = 0; i < n; i++)
		node_n = node_n->next;
	while (node_n != NULL)
	{
		node = node->next;
		if (node_n->next != NULL)
			pre = pre->next;
		node_n = node_n->next;

	}
	if (pre == node)
	{
		ListNode * Node = head;
		head = head->next;
		delete Node;
		return head;
	}

	ListNode *next = node->next;
	pre->next = next;
	node->next = NULL;
	delete node;
	return head;
}