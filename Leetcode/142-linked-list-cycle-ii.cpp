#include <iostream>
using namespace std;
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

ListNode *detectCycle(ListNode *head)
{
    if (!head || !head->next)
        return NULL;
    ListNode *fast = head, *slow = head;
    ListNode *node = head;
    while (fast && fast->next)
    {
        fast = fast->next->next;
        slow = slow->next;
        if (fast == slow)
        {
            while (node != slow)
            {
                node = node->next;
                slow = slow->next;
            }
            return node;
        }
    }
    return NULL;
}