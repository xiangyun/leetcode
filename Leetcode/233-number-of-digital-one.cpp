#include <iostream>
#include <string>
using namespace std;
int countDigitalOne(int n)
{
    if (n <= 0)
        return 0;
    if (n <= 9)
        return 1;
    int len = std::to_string(n).size();
    int num = std::pow(10, len - 1);
    if (n >= 2 * num)
        return num + countDigitalOne(n % num) + n / num * countDigitalOne(num - 1);
    else
        return n % num + 1 + countDigitalOne(n % num) + countDigitalOne(num - 1);
}