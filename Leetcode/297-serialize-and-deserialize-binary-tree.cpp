#include <iostream>
#include <string>
#include <queue>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
class Codec
{
  public:
    Codec() {}
    ~Codec() {}
    string serialize(TreeNode *root)
    {
        string s = "";
        if (!root)
            return "[null]";
        queue<TreeNode *> que;
        que.push(root);
        while(!que.empty())
        {
            TreeNode * cur_node = que.front();
            que.pop();
            if(cur_node)
            {
                
            }
            else
            {

            }
        }
    }
    TreeNode *deserialize(string data)
    {
        vector<string> res = split(data);
        return deserialize(res, 0);
    }
    vector<string> split(string s)
    {
        string::iterator it = s.begin() + 1;
        vector<string> res;
        string str = "";
        while (it != s.end())
        {
            while (it != s.end() - 1 && *it != ',')
            {
                str += *it;
                it++;
            }
            it++;
            res.push_back(str);
            str = "";
        }
        return res;
    }

  private:
    TreeNode *deserialize(vector<string> &Q, int i)
    {
        string str = Q[i];
        if (str == "null")
            return NULL;

        TreeNode *root = new TreeNode(stoi(str));
        if (!Q.empty() && (2 * i + 1) < Q.size())
            root->left = deserialize(Q, 2 * i + 1);
        if (!Q.empty() && (2 * i + 2) < Q.size())
            root->right = deserialize(Q, 2 * i + 2);
        return root;
    }
};

void destory(TreeNode *_root)
{
    TreeNode *temp = _root;
    //cout << "hello" << endl;
    if (!temp)
        return;
    destory(temp->left);
    destory(temp->right);
    delete temp;
    temp = NULL;
}

int main()
{
    Codec t;
    string s = "[1,2,3,null,null,4,5]";
    TreeNode *res = t.deserialize(s);
    string p = t.serialize(res);
    cout << p << endl;
    destory(res);
    return 0;
}