#include <iostream>
#include <vector>
using namespace std;

class Solution
{
  public:
    void dfs(int cur, int n, vector<int> &arr)
    {
        if (cur > n)
            return;
        arr.push_back(cur);
        for (int i = 0; i < 10; i++)
        {
            if (10 * cur + i > n)
                return;

            dfs(10 * cur + i, n, arr);
        }
    }

    vector<int> lexicalOrder(int n)
    {
        vector<int> res;
        for (int i = 1; i <= 9; i++)
            dfs(i, n, res);
        return res;
    }
};
