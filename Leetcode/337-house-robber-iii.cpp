#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution
{
  public:
    int rob(TreeNode *root)
    {
        unordered_map<TreeNode *, int> hash;
        return rob(root, hash);
    }
    int rob(TreeNode *root, unordered_map<TreeNode *, int> &hash)
    {
        if (!root)
            return 0;
        if (hash.count(root))
            return hash[root];
        int ll = root->left ? rob(root->left->left, hash) : 0;
        int lr = root->left ? rob(root->left->right, hash) : 0;
        int rl = root->right ? rob(root->right->left, hash) : 0;
        int rr = root->right ? rob(root->right->right, hash) : 0;
        hash[root] = max(root->val + ll + lr + rl + rr, rob(root->left, hash) + rob(root->right, hash));
        return hash[root];
    }
};