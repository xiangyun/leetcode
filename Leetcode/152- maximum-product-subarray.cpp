#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int maxProduct(vector<int> &nums)
{
    if (nums.empty())
        return 0;
    long long cur_max = nums[0], cur_min = nums[0], res = nums[0];
    for (int i = 1; i < nums.size(); i++)
    {
        long long temp = cur_max;
        cur_max = max((long long)nums[i], max(temp * nums[i], cur_min * nums[i]));
        cur_min = min((long long)nums[i], min(temp * nums[i], cur_min * nums[i]));
        res = max(res, cur_max);
    }
    return res;
}