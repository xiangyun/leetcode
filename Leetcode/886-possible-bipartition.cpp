#include <iostream>
#include <vector>
using namespace std;
// class UnionFind
// {
//   private:
//     int count;
//     int *parent;
//     int *rank;

//   public:
//     UnionFind(int n)
//     {
//         count = n;
//         parent = new int[n];
//         rank = new int[n];
//         for (int i = 0; i < n; i++)
//         {
//             parent[i] = i;
//             rank[i] = 1;
//         }
//     }
//     ~UnionFind()
//     {
//         delete[] parent;
//         delete[] rank;
//     }
//     int find(int x)
//     {
//         if (x != parent[x])
//             parent[x] = find(parent[x]);
//         return parent[x];
//     }
//     void Union(int x, int y)
//     {
//         int fx = find(x), fy = find(y);
//         if (fx == fy)
//             return;
//         if (rank[fx] > rank[fy])
//         {
//             parent[fy] = fx;
//             rank[fx] += rank[fy];
//         }
//         else
//         {
//             parent[fx] = fy;
//             rank[fy] += rank[fx];
//         }
//         count--;
//     }
//     bool isConnect(int x, int y)
//     {
//         return find(x) == find(y);
//     }
//     int size()
//     {
//         return count;
//     }
// };
// bool possibleBipartition(int N, vector<vector<int>> &dislikes)
// {
//     vector<vector<bool>> adj(N, vector<bool>(N, true));
//     UnionFind uf(N);
//     for (int i = 0; i < dislikes.size(); i++)
//     {
//         adj[dislikes[i][0]][dislikes[i][1]] = adj[dislikes[i][1]][dislikes[i][0]] = false;
//     }
//     for (int i = 0; i < N; i++)
//     {
//         for (int j = i + 1; j < N; j++)
//         {
//             if (adj[i][j])
//                 uf.Union(i, j);
//         }
//     }
//     return uf.size() > 1;
// }
bool isValid(vector<vector<int>> &graph, vector<int> &colors, int index, int cc)
{
    if (colors[index] != 0)
        return colors[index] == cc;
    colors[index] = cc;
    for (auto &i : graph[index])
    {
        if (!isValid(graph, colors, i, -cc))
            return false;
    }
    return true;
}

bool possibleBipartition(int N, vector<vector<int>> &dislikes)
{
    vector<int> colors(N, 0);
    vector<vector<int>> graph(N);
    for (int i = 0; i < dislikes.size(); i++)
    {
        graph[dislikes[i][0] - 1].push_back(dislikes[i][1] - 1);
        graph[dislikes[i][1] - 1].push_back(dislikes[i][0] - 1);
    }
    for (int i = 0; i < N; i++)
    {
        if (colors[i] == 0 && !isValid(graph, colors, i, 1))
            return false;
    }

    return true;
}