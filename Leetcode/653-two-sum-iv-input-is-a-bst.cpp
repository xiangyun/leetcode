#include <vector>
#include <iostream>
#include <unordered_map>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

bool target(TreeNode *root, int k, unordered_map<int, int> &hash)
{
    if (!root)
        return false;
    if (hash[root->val] > 0)
        return true;
    hash[k - root->val] = 1;
    return target(root->left, k, hash) || target(root->right, k, hash);
}

bool findTarget(TreeNode *root, int k)
{
    unordered_map<int, int> hash;
    return target(root,k,hash);
}