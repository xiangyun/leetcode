#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
class Solution
{
  public:
    int nthSuperUglyNumber(int n, vector<int> &nums)
    {
        int k = nums.size();
        vector<int> indices(k, 0);
        vector<int> res(n, INT_MAX);
        res[0] = 1;
        for (int i = 1; i < n; i++)
        {
            int index;
            for (int j = 0; j < k; j++)
            {
                int temp = nums[j] * res[indices[j]];
                if (res[i] > temp)
                {
                    res[i] = temp;
                    index = j;
                }
                else if (res[i] == temp)
                {
                    indices[j]++;
                }
            }
            indices[index]++;
        }
        return res[n - 1];
    }
};