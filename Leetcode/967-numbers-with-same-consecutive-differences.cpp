#include <iostream>
#include <vector>
#include <set>
using namespace std;

void backtracking(set<int> &arr, int N, int K, int cur_index, int val)
{
    if (cur_index == N)
    {
        arr.insert(val);
        return;
    }
    int res = val % 10;
    for (int i = 0; i <= 9; i++)
    {
        if (i - res == K)
            backtracking(arr, N, K, cur_index + 1, val * 10 + i);
        if (i + res == K)
            backtracking(arr, N, K, cur_index + 1, val * 10 + i);
    }
}

vector<int> numsSameConsecDiff(int N, int K)
{
    set<int> arr;
    for (int i = 1; i <= 9; i++)
    {
        backtracking(arr, N, K, 1, i);
    }
    if (N == 1)
        arr.insert(0);
    return vector<int>(arr.begin(), arr.end());
}