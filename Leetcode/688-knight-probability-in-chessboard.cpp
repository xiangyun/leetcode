#include <iostream>
#include <vector>
#include <array>
using namespace std;
/*
bool isValid(int N, int r, int c)
{
    return r >= 0 && c >= 0 && r < N && c < N;
}

int backtracking(int N, int K, int cur_r, int cur_c, int cur_k)
{
    if (!isValid(N, cur_r, cur_c))
        return 0;
    if (cur_k == K)
    {
        return 1;
    }
    int sum =
        backtracking(N, K, cur_r + 1, cur_c + 2, cur_k + 1) +
        backtracking(N, K, cur_r + 1, cur_c - 2, cur_k + 1) +
        backtracking(N, K, cur_r - 1, cur_c + 2, cur_k + 1) +
        backtracking(N, K, cur_r - 1, cur_c - 2, cur_k + 1) +
        backtracking(N, K, cur_r + 2, cur_c + 1, cur_k + 1) +
        backtracking(N, K, cur_r + 2, cur_c - 1, cur_k + 1) +
        backtracking(N, K, cur_r - 2, cur_c + 1, cur_k + 1) +
        backtracking(N, K, cur_r - 2, cur_c - 1, cur_k + 1);
    return sum;
}

double knightProbability(int N, int K, int r, int c)
{
    return backtracking(N, K, r, c, 0)/pow(8,K);
}
*/
bool isValid(int N, int r, int c)
{
    return r >= 0 && c >= 0 && r < N && c < N;
}
double knightProbability(int N, int K, int r, int c)
{
    vector<vector<vector<double>>> dp(K + 1, vector<vector<double>>(N, vector<double>(N, 0)));
    vector<int> di = {1, 1, 2, 2, -1, -1, -2, -2};
    vector<int> dj = {2, -2, 1, -1, 2, -2, 1, -1};
    dp[0][r][c] = 1;
    for (int t = 1; t <= K; t++)
    {
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                for (int l = 0; l < 8; l++)
                {
                    int x = i + di[l];
                    int y = j + dj[l];
                    if (isValid(N, x, y))
                    {
                        dp[t][i][j] += dp[t - 1][x][y] * 0.125;
                    }
                }
            }
        }
    }
    double prob = 0;
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            prob += dp[K][i][j];
        }
    }
    return prob;
}