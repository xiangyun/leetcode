#include <iostream>
#include <vector>
using namespace std;

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void inOrder(TreeNode* root, vector<int> & arr)
{
    if(root==NULL)
    return;
    inOrder(root->left,arr);
    arr.push_back(root->val);
    inOrder(root->right,arr);
}


vector<int> inorderTraversal(TreeNode *root)
{
    vector<int> arr;
    inOrder(root,arr);
    return arr;
}