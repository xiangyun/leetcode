#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
  private:
    bool isValid(int i, int j, vector<vector<int>> &arr)
    {
        return i >= 0 && j >= 0 && i < arr.size() && j < arr[0].size();
    }

  public:
    vector<vector<int>> updateMatrix(vector<vector<int>> &matrix)
    {
        for (int i = 0; i < matrix.size(); i++)
        {
            for (int j = 0; j < matrix[0].size(); j++)
            {
                if (matrix[i][j] == 0)
                    continue;
                matrix[i][j] = 10000;
                if (i > 0)
                    matrix[i][j] = min(matrix[i - 1][j] + 1, matrix[i][j]);
                if (j > 0)
                    matrix[i][j] = min(matrix[i][j - 1] + 1, matrix[i][j]);
            }
        }
        for (int i = matrix.size() - 1; i >= 0; i--)
        {
            for (int j = matrix[0].size() - 1; j >= 0; j--)
            {
                if (matrix[i][j] == 0)
                    continue;
                if (i < matrix.size() - 1)
                    matrix[i][j] = min(matrix[i + 1][j] + 1, matrix[i][j]);
                if (j < matrix[0].size() - 1)
                    matrix[i][j] = min(matrix[i][j + 1] + 1, matrix[i][j]);
            }
        }
        return matrix;
    }
};
