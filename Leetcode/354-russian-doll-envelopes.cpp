#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int maxEnvelopes(vector<pair<int, int>> &envelope)
{
    if (envelope.empty())
        return 0;
    sort(envelope.begin(), envelope.end());
    vector<int> dp(envelope.size(), 1);
    for (int i = 0; i < envelope.size(); i++)
    {
        for (int j = 0; j < i; j++)
        {
            if (envelope[j].first < envelope[i].first && envelope[j].second < envelope[i].second)
                dp[i] = max(dp[j] + 1, dp[i]);
        }
    }
    return dp[envelope.size()-1];
}