#include <iostream>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
/*
void inOrder(TreeNode *root, int &num)
{
    if (!root)
        return;
    num++;
    inOrder(root->left, num);
    inOrder(root->right, num);
}

int countNodes(TreeNode *root)
{
    int num = 0;
    inOrder(root, num);
    return num;
}
*/

int depth(TreeNode *root)
{
    if (!root)
        return 0;
    return 1 + depth(root->left);
}
int countNodes(TreeNode *root)
{
    if (!root)
        return 0;
    int ld = depth(root->left);
    int rd = depth(root->right);
    if (ld == rd)
        return (1 << ld) + countNodes(root->right);
    else
        return (1 << rd) + countNodes(root->left);
}