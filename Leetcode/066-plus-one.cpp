#include <iostream>
#include <vector>
#include <string>
using namespace std;
vector<int> plusOne(vector<int> &digits)
{
    int val = 0;
    vector<int> result;
    for (int i = 0; i < digits.size(); i++)
        val = val * 10 + digits[i];
    val++;
    while (val != 0)
    {
        int temp = val % 10;
        val = val / 10;
    }
    return result;
}