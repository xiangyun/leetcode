#include <iostream>
using namespace std;
bool isPowerOfFour(int num)
{
    if (num == 0)
        return false;
    if (num == 1)
        return true;
    if (num % 4 == 0)
    {
        return isPowerOfFour(num >> 1 >> 1);
    }
    else
        return false;
}