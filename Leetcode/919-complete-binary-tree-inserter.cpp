#include <iostream>
#include <vector>
#include <queue>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left, *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
class CBTInserter
{
  public:
    CBTInserter(TreeNode *root)
    {
        this->root = root;
        que.push(root);
        while (que.front()->left && que.front()->right)
        {
            TreeNode *cur_node = que.front();
            que.pop();
            que.push(cur_node->left);
            que.push(cur_node->right);
        }
    }

    int insert(int v)
    {
        TreeNode *cur_node = que.front();
        if (!cur_node->left)
            cur_node->left = new TreeNode(v);
        else
        {
            cur_node->right = new TreeNode(v);
            que.pop();
            que.push(cur_node->left);
            que.push(cur_node->right);
        }
        return cur_node->val;
    }

    TreeNode *get_root()
    {
        return root;
    }

  private:
    TreeNode *root;
    queue<TreeNode *> que;
};

/**
 * Your CBTInserter object will be instantiated and called as such:
 * CBTInserter* obj = new CBTInserter(root);
 * int param_1 = obj->insert(v);
 * TreeNode* param_2 = obj->get_root();
 */