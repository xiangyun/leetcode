#include <iostream>
//using namespace std;
class solution
{
  public:
    double pow(double x, int n)
    {
        if (n == 0)
            return 1;
        if (n < 0)
            return 1.0 / x * pow(1.0 / x, -(n + 1));
        if (n % 2 == 0)
            return pow(x * x, n / 2);
        return pow(x, n - 1) * x;
    }
};