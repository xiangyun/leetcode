#include <iostream>
#include <vector>
using namespace std;
int countTriplets(vector<int> &A)
{
    int sum = 0;
    for (int i = 0; i < A.size(); i++)
    {
        for (int j = 0; j < A.size(); j++)
        {
            for (int k = 0; k < A.size(); k++)
            {
                if (A[i] & A[j] & A[k] == 0)
                    ++sum;
            }
        }
    }
    return sum;
}