#include <iostream>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
void sum_of_left_leaves(TreeNode *root, int &sum)
{
    if (!root)
        return;
    if (root->left)
    {
        if (!root->left->left && !root->left->right)
            sum += root->left->val;
        else
        {
            sum_of_left_leaves(root->left, sum);
        }
    }
    sum_of_left_leaves(root->right, sum);
}

int sumOfLeftLeaves(TreeNode *root)
{
    int res = 0;
    sum_of_left_leaves(root, res);
    return res;
}
