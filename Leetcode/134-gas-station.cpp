#include <iostream>
#include <vector>
using namespace std;

int canCompleteCircuit(vector<int> &gas, vector<int> &cost)
{
    vector<int> pos;
    int n = gas.size();
    for (int i = 0; i < n; i++)
    {
        int cur_sum = gas[i];
        int j = 0;
        for (; j < n; j++)
        {
            if (cur_sum < cost[(i + j) % n])
                break;
            cur_sum = cur_sum - cost[(i + j) % n] + gas[(i + j + 1) % n];
        }
        if (j == n)
            return i;
    }
    return -1;
}
