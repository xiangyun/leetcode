/*
zigzag
*/
#include <iostream>
#include <string>
using namespace std;

string convert(string text, int nRows)
{
	if(nRows==1)
		return text;
	int len = text.size();
	string s = "";
	int count = 0;
	for (int i = 0; i < nRows; i++)
	{
		for (int j = 0; j < len; j++)
		{

			if (j % (2 * (nRows - 1)) == count)
			{
				s += text[j];
			}
			int res = 2 * (nRows - 1) - count;
			if (res != 0 && res != (2 * (nRows - 1)) && res != (nRows - 1))
			{
				if (j % (2 * (nRows - 1)) == res)
					s += text[j];
			}
			//cout << s<<endl;
		}
		count++;
	}
	return s;
}


int main()
{
	string text;
	int nRows;
	cin >> text >> nRows;
	cout << convert(text, nRows) << endl;
	return 0;
}