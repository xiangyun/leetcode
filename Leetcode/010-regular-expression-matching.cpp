#include <iostream>
#include <vector>
using namespace std;

//isMatch("ab",".*")->true;

bool isMatch(const char * s, const char *p)
{
	int len1 = strlen(s), len2 = strlen(p);
	vector<vector<bool>> dp(len1 + 1, vector<bool>(len2 + 1, false));
	dp[0][0] = true;
	for (int j = 1; j <= len2; j++)
	{
		if (p[j] == '*')
			dp[0][j] = dp[0][j - 1];//init()
	}

	for (int i = 1; i <= len1; i++)
	{
		for (int j = 1; j <= len2; j++)
		{
			if (s[i] == p[j] || p[j] == '.')
			{
				dp[i][j] = dp[i - 1][j - 1];
			}
			else
			{
                if(s[i]=='*')
                {
                    if(s[i-1]==p[j])
                        dp[i][j] = dp[i - 1][j];
                    else
                        dp[i][j] = dp[i - 2][j];
                }
                else if(s[i]=='.')
                    dp[i][j] = dp[i - 1][j - 1];
                else{
                    if(p[j]=='*')
                    {
                        if(p[j-1]==s[i])
                            dp[i][j] = dp[i - 1][j - 2];
                        else
                            dp[i][j] = dp[i][j - 2];
                    }
                }
            }
		}
	}
    return dp[len1][len2];
}