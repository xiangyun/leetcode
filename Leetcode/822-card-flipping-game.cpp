#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>
using namespace std;

// bool check(vector<int> &arr, int cur)
// {
//     unordered_map<int, int> hash(arr.begin(), arr.end());
//     if (hash[cur])
//         return false;
//     return true;
// }

int flipgame(vector<int> &fronts, vector<int> &backs)
{
    unordered_map<int, int> hash;
    int res = INT_MAX;
    for (int i = 0; i < fronts.size(); i++)
        if (fronts[i] == backs[i])
            hash[fronts[i]]++;
    for (auto &i : fronts)
        if (hash.find(i) == hash.end())
            res = min(res, i);
    for (auto &i : backs)
        if (hash.find(i) == hash.end())
            res = min(res, i);
    return res == INT_MAX ? 0 : res;
}