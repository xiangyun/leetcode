#include <iostream>
#include <vector>
using namespace std;
/*
int uniquePaths(int m, int n)
{
    if (m < 0 || n < 0)
        return 0;
    if ((m == 0 && n == 1) || (m == 1 && n == 0))
        return 1;
    return uniquePaths(m - 1, n) + uniquePaths(m, n - 1);
}
TLE
*/
int uniquePaths(int m,int n)
{
    vector<vector<int>> dp(m+1,vector<int>(n+1,0));
    dp[0][1]=1;
    for(int i=1;i<=m;i++)
    {
        for(int j=1;j<=n;j++)
        {
            dp[i][j]=dp[i][j-1]+dp[i-1][j];
        }
    }
    return dp[m][n];
}