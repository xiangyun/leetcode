#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>
using namespace std;
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};
int numComponents(ListNode *head, vector<int> &G)
{
    unordered_map<int, int> hash;
    for (auto &i : G)
        hash[i]++;
    ListNode *p = head;
    int last = -1, len = 0;
    while (p)
    {
        if (last == -1)
        {
            if (hash[p->val])
                last = p->val;
        }
        else if (hash[last] && hash[p->val])
        {
            last = p->val;
        }
        else
        {
            ++len;
            last = -1;
        }
        p = p->next;
    }
    return len + (last == -1 ? 0 : 1);
}