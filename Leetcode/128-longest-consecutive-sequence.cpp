#include <iostream>
#include <set>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include <unordered_set>
using namespace std;
int longestConsecutive(vector<int> &num)
{
    unordered_map<int,int> record;
    for(auto & i:num)
    record[i]++;
    int res = 0;
    for (int n : num)
    {
        if (record.find(n) == record.end())
            continue;
        record.erase(n);
        int prev = n - 1, next = n + 1;
        while (record.find(prev) != record.end())
            record.erase(prev--);
        while (record.find(next) != record.end())
            record.erase(next++);
        res = max(res, next - prev - 1);
    }
    return res;
}