#include <iostream>
#include <unordered_map>
using namespace std;
int calc(int n)
{
    int result = 0;
    while (n != 0)
    {
        int res = n % 10;
        n /= 10;
        result += res * res;
    }
    return result;
}

bool isHappy(int n)
{
    unordered_map<int, int> map;
    while (n != 1 && map[n] <= 1)
    {
        n = calc(n);
        map[n]++;
    }
    if (n == 1)
        return true;
    else
        return false;
}