#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
// int nthUglyNumber(int n)
// {
//     if (n <= 0)
//         return 0;
//     vector<int> dp(n);
//     dp[0] = 1;
//     int index_2 = 0, index_3 = 0, index_5 = 0;
//     for (int i = 1; i < n; i++)
//     {
//         dp[i] = min(dp[index_2] * 2, min(dp[index_3] * 3, dp[index_5] * 5));
//         while (dp[index_2] * 2 <= dp[i])
//             index_2++;
//         while (dp[index_3] * 3 <= dp[i])
//             index_3++;
//         while (dp[index_5] * 5 <= dp[i])
//             index_5++;
//     }
//     return dp[n - 1];
// }
int nthUglyNumber(int n)
{
    vector<int> dp(n + 1);
    dp[0] = 0;
    dp[1] = 1;
    int x = 1, y = 1, z = 1;
    for (int i = 2; i <= n; i++)
    {
        dp[i] = min(2 * dp[x], min(3 * dp[y], 5 * dp[z]));
        if (dp[i] == dp[x] * 2)
            x++;
        if (dp[i] == dp[y] * 3)
            y++;
        if (dp[i] == dp[z] * 5)
            z++;
    }
    return dp[n];
}