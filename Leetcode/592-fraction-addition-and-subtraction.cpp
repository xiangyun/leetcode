#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

int gcd(int x, int y)
{
    if (y == 0)
        return x;
    return gcd(y, x % y);
}

pair<int, int> add(int a, int b, int c, int d) //a/b+c/d
{
    if (a == 0 && b == 0)
    {
        return {c, d};
    }
    int x = a * d + b * c;
    int y = b * d;
    return {x, y};
}

string fractionAddition(string e)
{
    if (e.front() != '-')
        e.insert(e.begin(), '+');
    string temp = "";
    pair<int, int> res = {0, 0};
    int fac = 1, c = 0, d = 0;
    bool flag = true;
    for (int i = 0; i < e.size(); i++)
    {
        if (e[i] == '-')
        {
            res = add(res.first, res.second, fac * c, d);
            fac = -1;
            c = 0;
            d = 0;
            flag = true;
        }
        else if (e[i] == '+')
        {
            res = add(res.first, res.second, fac * c, d);
            fac = 1;
            c = 0;
            d = 0;
            flag = true;
        }
        else if (e[i] == '/')
        {
            flag = false;
        }
        else
        {
            if (flag)
                c = 10 * c + e[i] - '0';
            else
                d = 10 * d + e[i] - '0';
        }
    }
    res = add(res.first, res.second, fac * c, d);
    int factor = gcd(abs(res.first), res.second);
    return to_string(res.first / factor) + '/' + to_string(res.second / factor);
}