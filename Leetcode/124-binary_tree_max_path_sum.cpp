#include <iostream>
#include <algorithm>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};
int sum = INT_MIN;
int maxPathSum(TreeNode *root)
{
    if (!root)
        return 0;
    if (!root->left && !root->right)
        return root->val;
    int left = max(0, maxPathSum(root->left));
    int right = max(0, maxPathSum(root->right));
    sum = max(sum, left + right + root->val);
    return max(left, right) + root->val;
}

/*
int maxPathSum(TreeNode *root, int &max_val)
{
    if (!root)
        return 0;
    int left = maxPathSum(root->left, max_val), right = maxPathSum(root->right, max_val);
    int t = max(root->val + left, max(root->val + right, root->val));
    max_val = max(max_val, max(left + right + root->val, t));
    return t;
}

int maxPathSum(TreeNode *root)
{
    int max_val = INT_MIN;
    maxPathSum(root, max_val);
    return max_val == INT_MIN ? 0 : max_val;
}
*/