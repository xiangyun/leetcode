#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;
bool checkSubarraySum(vector<int> &nums, int k)
{
    unordered_map<int, int> hash;
    hash[0] = -1;
    int cursum = 0;
    for (int i = 0; i < nums.size(); i++)
    {
        cursum = (cursum + nums[i]) % k;
        if (hash.count(cursum))
        {
            if (i - hash[cursum] > 1)
                return true;
        }
        else
            hash[cursum]=i;
    }
    return false;
}