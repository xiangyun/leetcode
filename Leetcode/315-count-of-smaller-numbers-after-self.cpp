#include <iostream>
#include <vector>
#include <algorithm>
#include <assert.h>
using namespace std;
// struct SegmentTreeNode
// {
//     int start, end;
//     SegmentTreeNode *left, *right;
//     int sum;
//     SegmentTreeNode()
//     {
//         start = end = -1;
//         sum = 0;
//         left = right = NULL;
//     }
//     SegmentTreeNode(int _start, int _end) : start(_start), end(_end), sum(0), left(NULL), right(NULL) {}
// };
// class SegmentTree
// {

//   public:
//     SegmentTree(vector<int> &arr)
//     {
//         buildTree(arr, 0, arr.size() - 1);
//         size = arr.size();
//     }
//     ~SegmentTree()
//     {
//         destroy(root);
//     }
//     int query(int start, int end)
//     {
//         assert(start <= end);
//         return query(start, end, root);
//     }
//     void update(int index, int val)
//     {
//         assert(index >= 0 && index < size);
//         modifyTreeVal(index, val, root);
//     }

//   private:
//     SegmentTreeNode *buildTree(vector<int> &arr, int start, int end)
//     {
//         if (start > end)
//             return NULL;
//         SegmentTreeNode *_root = new SegmentTreeNode(start, end);
//         if (start == end)
//         {
//             _root->sum = arr[start];
//             return _root;
//         }
//         int mid = (end - start) / 2 + start;
//         _root->left = buildTree(arr, start, mid);
//         _root->right = buildTree(arr, mid + 1, end);
//         _root->sum = _root->left->sum + _root->right->sum;
//         return _root;
//     }
//     int query(int start, int end, SegmentTreeNode *&_root)
//     {
//         if (_root == NULL)
//             return 0;
//         if (_root->start == start && _root->end == end)
//             return _root->sum;
//         int mid = (_root->start + _root->end) / 2;
//         if (mid > end)
//             return query(start, end, _root->left);
//         if (mid <= start)
//             return query(start, end, _root->right);
//         return query(start, mid, _root->left) + query(mid + 1, end, _root->right);
//     }
//     int modifyTreeVal(int index, int val, SegmentTreeNode *&_root)
//     {
//         if (!_root)
//             return 0;
//         int diff;
//         if (index == _root->start && index == _root->end)
//         {
//             diff = val - _root->sum;
//             _root->sum = val;
//             return diff;
//         }
//         int mid = (_root->start + _root->end) / 2;
//         if (mid > index)
//             diff = modifyTreeVal(index, val, _root->left);
//         else
//             diff = modifyTreeVal(index, val, _root->right);
//         _root->sum += diff;
//         return diff;
//     }
//     void destroy(SegmentTreeNode *&_root)
//     {
//         if (!_root)
//             return;
//         destroy(_root->left);
//         destroy(_root->right);
//         delete _root;
//         _root = NULL;
//     }
//     SegmentTreeNode *root;
//     int size;
// };
// class Solution
// {
//   private:
//     vector<pair<int, int>> temp;

//   public:
//     vector<int> countSmaller(vector<int> &nums)
//     {
//         vector<int> res;
//         SegmentTree st(nums);
//         int s = nums.size();
//         for (int i = 0; i < nums.size(); i++)
//             temp.emplace_back(nums[i], i);
//         sort(temp.begin(), temp.end());
//         for (auto &i : temp)
//         {
//             int a = i.second;
//             if (a == nums.size() - 1)
//             {
//                 res[a] = 0;
//                 st.update(a, 1);
//             }
//             else
//             {
//                 res[a] = st.query(a + 1, nums.size() - 1);
//                 st.update(a, 1);
//             }
//         }
//         return res;
//     }
// };

// https://leetcode.com/problems/count-of-smaller-numbers-after-self/discuss/76607/C%2B%2B-O(nlogn)-Time-O(n)-Space-MergeSort-Solution-with-Detail-Explanation
class Solution
{
  public:
    vector<int> countSmaller(vector<int> &nums)
    {
        int n = nums.size();
        vector<int> res(n, 0), indexes(n, 0);
        for (int i = 0; i < n; ++i)
            indexes[i] = i;
        countSmaller(res, indexes, nums, 0, n);
        return res;
    }

    void countSmaller(vector<int> &res, vector<int> &indexes, vector<int> &nums, int first, int last)
    {
        int len = last - first;
        if (len <= 1)
            return;

        int mid = first + len / 2;
        countSmaller(res, indexes, nums, first, mid);
        countSmaller(res, indexes, nums, mid, last);

        vector<int> updated;
        updated.reserve(len);
        int p1 = first, p2 = mid, cnt = 0;
        while (p1 < mid || p2 < last)
        {
            if (p2 == last || (p1 < mid && nums[indexes[p1]] <= nums[indexes[p2]]))
            {
                updated.push_back(indexes[p1]);
                res[indexes[p1]] += cnt;
                ++p1;
            }
            else
            {
                updated.push_back(indexes[p2]);
                ++cnt, ++p2;
            }
        }
        move(updated.begin(), updated.end(), indexes.begin() + first);
    }
};
void mergeSort(vector<pair<int, int>> &nums, int low, int high, vector<int> &ans)
{
    if (high - low <= 1)
        return;
    int mid = (low + high) / 2, right = mid;
    mergeSort(nums, low, mid, ans);
    mergeSort(nums, mid + 1, high, ans);
    for (int i = low; i < mid; i++)
    {
        while (right < high && nums[i].first > nums[right].first)
            right++;
        ans[nums[i].second] += right - mid;
    }
    inplace_merge(nums.begin() + low, nums.begin() + mid, nums.begin() + right);
}

vector<int> countSmaller(vector<int> &nums)
{
    if (nums.size() == 0)
        return {};
    int len = nums.size();
    vector<int> ans(len, 0);
    vector<pair<int, int>> vec;
    for (int i = 0; i < len; i++)
        vec.emplace_back(nums[i], i);
    mergeSort(vec, 0, len, ans);
    return ans;
}
