#include <iostream>
/*
out of representable values
string contains additional characters (ignored)

*/
using namespace std;
int atoi(const char * str)
{
    if(str==NULL)
        return 0;
    int len = strlen(str);
    int val = 0;
    for (int i = 0; i < len;i++)
    {
        int temp = str[i] - '0';
        val = val * 10 + temp;
    }
    return val;
}