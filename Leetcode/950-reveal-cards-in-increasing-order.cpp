#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void arrange(vector<int> &arr)
{
    for (int i = 0; i < arr.size(); i++)
        swap(arr[i], arr[arr.size() - 1]);
}

vector<int> deckRevealedIncreasing(vector<int> &deck)
{
    sort(deck.begin(), deck.end());
    vector<int> arr;
    for (int i = deck.size() - 1; i >= 0; i--)
    {
        arrange(arr);
        arr.insert(arr.begin(), deck[i]);
    }
    return arr;
}