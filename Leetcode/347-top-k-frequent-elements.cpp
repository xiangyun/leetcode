#include <iostream>
#include <vector>
#include <queue>
#include <unordered_map>
using namespace std;
vector<int> topKFrequent(vector<int> &nums, int k)
{
    auto cmp = [](const pair<int, int> &a, const pair<int, int> &b) {
        return a.second < b.second;
    };
    priority_queue<pair<int, int>, vector<pair<int, int>>, decltype(cmp)> heap(cmp);
    unordered_map<int, int> hash;
    for (auto &i : nums)
    {
        hash[i]++;
    }
    for (auto it = hash.begin(); it != hash.end(); it++)
    {
        heap.emplace(it->first, it->second);
    }
    vector<int> res;
    for (int i = 0; i < k; i++)
    {
        res.push_back(heap.top().first);
        heap.pop();
    }
    return res;
}
