#include <iostream>
#include <vector>
using namespace std;
vector<int> searchRange(vector<int>& nums, int target)
{
	vector<int> range;
	if (nums.size() == 0)
	{
		range.push_back(-1);
		range.push_back(-1);
		return range;
	}
	if (nums.size() == 1)
	{
		if (nums[0] == target)
		{
			range.push_back(0);
			range.push_back(0);
			return range;
		}
	}
	int size = nums.size();
	int mid = size / 2;

	int left = 0, right = size - 1;
	while (left != right)
	{
		if (target == nums[mid])
		{
			if (mid == (size - 1))
			{
				range.push_back(mid - 1); range.push_back(mid);
			}
			range.push_back(mid);
			range.push_back(mid + 1);
			return range;
			break;
		}
		else if (target > nums[mid])
		{
			left = mid + 1;
		}
		else
		{
			right = mid - 1;
		}
		mid /= 2;
	}
	range.push_back(-1);
	range.push_back(-1);
	return range;
}
