#include <iostream>
#include <vector>
using namespace std;

vector<int> sortArrayByParity(vector<int> &A)
{
    if (A.empty())
        return A;
    int start = 0, end = A.size() - 1;
    while (start < end)
    {
        while (start <= end && A[start] % 2 == 0)
            start++;
        while (start <= end && A[end] % 2 == 1)
            end--;
        if (start < end)
            swap(A[start], A[end]);
    }
    return A;
}