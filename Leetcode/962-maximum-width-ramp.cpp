#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include <map>
#include <stack>
using namespace std;
// int maxWidthRamp(vector<int> &A)
// {
//     int n = A.size(), max_len = INT_MIN;
//     vector<int> min_ele(A.size(), INT_MAX);
//     vector<int> max_ele(A.size(), INT_MIN);
//     min_ele[0] = A[0];
//     for (int i = 1; i < n; i++)
//     {
//         min_ele[i] = min(min_ele[i - 1], A[i]);
//     }
//     max_ele[n - 1] = A[n - 1];
//     for (int j = n - 2; j >= 0; j--)
//     {
//         max_ele[j] = max(max_ele[j + 1], A[j]);
//     }
//     int i = 0, j = 0;
//     while (i < n && j < n)
//     {
//         if (min_ele[i] <= max_ele[j])
//         {
//             max_len = max(max_len, j - i);
//             j++;
//         }
//         else
//             i++;
//     }
//     return max_len;
// }
// int maxWidthRamp(vector<int>& A) {
// 	stack<int> s;
// 	int res = 0, n = A.size();
// 	for (int i = 0; i < n; ++i)
// 		if (s.empty() || A[s.top()] > A[i])
// 			s.push(i);
// 	for (int i = n - 1; i >= 0; --i)
// 		while (!s.empty() && A[s.top()] <= A[i])
// 			res = max(res, i - s.top()), s.pop();
// 	return res;
// }
int maxWidthRamp(vector<int> &A)
{
    vector<pair<int, int>> arr;
    for (int i = 0; i < A.size(); i++)
        arr.emplace_back(A[i], i);
    sort(arr.begin(), arr.end());
    int min_val = arr[0].first, cur_index = arr[0].second, res = 0;
    for (int i = 0; i < arr.size(); i++)
    {
        if (arr[i].second > cur_index)
            res = max(res, arr[i].second - cur_index);
        else
            cur_index = arr[i].second;
    }
    return res;
}