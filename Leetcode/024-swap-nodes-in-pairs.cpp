#include <iostream>
using namespace std;
struct ListNode {
	int val;
	ListNode *next;
	ListNode(int x) :val(x), next(NULL) {}
};
ListNode *swapPairs(ListNode *head) {
	if (head == NULL)
		return head;
	if (head->next == NULL)
		return head;
	ListNode *node = new ListNode(0);
	ListNode *pre = node;
	pre->next = head;
	ListNode *cur = head;
	while (cur->next&&cur->next->next)
	{
		ListNode *next = cur->next;
		cur->next = next->next;
		pre->next = next;
		next->next = cur;
		cur = cur->next;
		pre = pre->next->next;
	}
	if (cur->next&&cur->next->next == NULL)
	{
		ListNode *next = cur->next;
		pre->next = next;
		cur->next = NULL;
		next->next = cur;
	}
	head = node->next;
	node->next = NULL;
	delete node;
	return head;
}
