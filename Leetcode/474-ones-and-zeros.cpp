#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;
int findMaxForm(vector<string> &strs, int m, int n)
{
    vector<pair<int, int>> vec;
    for (auto str : strs)
    {
        int x = 0, y = 0;
        for (auto c : str)
            if (c == '0')
                x++;
            else
                y++;
        vec.emplace_back(x, y);
    }
    vector<vector<vector<int>>> dp(vec.size() + 1, vector<vector<int>>(m + 1, vector<int>(n + 1)));
    for (int i = 0; i <= vec.size(); i++)
    {
        for (int j = 0; j <= m; j++)
        {
            for (int k = 0; k <= n; k++)
            {
                if (i == 0)
                    dp[i][j][k] = 0;
                else if (j >= vec[i - 1].first && k >= vec[i - 1].second)
                {
                    dp[i][j][k] = max(dp[i - 1][j][k], dp[i - 1][j - vec[i - 1].first][k - vec[i - 1].second] + 1);
                }
                else
                    dp[i][j][k] = dp[i - 1][j][k];
            }
        }
    }
    return dp[vec.size()][m][n];
}