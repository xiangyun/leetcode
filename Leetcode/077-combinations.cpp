#include <iostream>
#include <vector>
using namespace std;
void backtracking(vector<vector<int>> &res, vector<int> arr, int index, int k, int n)
{
    if (arr.size() == k)
    {
        res.push_back(arr);
        return;
    }
    if (index >= n)
        return;
    for (int i = index + 1; i <= n; i++)
    {
        arr.push_back(i);
        backtracking(res, arr, i, k, n);
        arr.pop_back();
    }
}

vector<vector<int>> combine(int n, int k)
{
    vector<vector<int>> res;
    vector<int> arr;
    backtracking(res, arr, 0, k, n);
    return res;
}