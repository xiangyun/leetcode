#include <iostream>
using namespace std;
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution
{
  public:
    ListNode *insert(ListNode *head, ListNode *node)
    {
        ListNode *p = head, *cur = NULL;
        if (node->val >= head->val)
        {
            node->next = head;
            return node;
        }
        while (p && p->val > node->val)
        {
            cur = p;
            p = p->next;
        }
        if (!p)
        {
            node->next = NULL;
            cur->next = node;
        }
        else
        {
            cur->next = node;
            node->next = p;
        }
        return head;
    }
    ListNode *reverseList(ListNode *head)
    {
        ListNode *cur = head, *pre = NULL, *next;
        while (cur)
        {
            next = cur->next;
            cur->next = pre;
            pre = cur;
            cur = next;
        }
        return pre;
    }
    ListNode *insertionSortList(ListNode *head)
    {
        if (!head || !head->next)
            return head;
        ListNode *p = head;
        ListNode *q = head->next;
        p->next = NULL;
        while (q)
        {
            ListNode *next = q->next;
            p = insert(p, q);
            q = next;
        }
        return reverseList(p);
    }
};