#include <iostream>
#include <vector>
#include <list>
#include <queue>
#include <unordered_map>
using namespace std;
vector<int> findMinHeightTrees(int n, vector<pair<int, int>> &edges)
{
    vector<int> res;
    int *degree = new int[n];
    vector<vector<int>> adj(n, vector<int>(n, 0));
    queue<int> vec;
    for (auto &i : edges)
    {
        degree[i.first]++;
        degree[i.second]++;
        adj[i.first][i.second] = 1;
        adj[i.second][i.first] = 1;
    }
    for (int i = 0; i < n; i++)
    {
        if (degree[i] == 1)
            vec.push(i);
        if (degree[i] == 0)
            res.push_back(i);
    }
    while (n > 2 && !vec.empty())
    {
        int temp = vec.front();
        int sec;
        for (int i = 0; i < n; i++)
        {
            if (adj[temp][i] == 1)
            {
                sec = i;
                break;
            }
        }
        adj[sec][temp] = 0;
        adj[temp][sec] = 0;
        degree[temp]--;
        degree[sec]--;
        if (degree[sec] == 1)
            vec.push(sec);
        n--;
    }

    while (!vec.empty())
    {
        res.push_back(vec.front());
        vec.pop();
    }
    return res;
}