#include <iostream>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

bool mirror(TreeNode *r1, TreeNode *r2)
{
    if (!r1 && !r2)
        return true;
    if (!r1 || !r2)
        return false;
    return r1->val == r2->val && mirror(r1->left, r2->right) && mirror(r1->right, r2->left);
}

bool isSymmetric(TreeNode *root)
{
    return mirror(root, root);
}
